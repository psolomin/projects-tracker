import React from "react"
import { Route, Switch } from "react-router-dom"
import Home from "./containers/Home"
import NotFound from "./containers/NotFound"
import Login from "./containers/Login"
import SignUp from './containers/SignUp'
import Projects from './containers/Projects'
import NewProject from './containers/NewProject'
import Project from './containers/Project'
import Issues from './containers/Issues'
import NewIssue from './containers/NewIssue'
import Issue from './containers/Issue'
import Comments from './containers/Comments'
import NewComment from './containers/NewComment'
import Comment from './containers/Comment'
import AppliedRoute from './components/AppliedRoute'
import UpdateIssue from './containers/UpdateIssue'
import Users from "./containers/Users";
import User from "./containers/User";

export default ({ childProps }) =>
  <Switch>
    <AppliedRoute path="/" exact component={Home} props={childProps} />
    <AppliedRoute path="/login" exact component={Login} props={childProps} />
    <AppliedRoute path="/signup" exact component={SignUp} props={childProps} />

    <AppliedRoute path="/projects/filters" exact component={Projects} props={childProps} />
    <AppliedRoute path="/projects/add-new" exact component={NewProject} props={childProps} />
    <AppliedRoute path="/projects/:id" exact component={Project} props={childProps} />

    <AppliedRoute path="/issues/filters" exact component={Issues} props={childProps} />
    <AppliedRoute path="/projects/:id/issues/add-new" exact component={NewIssue} props={childProps} />
    <AppliedRoute path="/issues/:id" exact component={Issue} props={childProps} />
    <AppliedRoute path="/issues/:id/update" exact component={UpdateIssue} props={childProps} />

    <AppliedRoute path="/comments/filters" exact component={Comments} props={childProps} />
    <AppliedRoute path="/issues/:id/comments/add-new" exact component={NewComment} props={childProps} />
    <AppliedRoute path="/comments/:id" exact component={Comment} props={childProps} />

    <AppliedRoute path="/users/all" exact component={Users} props={childProps} />
    <AppliedRoute path="/users/:id" exact component={User} props={childProps} />

    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>
