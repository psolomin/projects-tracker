import React, { Component, Fragment } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Nav, Navbar, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import './App.css'
import Routes from './Routes'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isAuthenticated: props.isAuthenticated,
      isAuthenticating: props.isAuthenticating,
      relatedResourceUri: props.relatedResourceUri,
      authService: props.authService,
      homeService: props.homeService,
      projectsService: props.projectsService,
      issuesService: props.issuesService,
      commentsService: props.commentsService,
      usersService: props.usersService
    }
  }

  componentDidMount() {
    try {
      let u = this.state.authService.getCurrentUser()
      if (u != null)
        this.userHasAuthenticated(true)
      this.setState({ isAuthenticating: false })
    } catch (error) {
      if (error !== 'No current user') {
        alert(error)
      }
    }
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated })
  }

  setRelatedResourceUri = uri => {
    this.setState({ relatedResourceUri: uri })
  }

  handleLogout = event => {
    this.state.authService.logout()
    this.userHasAuthenticated(false)
    this.props.history.push('/login')
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      relatedResourceUri: this.state.relatedResourceUri,
      userHasAuthenticated: this.userHasAuthenticated,
      setRelatedResourceUri: this.setRelatedResourceUri,
      authService: this.state.authService,
      homeService: this.state.homeService,
      projectsService: this.state.projectsService,
      issuesService: this.state.issuesService,
      commentsService: this.state.commentsService,
      usersService: this.state.usersService
    }

    return (
      !this.state.isAuthenticating &&
      <div className="App container">
      <Navbar fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Home</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            {this.state.isAuthenticated  // TODO: study how this syntax works
              ? <NavItem onClick={this.handleLogout}>Logout</NavItem>
              : <Fragment>
                  <LinkContainer to="/signup">
                    <NavItem>Signup</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/login">
                    <NavItem>Login</NavItem>
                  </LinkContainer>
                </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Routes childProps={childProps} />
    </div>
    )
  }
}

export default withRouter(App)
