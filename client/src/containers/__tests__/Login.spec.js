import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';
import { act } from 'react-dom/test-utils';
import { fireEvent } from '@testing-library/react'
import Login from '../Login';
import { createMemoryHistory } from "history";
import { Router } from "react-router";

let container;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container.remove();
  container = null;
});

test('login form renders with expected content', () => {
    act(() => {
        ReactDOM.render(<Login/>, container);
    });
    const usr = container.querySelector('[type=user_name]');
    const pass = container.querySelector('[type=user_password]');
    expect(usr.placeholder).toBe('User name');
    expect(pass.placeholder).toBe('Password');
});


test('login form submission redirects to home page', () => {
    function loginStub(usr, pass) {
        return Promise.resolve({status: 200, body: {}});
    };
    function homeStub() {
        return Promise.resolve({status: 200, body: {}});
    };
    function userHasAuthenticatedStub(arg) {};
    const historyMock = { push: jest.fn() };

    act(() => {
        ReactDOM.render(
            <Login
                authService={{login: loginStub}}
                homeService={{home: homeStub}}
                userHasAuthenticated={userHasAuthenticatedStub}
                isAuthenticated={true}
                history={historyMock}
            />,
            container
        );
    });
    const name = container.querySelector('input[type="user_name"]')
    const pass = container.querySelector('input[type="user_password"]')
    const submit = container.querySelector('button[type="submit"]')

    fireEvent.change(name, { target: { value: 'john' } });
    fireEvent.change(pass, { target: { value: 'password' } });
    fireEvent.submit(submit);
    setTimeout(function() {
        try {
          expect(historyMock.push.mock.calls[0][0]).toEqual('/');
          done()
        } catch (e) {
          done.fail(e)
        }
    }, 10);
});

test('login form submission redirects to home page 2', () => {
    function loginStub(usr, pass) {
        return Promise.resolve({status: 200, body: {}});
    };
    function homeStub() {
        return Promise.resolve({status: 200, body: {}});
    };
    function userHasAuthenticatedStub(arg) {};

    const history = createMemoryHistory();
});
