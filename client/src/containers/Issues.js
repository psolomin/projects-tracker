import React, { Component } from "react"
import { PageHeader, ListGroup } from "react-bootstrap"
import Paginator from '../components/Paginator'
import ListWithNewButton from '../components/ListWithNewButton'
import "./Issues.css"
import { getPaginator } from '../helpers/Fetcher'

export default class Issues extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      paginator: {},
      issues: [],
      createNewLink: null
    }

    this.paginatorOnClickAction = this.paginatorOnClickAction.bind(this)
    this.loadIssues = this.loadIssues.bind(this)
    this.renderIssues = this.renderIssues.bind(this)
    this.renderIssuesList = this.renderIssuesList.bind(this)
  }

  renderIssuesList() {
    return <ListWithNewButton
      items={this.state.issues}
      itemName='issue'
      previewFieldName='issue_name'
      urlToItemPrefix='issues'
      newUri={this.state.createNewLink}
      onClickAction={this.addNewOnClickAction.bind(this)}
    />
  }

  paginatorOnClickAction(uri) {
    this.loadIssues(uri)
  }

  addNewOnClickAction(uri) {
    this.props.setRelatedResourceUri(uri)
  }

  renderIssues() {
    return (
      <div className="issues">
        <PageHeader>Issues</PageHeader>
        {this.state.isLoading
          ? <div>Loading...</div>
          : <div>
              <ListGroup>{this.renderIssuesList(this.state.issues)}</ListGroup>
              <Paginator
                paginator={this.state.paginator}
                paginatorOnClickAction={this.paginatorOnClickAction}
              />
            </div>
        }
      </div>
    )
  }

  render() {
    return (
      <div className="Issues">
        {this.renderIssues()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadIssues(this.props.relatedResourceLink)
      : this.props.history.push('/login')
  }
  
  loadIssues(url) {
    this.setState({ isLoading: true })
    
    this.props.issuesService.issues(
      url,
      this.props.location.pathname,
      this.props.location.search
    ).then(
      response => {
        if (response.status === 200) {
          return response.json().then(
            responseData => {
              this.setState({
                issues: responseData._embedded ? responseData._embedded.issues : [],
                paginator: getPaginator(responseData),
                createNewLink: responseData._links['add-new-issue'],
                isLoading: false,
              })
            }
          )
        } else {
          response.json().then(apiError => {
            alert(apiError.message)
            this.props.history.push('/')
          })
        }
      }
    ).catch(error => { alert(error) })
  }
}
