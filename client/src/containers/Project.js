import React, { Component } from "react"
import { Jumbotron } from "react-bootstrap"
import "./Project.css"
import ActionLinksButtons from '../components/ActionLinksButtons'

export default class Project extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      project: null,
      links: null
    }

    this.loadProject = this.loadProject.bind(this)
  }

  renderContainer() {
    return (
      <div className="project">
        {this.state.isLoading
          ? <div>Loading...</div>
          : this.renderProject(this.state.project)}
      </div>
    )
  }

  linkedResourceButtonOnClick(uri, path) {
    this.props.setRelatedResourceUri(uri)
    this.props.history.push(path)
  }

  // TODO: display project settings
  renderProject(project) {
    return <Jumbotron>
      <h1>{project.project_name}</h1>
      <p>Description: {project.description}</p>
      <p>
        <ActionLinksButtons
          links={this.state.links}
          onClickHandler={this.linkedResourceButtonOnClick.bind(this)}
        />
      </p>
    </Jumbotron>
  }

  render() {
    return (
      <div className="Project">
        {this.renderContainer()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadProject()
      : this.props.history.push('/login')
  }
  
  loadProject() {
    this.setState({ isLoading: true })
    this.props.projectsService.project(
      this.props.relatedResourceUri,
      this.props.location.pathname
    ).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({
            isLoading: false,
            project: data,
            links: data._links
          })
        })
      } else {
        response.json().then(apiError => {
          alert(apiError.message)
          this.props.history.push('/projects')
        })
      }
    }).catch(error => { alert(error) })
  }
}
