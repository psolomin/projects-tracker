import React, { Component } from "react"
import { Jumbotron } from "react-bootstrap"
import "./Issue.css"
import ActionLinksButtons from '../components/ActionLinksButtons'

export default class Issue extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      issue: null,
      links: null
    }

    this.loadIssue = this.loadIssue.bind(this)
  }

  renderContainer() {
    return (
      <div className="issue">
        {this.state.isLoading
          ? <div>Loading...</div>
          : this.renderIssue(this.state.issue)}
      </div>
    )
  }

  linkedResourceButtonOnClick(uri, path) {
    this.props.setRelatedResourceUri(uri)
    this.props.history.push(path)
  }

  // TODO: '/new' is evil
  renderIssue(issue) {
    return <Jumbotron>
      <h1>{issue.issue_name}</h1>
      <p>Description: {issue.description}</p>
      <p>Created at: {issue.created_at}</p>
      <p>Updated at: {issue.updated_at || 'No status updates.'}</p>
      <p>Current state: {issue.state}</p>
      <p>
        <ActionLinksButtons
          links={this.state.links}
          onClickHandler={this.linkedResourceButtonOnClick.bind(this)}
        />
      </p>
    </Jumbotron>
  }

  render() {
    return (
      <div className="Issue">
        {this.renderContainer()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadIssue()
      : this.props.history.push('/login')
  }
  
  loadIssue() {
    this.setState({ isLoading: true })

    this.props.issuesService.issue(
      this.props.relatedResourceUri,
      this.props.location.pathname
    ).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({
            isLoading: false,
            issue: data,
            links: data._links
          })
        })
      } else {
        response.json().then(apiError => {
          alert(apiError.message)
          this.props.history.push('/issues')
        })
      }
    }).catch(error => { alert(error) })
  }
}
