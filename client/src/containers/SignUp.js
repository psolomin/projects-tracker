import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import './SignUp.css'
import LoaderButton from '../components/LoaderButton'

export default class SignUp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userName: '',
      userPassword: '',
      isLoading: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  validateForm() {
    return this.state.userName.length > 0 && this.state.userPassword.length > 0
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()

    this.setState({ isLoading: true })

    this.props.authService.signUp(
      this.state.userName,
      this.state.userPassword
    ).then(result => {
      if (result.status === 201) {
        this.props.history.push('/login') // redirect to login after signup
      } else {
        result.json().then(apiError => {
          alert(apiError.message)
        })
      }
    }).catch(error => {
      alert(error.message)
    })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <div className="SignUp">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="userName" bsSize="large">
            <ControlLabel>User name</ControlLabel>
            <FormControl
              type="user_name"
              value={this.state.userName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="userPassword" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.userPassword}
              onChange={this.handleChange}
              type="user_password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Create user"
            loadingText="Creating user…"
          />
        </form>
      </div>
    )
  }
}
