import React, { Component } from "react"
import { PageHeader, ListGroup } from "react-bootstrap"
import Paginator from '../components/Paginator'
import ListWithNewButton from '../components/ListWithNewButton'
import "./Projects.css"
import { getPaginator } from '../helpers/Fetcher'

export default class Projects extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      paginator: {},
      projects: [],
      createNewLink: null
    }

    this.paginatorOnClickAction = this.paginatorOnClickAction.bind(this)
    this.loadProjects = this.loadProjects.bind(this)
    this.renderProjects = this.renderProjects.bind(this)
    this.renderProjectsList = this.renderProjectsList.bind(this)
  }

  renderProjectsList() {
    return <ListWithNewButton
      items={this.state.projects}
      itemName='project'
      previewFieldName='project_name'
      urlToItemPrefix='projects'
      newUri={this.state.createNewLink}
      onClickAction={this.addNewOnClickAction.bind(this)}
    />
  }

  addNewOnClickAction(uri) {
    this.props.setRelatedResourceUri(uri)
  }

  paginatorOnClickAction(uri) {
    this.loadProjects(uri)
  }

  renderProjects() {
    return (
      <div className="projects">
        <PageHeader>Projects</PageHeader>
        {this.state.isLoading
          ? <div>Loading...</div>
          : <div>
              <ListGroup>{this.renderProjectsList(this.state.projects)}</ListGroup>
              <Paginator
                paginator={this.state.paginator}
                paginatorOnClickAction={this.paginatorOnClickAction}
              />
            </div>
        }
      </div>
    )
  }

  render() {
    return (
      <div className="Projects">
        {this.renderProjects()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadProjects()
      : this.props.history.push('/login')
  }
  
  loadProjects(url) {
    this.setState({ isLoading: true })
    this.props.projectsService.projects(url, this.props.location.pathname).then(
      response => {
        if (response.status === 200) {
          return response.json().then(
            responseData => {
              this.setState({
                projects: responseData._embedded ? responseData._embedded.projects : [],
                paginator: getPaginator(responseData),
                createNewLink: responseData._links['add-new-project'],
                isLoading: false,
              })
            }
          )
        } else {
          response.json().then(apiError => alert(apiError.message))
        }
      }
    ).catch(error => { alert(error) })
  }
}
