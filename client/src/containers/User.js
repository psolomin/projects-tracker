import React, { Component } from "react"
import { Jumbotron } from "react-bootstrap"
import "./User.css"
import ActionLinksButtons from '../components/ActionLinksButtons'

export default class User extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      user: null,
      links: null
    }

    this.loadUser = this.loadUser.bind(this)
  }

  renderContainer() {
    return (
      <div className="user">
        {this.state.isLoading
          ? <div>Loading...</div>
          : this.renderUser(this.state.user)}
      </div>
    )
  }

  linkedResourceButtonOnClick(uri, path) {
    this.props.setRelatedResourceUri(uri)
    this.props.history.push(path)
  }

  renderUser(user) {
    return <Jumbotron>
      <h1>{user.user_name}</h1>
      <p>Created at: {user.created_at}</p>
      <p>
        <ActionLinksButtons
          links={this.state.links}
          onClickHandler={this.linkedResourceButtonOnClick.bind(this)}
        />
      </p>
    </Jumbotron>
  }

  render() {
    return (
      <div className="User">
        {this.renderContainer()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadUser()
      : this.props.history.push('/login')
  }
  
  loadUser() {
    this.setState({ isLoading: true })

    this.props.usersService.user(
      this.props.relatedResourceUri,
      this.props.location.pathname
    ).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({
            isLoading: false,
            user: data,
            links: data._links
          })
        })
      } else {
        response.json().then(apiError => {
          alert(apiError.message)
          this.props.history.push('/users')
        })
      }
    }).catch(error => { alert(error) })
  }
}
