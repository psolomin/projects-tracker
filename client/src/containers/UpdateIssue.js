import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import LoaderButton from '../components/LoaderButton'
import './UpdateIssue.css'

export default class UpdateIssue extends Component {
  constructor(props) {
    super(props)

    this.file = null

    this.state = {
      isLoading: null,
      newIssueState: '',
      issueLink: null
    }
  }
  
  validateForm = () => { return true }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleCancel = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    this.props.history.push(
      this.props.relatedResourceUri 
        ? new URL(this.props.relatedResourceUri).pathname
        : '/issues/filters')
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    this.props.issuesService.updateIssueState(
      this.props.relatedResourceUri,
      this.props.location.pathname,
      this.state.newIssueState
    ).then(
      response => {
        if (response.status === 200) {
          response.json().then(data => {
            let issueLink = data._links.self.href
            this.props.setRelatedResourceUri(issueLink)
            this.props.history.push(new URL(issueLink).pathname)
          })
        } else {
          response.json().then(apiError => { alert(apiError.message) })
        }
      }
    ).catch(error => { alert(error) })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <div className="UpdateIssue">
        <form onSubmit={this.handleSubmit} onReset={this.handleCancel}>
        <FormGroup controlId="newIssueState" bsSize="large">
            <ControlLabel>New issue state</ControlLabel>
            <FormControl
              type="issue_state"
              value={this.state.newIssueState}
              onChange={this.handleChange}
            />
          </FormGroup>
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Update"
            loadingText="Updating…"
          />
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={false}
            type="reset"
            isLoading={this.state.isLoading}
            text="Cancel"
            loadingText="Cancelling…"
          />
        </form>
      </div>
    )
  }
}
