import React, { Component } from "react"
import { Jumbotron } from "react-bootstrap"
import "./Comment.css"
import ActionLinksButtons from '../components/ActionLinksButtons'

export default class Comment extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      comment: null,
      links: null
    }

    this.loadComment = this.loadComment.bind(this)
  }

  renderContainer() {
    return (
      <div className="comment">
        {this.state.isLoading
          ? <div>Loading...</div>
          : this.renderComment(this.state.comment)}
      </div>
    )
  }

  linkedResourceButtonOnClick(uri, path) {
    this.props.setRelatedResourceUri(uri)
    this.props.history.push(path)
  }

  // TODO: '/new' is evil
  renderComment(comment) {
    return <Jumbotron>
      <h1>{comment.comment_body}</h1>
      <p>Created at: {comment.created_at}</p>
      <p>
        <ActionLinksButtons
          links={this.state.links}
          onClickHandler={this.linkedResourceButtonOnClick.bind(this)}
        />
      </p>
    </Jumbotron>
  }

  render() {
    return (
      <div className="Comment">
        {this.renderContainer()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadComment()
      : this.props.history.push('/login')
  }
  
  loadComment() {
    this.setState({ isLoading: true })

    this.props.commentsService.comment(
      this.props.relatedResourceUri,
      this.props.location.pathname
    ).then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({
            isLoading: false,
            comment: data,
            links: data._links
          })
        })
      } else {
        response.json().then(apiError => {
          alert(apiError.message)
          this.props.history.push('/comments')
        })
      }
    }).catch(error => { alert(error) })
  }
}
