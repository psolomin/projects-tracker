import React, { Component } from "react"
import "./Home.css"
import ActionLinksButtons from '../components/ActionLinksButtons'

export default class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      links: []
    }
  }

  linkedResourceButtonOnClick(uri, page) {
    this.props.setRelatedResourceUri(uri)
    this.props.history.push(page)
  }

  renderContainer() {
    return (
      <div className="Home">
        <div className="lander">
          <h1>ProjectTracker</h1>
          <p>Authenticated</p>
        </div>
        <div className="menu">
        <p>
          <ActionLinksButtons
            links={this.state.links}
            onClickHandler={this.linkedResourceButtonOnClick.bind(this)}
          />
        </p>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="Home">
        {this.renderContainer()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadHome()
      : this.props.history.push('/login')
  }

  loadHome() {
    this.setState({ isLoading: true })

    this.props.homeService.home().then(response => {
      if (response.status === 200) {
        response.json().then(data => {
          this.setState({
            isLoading: false,
            links: data._links
          })
        })
      } else {
        response.json().then(apiError => {
          alert(apiError.message)
        })
      }
    }).catch(error => { alert(error) })
  }
}
