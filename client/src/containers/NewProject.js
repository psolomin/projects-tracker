import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import LoaderButton from '../components/LoaderButton'
import './NewProject.css'

export default class NewProject extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      projectName: '',
      description: '',
      issueStates: '',
      issueStateTransitions: ''
    }
  }

  validateJsonInputField = (fieldVal) => {
    try {
      JSON.parse(fieldVal)
    } catch (e) {
      return false
    }
    return true
  }

  validateJsonInput = () => {
    return this.validateJsonInputField(this.state.issueStates)
      && this.validateJsonInputField(this.state.issueStateTransitions)
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    this.props.projectsService.newProject(
      this.props.relatedResourceUri,
      this.props.location.pathname,
      this.state.projectName,
      this.state.description,
      {
        issue_states: JSON.parse(this.state.issueStates),
        issue_state_transitions: JSON.parse(this.state.issueStateTransitions)
      }
    ).then(
      response => {
        if (response.status === 201) {
          this.props.history.push("/projects/filters")
        } else {
          response.json().then(apiError => { alert(apiError.message) })
        }
      }
    ).catch(error => { alert(error) })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <div className="NewProject">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="projectName" bsSize="large">
            <ControlLabel>Project name</ControlLabel>
            <FormControl
              type="project_name"
              value={this.state.projectName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="description" bsSize="large">
            <ControlLabel>Project description</ControlLabel>
            <FormControl
              value={this.state.description}
              onChange={this.handleChange}
              type="description"
            />
          </FormGroup>
          <FormGroup controlId="issueStates" bsSize="large">
            <ControlLabel>Issues' possible states</ControlLabel>
            <FormControl
              placeholder='[{"name":"open","is_first":"true"}]'
              value={this.state.issueStates}
              onChange={this.handleChange}
              type="issues_states"
              className={!this.validateJsonInputField(this.state.issueStates) ? "error" : ""}
            />
          </FormGroup>
          <FormGroup controlId="issueStateTransitions" bsSize="large">
            <ControlLabel>Issues' states possible transitions</ControlLabel>
            <FormControl
              placeholder='[{"state_from": "open", "state_to": "closed"}]'
              value={this.state.issueStateTransitions}
              onChange={this.handleChange}
              type="issue_state_transitions"
              className={!this.validateJsonInputField(this.state.issueStateTransitions) ? "error" : ""}
            />
          </FormGroup>
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateJsonInput()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Create"
            loadingText="Creating…"
          />
        </form>
      </div>
    )
  }
}
