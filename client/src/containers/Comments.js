import React, { Component } from "react"
import { PageHeader, ListGroup } from "react-bootstrap"
import Paginator from '../components/Paginator'
import ListWithNewButton from '../components/ListWithNewButton'
import "./Comments.css"
import { getPaginator } from '../helpers/Fetcher'

export default class Comments extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      paginator: {},
      comments: [],
      createNewLink: null
    }

    this.addNewOnClickAction = this.addNewOnClickAction.bind(this)
    this.paginatorOnClickAction = this.paginatorOnClickAction.bind(this)
    this.loadComments = this.loadComments.bind(this)
    this.renderComments = this.renderComments.bind(this)
    this.renderCommentsList = this.renderCommentsList.bind(this)
  }

  renderCommentsList() {
    return <ListWithNewButton
      items={this.state.comments}
      itemName='comment'
      previewFieldName='comment_body'
      urlToItemPrefix='comments'
      newUri={this.state.createNewLink}
      onClickAction={this.addNewOnClickAction.bind(this)}
    />
  }

  paginatorOnClickAction(uri) {
    this.loadComments(uri)
  }

  addNewOnClickAction(uri) {
    this.props.setRelatedResourceUri(uri)
  }

  renderComments() {
    return (
      <div className="comments">
        <PageHeader>Comments</PageHeader>
        {this.state.isLoading
          ? <div>Loading...</div>
          : <div>
              <ListGroup>{this.renderCommentsList(this.state.comments)}</ListGroup>
              <Paginator
                paginator={this.state.paginator}
                paginatorOnClickAction={this.paginatorOnClickAction}
              />
            </div>
        }
      </div>
    )
  }

  render() {
    return (
      <div className="Comments">
        {this.renderComments()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadComments(this.props.relatedResourceLink)
      : this.props.history.push('/login')
  }
  
  loadComments(url) {
    this.setState({ isLoading: true })
    
    this.props.commentsService.comments(
      url,
      this.props.location.pathname,
      this.props.location.search
    ).then(
      response => {
        if (response.status === 200) {
          return response.json().then(
            responseData => {
              this.setState({
                comments: responseData._embedded ? responseData._embedded.comments : [],
                paginator: getPaginator(responseData),
                createNewLink: responseData._links['add-new-comment'],
                isLoading: false,
              })
            }
          )
        } else {
          response.json().then(apiError => {
            alert(apiError.message)
            this.props.history.push('/')
          })
        }
      }
    ).catch(error => { alert(error) })
  }
}
