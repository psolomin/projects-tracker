import React, { Component } from "react"
import { PageHeader, ListGroup } from "react-bootstrap"
import Paginator from '../components/Paginator'
import ListWithNewButton from '../components/ListWithNewButton'
import "./Users.css"
import { getPaginator } from '../helpers/Fetcher'

export default class Users extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      paginator: {},
      users: []
    }

    this.paginatorOnClickAction = this.paginatorOnClickAction.bind(this)
    this.loadUsers = this.loadUsers.bind(this)
    this.renderUsers = this.renderUsers.bind(this)
    this.renderUsersList = this.renderUsersList.bind(this)
  }

  renderUsersList() {
    return <ListWithNewButton
      items={this.state.users}
      itemName='user'
      previewFieldName='user_name'
      urlToItemPrefix='users'
      newUri={this.state.createNewLink}
      onClickAction={this.addNewOnClickAction.bind(this)}
    />
  }

  paginatorOnClickAction(url) {
    this.loadUsers(url)
  }

  addNewOnClickAction(url) {
    this.props.setRelatedResourceUri(url)
  }

  renderUsers() {
    return (
      <div className="users">
        <PageHeader>Users</PageHeader>
        {this.state.isLoading
          ? <div>Loading...</div>
          : <div>
              <ListGroup>{this.renderUsersList(this.state.users)}</ListGroup>
              <Paginator
                paginator={this.state.paginator}
                paginatorOnClickAction={this.paginatorOnClickAction}
              />
            </div>
        }
      </div>
    )
  }

  render() {
    return (
      <div className="Users">
        {this.renderUsers()}
      </div>
    )
  }

  componentDidMount() {
    this.props.isAuthenticated
      ? this.loadUsers(this.props.relatedResourceLink)
      : this.props.history.push('/login')
  }
  
  loadUsers(url) {
    this.setState({ isLoading: true })
    
    this.props.usersService.users(
      url,
      this.props.location.pathname,
      this.props.location.search
    ).then(
      response => {
        if (response.status === 200) {
          return response.json().then(
            responseData => {
              this.setState({
                users: responseData._embedded ? responseData._embedded.users : [],
                paginator: getPaginator(responseData),
                isLoading: false,
              })
            }
          )
        } else {
          response.json().then(apiError => {
            alert(apiError.message)
            this.props.history.push('/')
          })
        }
      }
    ).catch(error => { alert(error) })
  }
}
