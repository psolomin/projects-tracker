import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import LoaderButton from '../components/LoaderButton'
import './NewIssue.css'

export default class NewIssue extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: null,
      issueName: '',
      description: ''
    }
  }
  
  validateForm = () => { return true }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    this.props.issuesService.newIssue(
      this.props.relatedResourceLink,
      this.props.location.pathname,
      this.state.issueName,
      this.state.description
    ).then(
      response => {
        if (response.status === 201) {
          response.json().then(data => {
            let parentProjectIssuesLink = data._links['parent-project-issues'].href
            this.props.setRelatedResourceUri(parentProjectIssuesLink)
            let u = new URL(parentProjectIssuesLink)
            this.props.history.push(u.pathname + u.search)
          })
        } else {
          response.json().then(apiError => { alert(apiError.message) })
        }
      }
    ).catch(error => { alert(error) })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <div className="NewIssue">
        <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="issueName" bsSize="large">
            <ControlLabel>Issue name</ControlLabel>
            <FormControl
              type="issue_name"
              value={this.state.issueName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="description" bsSize="large">
            <ControlLabel>Issue description</ControlLabel>
            <FormControl
              value={this.state.description}
              onChange={this.handleChange}
              type="description"
            />
          </FormGroup>
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Create"
            loadingText="Creating…"
          />
        </form>
      </div>
    )
  }
}
