import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import './Login.css'
import LoaderButton from '../components/LoaderButton'

export default class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userName: '',
      userPassword: '',
      isLoading: false
    }
  }

  validateForm() {
    return this.state.userName.length > 0 && this.state.userPassword.length > 0
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit = event => {
    event.preventDefault()

    this.setState({ isLoading: true })

    this.props.authService.login(
      this.state.userName,
      this.state.userPassword
    ).then(result => {
      if (result.status === 200) {
        this.props.userHasAuthenticated(true)
        this.props.history.push('/') // redirect to homepage after login
      } else {
        result.json().then(apiError => { alert(apiError.message) })
      }
    }).catch(error => {
      console.log(error)
      alert(error.message)
    })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="userName" bsSize="large">
            <ControlLabel>User name</ControlLabel>
            <FormControl
              type="user_name"
              placeholder="User name"
              value={this.state.userName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="userPassword" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.userPassword}
              onChange={this.handleChange}
              type="user_password"
              placeholder="Password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Logging in…"
          />
        </form>
      </div>
    )
  }
}
