import React, { Component } from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import LoaderButton from '../components/LoaderButton'
import './NewComment.css'

export default class NewComment extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      commentBody: ''
    }
  }
  
  validateForm = () => { return true }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({ isLoading: true })
    this.props.commentsService.newComment(
      this.props.relatedResourceLink,
      this.props.location.pathname,
      this.state.commentBody
    ).then(
      response => {
        if (response.status === 201) {
          response.json().then(data => {
            let parentIssueCommentsLink = data._links['parent-issue-comments'].href
            let uri = new URL(parentIssueCommentsLink)
            this.props.setRelatedResourceUri(parentIssueCommentsLink)
            this.setState({ isLoading: false })
            this.props.history.push(uri.pathname + uri.search)
          })
        } else {
          response.json().then(apiError => {
            alert(apiError.message)
            this.setState({ isLoading: false })
          })
        }
      }
    ).catch(error => { alert(error) })
  }

  render() {
    return (
      <div className="NewComment">
        <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="commentBody" bsSize="large">
            <ControlLabel>Comment body</ControlLabel>
            <FormControl
              type="comment_body"
              value={this.state.commentBody}
              onChange={this.handleChange}
            />
          </FormGroup>
          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Create"
            loadingText="Creating…"
          />
        </form>
      </div>
    )
  }
}
