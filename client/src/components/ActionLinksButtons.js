import React from 'react'
import ActionLinkButton from './ActionLinkButton'

export default ({
  links,
  onClickHandler,
  ...props
}) =>
  Object.keys(links).filter(k => k !== 'self').map(
    (linkRelName, i) => {
      let link = links[linkRelName]
      return <ActionLinkButton
        linkRel={linkRelName}
        link={link}
        onClickHandler={onClickHandler}
        key={linkRelName}
      />
    }
  )
