import React from 'react'
import { Pagination } from 'react-bootstrap'

export default ({ paginator, paginatorOnClickAction }) =>
  <Pagination>
    <Pagination.First onClick={() => paginatorOnClickAction(paginator.firstLink)} />
    <Pagination.Prev onClick={() => paginatorOnClickAction(paginator.previousLink)} />
    
    {paginator.currentPage > 1 &&
      <Pagination.Item onClick={() => paginatorOnClickAction(paginator.firstLink)}>
        {1}
      </Pagination.Item>
    }
    
    {paginator.currentPage >= 3 && <Pagination.Ellipsis />}

    <Pagination.Item active onClick={() => paginatorOnClickAction(paginator.currentLink)}>
      {paginator.currentPage}
    </Pagination.Item>
    
    {paginator.currentPage <= paginator.totalPages - 2 && <Pagination.Ellipsis />}

    {paginator.currentPage < paginator.totalPages &&
      <Pagination.Item onClick={() => paginatorOnClickAction(paginator.lastLink)}>
        {paginator.totalPages}
      </Pagination.Item>
    }
    
    <Pagination.Next onClick={() => paginatorOnClickAction(paginator.nextLink)}/>
    <Pagination.Last onClick={() => paginatorOnClickAction(paginator.lastLink)}/>
  </Pagination>
