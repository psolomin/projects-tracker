import React from 'react';
import { mount } from 'enzyme';
import ActionLinkButton from '../ActionLinkButton';

test('action button shows link title text', () => {
   const linkRel = 'add-new';
   const link = {href: 'https://www.example.com', title: 'add new thing'};
   const onClickHandler = null;
   const wrapper = mount(
	   <ActionLinkButton
         linkRel={linkRel}
         link={link}
         onClickHandler={onClickHandler}
         key={linkRel}
       />
   );
   expect(wrapper.text()).toMatch(link.title);
});

test('action button click executes onClickHandler', () => {
   const linkRel = 'add-new';
   const href = 'https://www.example.com?arg1=a&arg2=b';
   const pushPage = 'push-here'
   const link = {href: href, title: 'add new thing'};
   const handler = jest.fn()
   const wrapper = mount(
	   <ActionLinkButton
         linkRel={linkRel}
         link={link}
         onClickHandler={handler}
         key={linkRel}
		 data-test="ab1"
       />
   );
   wrapper.find('[data-test="ab1"]').simulate('click');
   const redirectionArgs = '//add-new?arg1=a&arg2=b'
   expect(handler).toHaveBeenCalledWith(href, redirectionArgs);
});

test('action button click executes onClickHandler with update arg', () => {
   const linkRel = 'update-thing';
   const href = 'https://www.example.com?arg1=a&arg2=b';
   const pushPage = 'push-here'
   const link = {href: href, title: 'update thing'};
   const handler = jest.fn()
   const wrapper = mount(
	   <ActionLinkButton
         linkRel={linkRel}
         link={link}
         onClickHandler={handler}
         key={linkRel}
		 data-test="ab1"
       />
   );
   wrapper.find('[data-test="ab1"]').simulate('click');
   const redirectionArgs = '//update?arg1=a&arg2=b'
   expect(handler).toHaveBeenCalledWith(href, redirectionArgs);
});
