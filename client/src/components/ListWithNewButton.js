import React from 'react'
import { ListGroupItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

export default ({
  items,
  itemName,
  previewFieldName,
  urlToItemPrefix,
  newUri,
  onClickAction,
  ...props
}) => {
  let newPath = newUri
      ? new URL(newUri.href).pathname + '/add-new'
      : null
  return [{}].concat(items).map(
    (item, i) =>
      i !== 0
        ? <LinkContainer
          key={item.id}
          to={new URL(item._links.self.href).pathname}
          onClick={() => onClickAction(item._links.self.href)}
        >
          <ListGroupItem header={item[previewFieldName].trim().split("\n")[0]}>
            {"Created: " + new Date(item.created_at).toLocaleString()}
          </ListGroupItem>
        </LinkContainer>
        : newUri
          ? <LinkContainer
              key="new"
              to={newPath}
              onClick={() => onClickAction()}
            >
            <ListGroupItem>
            <h4>
              <b>{"\uFF0B"}</b> Create a new {itemName}
            </h4>
          </ListGroupItem>
          </LinkContainer>
          : <div key="dummy"></div>
  )
}
