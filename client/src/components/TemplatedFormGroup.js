import React from 'react'
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap'

export default (templateProperty) =>
  <FormGroup controlId={templateProperty.name} bsSize="large">
    <ControlLabel>{templateProperty.title}</ControlLabel>
  <FormControl
    value={templateProperty.value}
    onChange={templateProperty.changeHandler}
    type={templateProperty.name}
  />
  </FormGroup>
