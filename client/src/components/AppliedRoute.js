import React from 'react'
import { Route } from 'react-router-dom'

// TODO: study how this syntax works.
// TODO: there should be a way to block routes if user is not authorized.
export default ({ component: C, props: cProps, ...rest }) =>
  <Route {...rest} render={props => <C {...props} {...cProps} />} />
