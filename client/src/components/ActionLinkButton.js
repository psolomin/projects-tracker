import React from 'react'
import { Button } from 'react-bootstrap'

export default ({
  linkRel,
  link,
  onClickHandler,
  ...props
}) => {
  const url = new URL(link.href)
  const creationActionSuffix = linkRel.startsWith('add-new') ? '/add-new' : ''
  const updatingActionSuffix = linkRel.startsWith('update') ? '/update' : ''
  const pushPage = url.pathname + creationActionSuffix + updatingActionSuffix + url.search
  return <Button
    bsStyle="primary"
    onClick={() => { onClickHandler(link.href, pushPage) }}
  >{link.title}
  </Button>
}
