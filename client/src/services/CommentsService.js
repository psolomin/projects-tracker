import { config } from '../config'

export default class CommentsService {
  constructor(authService) {
    this.authService = authService
  }
  
  // TODO: deal with `/new` suffix gracefully
  newComment(newCommentLink, pathName, commentBody) {
    let u = this.authService.getCurrentUser()
    let finalNewCommentLink = newCommentLink
      ? newCommentLink
      : (config.server.HTTP_API_ENDPOINT + pathName).replace('/add-new', '')
    return fetch(finalNewCommentLink, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      },
      body: JSON.stringify({ 'comment_body': commentBody })
    })
  }

  comments(commentsLink, pathName, searchString) {
    let u = this.authService.getCurrentUser()
    let finalCommentsLink = commentsLink
      ? commentsLink
      : config.server.HTTP_API_ENDPOINT + pathName + searchString
    return fetch(finalCommentsLink, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }

  comment(uri, pathname) {
    let u = this.authService.getCurrentUser()
    let target = uri || config.server.HTTP_API_ENDPOINT + pathname
    return fetch(target, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }
}
