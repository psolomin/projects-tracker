import { config } from '../config'
export default class AuthService {
  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))
  }

  signUp(userName, userPassword) {
    return fetch(config.server.HTTP_API_ENDPOINT + '/users', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify({ 'user_name': userName, 'user_password': userPassword })
    })
  }
  
  login(userName, userPassword) {
    var user = {}
    user.authData = window.btoa(userName + ':' + userPassword)
    return fetch(
      config.server.HTTP_API_ENDPOINT + '/users/home', {
        headers: { 'Authorization': 'Basic ' + user.authData }
      }
    ).then(
      response => {
        if (response.status === 200) {
          this.currentUser = user
          localStorage.setItem('user', JSON.stringify(user))
        }
        return response
      }
    )
  }
  
  logout() {
    localStorage.removeItem('user')
  }
  
  getCurrentUser = () => {
    const fromLocalStorage = JSON.parse(localStorage.getItem('user'))
    return fromLocalStorage != null ? fromLocalStorage : this.currentUser
  }
}
