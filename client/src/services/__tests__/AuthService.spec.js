import AuthService from '../AuthService';

beforeEach(() => {
    clearAuthData();
});

afterEach(() => {
    clearAuthData();
});

test('it has user after construction if user was saved', () => {
    let usr = {a: 'a'};
    setSessionUser(usr)
    let au = new AuthService();
    expect(au.getCurrentUser()).toStrictEqual(usr);
});

test('it has no user after initialization if user was not saved', () => {
    let au = new AuthService();
    expect(au.getCurrentUser()).toBeNull();
});

function clearAuthData() {
    localStorage.removeItem('user');
};

function setSessionUser(usr) {
    localStorage.setItem('user', JSON.stringify(usr));
};
