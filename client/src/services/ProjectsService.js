import { config } from '../config'

export default class ProjectsService {
  constructor(authService) {
    this.authService = authService
  }
  
  newProject(newProjectLink, pathname, projectName, description, projectSettings) {
    let u = this.authService.getCurrentUser()
    let finalNewProjectLink = newProjectLink
      ? newProjectLink
      : (config.server.HTTP_API_ENDPOINT + pathname).replace('/add-new', '')
    let data = JSON.stringify(
      {
        'project_name': projectName,
        'description': description,
        'project_settings': projectSettings
      }
    )
    return fetch(finalNewProjectLink, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      },
      body: data
    })
  }

  projects(url, pathname) {
    let u = this.authService.getCurrentUser()
    let target = url || config.server.HTTP_API_ENDPOINT + pathname
    return fetch(target, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }

  project(uri, pathname) {
    let u = this.authService.getCurrentUser()
    let target = uri || config.server.HTTP_API_ENDPOINT + pathname
    return fetch(target, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }
}
