import { config } from '../config'

export default class UsersService {
  constructor(authService) {
    this.authService = authService
  }

  users(usersLink, pathName, searchString) {
    let u = this.authService.getCurrentUser()
    let finalUsersLink = usersLink
      ? usersLink
      : config.server.HTTP_API_ENDPOINT + pathName + searchString
    return fetch(finalUsersLink, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }

  user(userLink, pathName) {
    let u = this.authService.getCurrentUser()
    let finalUsersLink = userLink
      ? userLink
      : config.server.HTTP_API_ENDPOINT + pathName
    return fetch(finalUsersLink, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }
}
