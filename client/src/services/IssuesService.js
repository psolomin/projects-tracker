import { config } from '../config'

export default class IssuesService {
  constructor(authService) {
    this.authService = authService
  }
  
  // TODO: deal with `/new` suffix gracefully
  newIssue(newIssueLink, pathName, issueName, description) {
    let u = this.authService.getCurrentUser()
    let finalNewIssueLink = newIssueLink
      ? newIssueLink
      : (config.server.HTTP_API_ENDPOINT + pathName).replace('/add-new', '')
    return fetch(finalNewIssueLink, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      },
      body: JSON.stringify(
        {
          'issue_name': issueName,
          'description': description
        }
      )
    })
  }

  issues(issuesLink, pathName, searchString) {
    let u = this.authService.getCurrentUser()
    let finalIssuesLink = issuesLink
      ? issuesLink
      : config.server.HTTP_API_ENDPOINT + pathName + searchString
    return fetch(finalIssuesLink, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }

  issue(uri, pathname) {
    let u = this.authService.getCurrentUser()
    let target = uri || config.server.HTTP_API_ENDPOINT + pathname
    return fetch(target, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }

  updateIssueState(
    updateIssueUri,
    pathname,
    newIssueState
  ) {
    let u = this.authService.getCurrentUser()
    let failOverPathCut = pathname.replace('/update', '')
    let target = updateIssueUri || config.server.HTTP_API_ENDPOINT + failOverPathCut
    return this.issue(target, pathname).then(response => {
      if (response.status === 200) {
        let version = response.headers.get('ETag')
        return fetch(target, {
          method: 'PATCH',
          headers: {
            'If-Match': version,
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + u.authData
          },
          body: JSON.stringify({ 'new_state': newIssueState })
        })
      }
    })
  }
}
