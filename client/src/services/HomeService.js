import { config } from '../config'

export default class HomeService {
  constructor(authService) {
    this.authService = authService
  }
  
  home() {
    let u = this.authService.getCurrentUser()
    return fetch(config.server.HTTP_API_ENDPOINT + '/', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + u.authData
      }
    })
  }
}
