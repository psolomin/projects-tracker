import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter as Router } from "react-router-dom"
import AuthService from './services/AuthService'
import HomeService from './services/HomeService'
import ProjectsService from './services/ProjectsService'
import IssuesService from './services/IssuesService'
import CommentsService from './services/CommentsService'
import UsersService from './services/UsersService'

const authService = new AuthService()
const homeService = new HomeService(authService)
const projectsService = new ProjectsService(authService)
const issuesService = new IssuesService(authService)
const commentsService = new CommentsService(authService)
const usersService = new UsersService(authService)

ReactDOM.render(
  <Router>
      <App
          isAuthenticated={false}
          isAuthenticating={false}
          relatedResourceUri={null}
          authService={authService}
          homeService={homeService}
          projectsService={projectsService}
          issuesService={issuesService}
          commentsService={commentsService}
          usersService={usersService}
      />
  </Router>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
