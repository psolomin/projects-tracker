import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import { BrowserRouter } from 'react-router-dom';
import { createMemoryHistory } from "history";
import { Router } from "react-router";

import AuthService from '../services/AuthService'
import HomeService from '../services/HomeService'
import ProjectsService from '../services/ProjectsService'
import IssuesService from '../services/IssuesService'
import CommentsService from '../services/CommentsService'
import UsersService from '../services/UsersService'

const authService = new AuthService()
const homeService = new HomeService(authService)
const projectsService = new ProjectsService(authService)
const issuesService = new IssuesService(authService)
const commentsService = new CommentsService(authService)
const usersService = new UsersService(authService)

it('renders with BrowserRouter without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <BrowserRouter>
        <App
            isAuthenticated={false}
            isAuthenticating={false}
            relatedResourceUri={null}
            authService={authService}
            homeService={homeService}
            projectsService={projectsService}
            issuesService={issuesService}
            commentsService={commentsService}
            usersService={usersService}
        />
    </BrowserRouter>,
    div
  )
  ReactDOM.unmountComponentAtNode(div)
});

it('redirects to login page if there is no current user', () => {
  function getCurrentUserStub() { return null };

  const div = document.createElement('div');
  const history = createMemoryHistory();

  ReactDOM.render(
    <Router history={history}>
        <App
          isAuthenticated={false}
          isAuthenticating={false}
          authService={{getCurrentUser: getCurrentUserStub}}
          />
    </Router>,
    div
  )
  expect(history.location.pathname).toBe('/login');
  ReactDOM.unmountComponentAtNode(div);
});
