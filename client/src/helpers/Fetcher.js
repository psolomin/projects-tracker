export const getResourceRepresentation = (url) => {
  return fetch(url, { headers: { 'Content-Type': 'application/json' }}
  ).then(
    response => { if (response.status === 200) return response.json() }
  )
}

// this is executed for any request made:
export const getLinks = (bodyData) => {
  return bodyData["_links"] || []
}

export const getPage = (bodyData) => {
  return bodyData["page"] || {}
}

export const getPaginator = (bodyData) => {
  let links = getLinks(bodyData)
  let page = getPage(bodyData)

  let previousLink = links["prev"]
  let nextLink = links["next"]

  return {
    pageSize: page["size"],
    totalElements: page["total_elements"],
    totalPages: page["total_pages"],
    currentPage: page["number"] + 1,
    firstLink: links["first"]["href"],
    currentLink: links["self"]["href"],
    lastLink: links["last"]["href"],

    previousLink: previousLink ? previousLink["href"] : links["self"]["href"],
    nextLink: nextLink ? nextLink["href"] : links["self"]["href"]
  }
}

const buildTemplateProto = (templateProperties) => {
  let templateProto = []
  return templateProperties.forEach(
    function (templateProperty) {
      let protoEntry = {
        name: templateProperty.user_name,
        title: templateProperty.prompt,
        required: templateProperty.required,
        defaultValue: templateProperty.value
      }
      templateProto.push(protoEntry)
    }
  )
}

// this should be triggered by Content-Type header:
export const getFormProto = (url) => {
  getResourceRepresentation(url)
    .then(bodyData => { return bodyData._templates.default })
    .then(
      // passing data to intermediary object - proto:
      templateDetails => {
        return {
          title: templateDetails.title,
          method: templateDetails.method,
          contentType: templateDetails.content_type,
          formData: buildTemplateProto(templateDetails.properties)
        }
      }
    )
}
