# PROJECTS TRACKER

Web application (server and client) for projects' tracking

I keep contributing to this from time to time to continue learning things about Spring Boot and / or React.

## Domain and functionality

A **project** is a long-running development activity, such as "Project Phoenix". It is characterized by:

- name
- short description

An **issue** is a task that needs to be done for a project: fix a bug, add new feature, etc. An issue always exists in the context of a project. It is characterized by:

- ID
- name
- description
- creation date and an optional close date
- labels, such as `bug`, `new-feature`

An issue has a **state**. The set of available states and the set of possible transitions between states is configurable per project. Available states must include the `closed` and `archived` states. A `closed` project can always be `archived`.

An **issue** has **comments**, each comment is characterized by a short text. A comment cannot be added to an archived issue.

Issues can have zero or more **labels**. The set of allowed labels is defined per project.

## Components

- Server (Spring Boot HTTP app written in Kotlin)
- Client (React browser app written in JavaScript)

## Server

- Implements "projects and issues" backend system
- Exposes HTTP API endpoints

### Endpoints

See [endpoints documentation](server/docs/endpoints.md) for more details.

### Architecture

![server-arch](server/docs/server-architecture.png)

See [Server README](server/README.md) for more details.

## Client

- Implements GUI for "projects and issues" system

### Architecture

See [Client README](client/README.md) for more details.

## How-to

### Requirements

You will need the following to be installed:

```
$ java -version
openjdk version "1.8.0_232"
OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_232-b09)
OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.232-b09, mixed mode)

$ javac -version
javac 1.8.0_232

$ docker -v
Docker version 19.03.4, build 9013bf5

$ docker-compose -v
docker-compose version 1.24.1, build 4667896b

$ npm --version
6.11.3

$ make -v
GNU Make 3.81
Copyright (C) 2006  Free Software Foundation, Inc.
This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

This program built for i386-apple-darwin11.3.0
```

## Notes on local mode and SSL

By default, your browser will block self-signed certs. To overcome this in Chrome, you can change the setting at `chrome://flags/#allow-insecure-localhost`. There might be other settings required such that your browser accepts calls to `web.lvh.me` and `web-server.lvh.me` domains with self-signed certs.

### Build & start the system

1. Build client app bundle

```bash
cd client ; npm install ; npm run build ; cd ..
cp -avR client/build server/nginx
```

2. Start DB, 2 server nodes and reverse-proxy serving static React bundle

```bash
cd server ; docker-compose up --build -d nginx ; cd ..
```

Give the system ~ 30 seconds to boot, and type `web.lvh.me` in your browser.

### Start UI in dev mode

Same as above, then

```bash
cd client ; npm install ; npm start
```

### Stop all services

```bash
cd server ; docker-compose down -v ; cd ..
```
