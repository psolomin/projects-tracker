/*
 Create tables for the server app
*/

CREATE TABLE IF NOT EXISTS public.users (
  id varchar(64) PRIMARY KEY,
  user_name varchar(512) UNIQUE NOT NULL,
  user_password varchar(512) NOT NULL,
  created_at timestamp NOT NULL,
  is_deleted boolean NOT NULL DEFAULT false,
  user_version bigint NOT NULL  -- for optimistic locking
);

CREATE TABLE IF NOT EXISTS public.projects (
  id varchar(64) PRIMARY KEY,
  project_name varchar(512) UNIQUE NOT NULL,
  description text NOT NULL,
  created_at timestamp NOT NULL,
  creator_id varchar(64) REFERENCES public.users(id),
  is_deleted boolean NOT NULL DEFAULT false,
  project_version bigint NOT NULL  -- for optimistic locking
);

CREATE TABLE IF NOT EXISTS public.issues_states_config (
  id bigserial PRIMARY KEY,
  state_name varchar(512) NOT NULL,
  is_first boolean NOT NULL,
  parent_project_id varchar(64) REFERENCES public.projects(id),
  UNIQUE (state_name, parent_project_id) -- unique state names per project
);

-- only one state will be first for a given project
CREATE UNIQUE INDEX IF NOT EXISTS only_one_status_is_first_within_project_true_uix
ON public.issues_states_config (parent_project_id, is_first)
WHERE is_first;

CREATE TABLE IF NOT EXISTS public.issues_states_transitions_config (
  id bigserial PRIMARY KEY,
  from_state_id bigint REFERENCES public.issues_states_config(id),
  to_state_id bigint REFERENCES public.issues_states_config(id),
  parent_project_id varchar(64) REFERENCES public.projects(id),
  UNIQUE (from_state_id, to_state_id) -- there should be no duplicated transitions
);

CREATE TABLE IF NOT EXISTS public.issues_labels_config (
  id bigserial PRIMARY KEY,
  label varchar(512) NOT NULL,
  parent_project_id varchar(64) REFERENCES public.projects(id),
  UNIQUE (label, parent_project_id) -- unique issue labels per project
);

CREATE TABLE IF NOT EXISTS public.issues (
  id varchar(64) PRIMARY KEY,
  issue_name varchar(512) NOT NULL,
  description text NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  created_by varchar(64) REFERENCES public.users(id),
  updated_by varchar(64) REFERENCES public.users(id),
  parent_project_id varchar(64) REFERENCES public.projects(id),
  state_id bigint REFERENCES public.issues_states_config(id),
  is_deleted boolean NOT NULL DEFAULT false,
  issue_version bigint NOT NULL  -- for optimistic locking
);

CREATE TABLE IF NOT EXISTS public.issues_labels (
  id bigserial PRIMARY KEY,
  issue_id varchar(64) REFERENCES public.issues(id),
  label_id bigint REFERENCES public.issues_labels_config(id),
  UNIQUE (issue_id, label_id) -- unique issue labels per issue
);

CREATE TABLE IF NOT EXISTS public.issues_states_history (
  id bigserial PRIMARY KEY,
  state_id bigint REFERENCES public.issues_states_config(id),
  ts timestamp NOT NULL,
  actor varchar(64) REFERENCES public.users(id),
  parent_issue_id varchar(64) REFERENCES public.issues(id)
);

CREATE TABLE IF NOT EXISTS public.comments (
  id varchar(64) PRIMARY KEY,
  comment_body varchar(512) NOT NULL,
  created_at timestamp NOT NULL,
  creator_id varchar(64) REFERENCES public.users(id),
  parent_issue_id varchar(64) REFERENCES public.issues(id),
  is_deleted boolean NOT NULL DEFAULT false
);


/*
 Create new DB role for the server app
*/

REVOKE CREATE ON SCHEMA public FROM public;

CREATE OR REPLACE FUNCTION public.grant_privileges_to_user(_usr regrole) RETURNS VOID AS
$$
BEGIN
  EXECUTE format(
    'GRANT CONNECT ON DATABASE web_api_db TO %1$s;
     GRANT USAGE ON SCHEMA public TO %1$s;
     GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO %1$s;
     GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public TO %1$s;',
    _usr
  );
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION public.revoke_privileges_from_user(_usr regrole) RETURNS VOID AS
$$
BEGIN
  EXECUTE format(
    'REVOKE CONNECT ON DATABASE web_api_db FROM %1$s;
     REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM %1$s;
     REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM %1$s;
     REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM %1$s;',
    _usr
  );
END;
$$ language 'plpgsql';

DO $$
BEGIN
  CREATE USER web_api_user WITH PASSWORD 'gKbqSCBxofNcEYGdyIyOaWnP';
  PERFORM public.grant_privileges_to_user('web_api_user');

  -- if user already exists, re-create all privileges:
  EXCEPTION WHEN DUPLICATE_OBJECT THEN
  PERFORM public.revoke_privileges_from_user('web_api_user');
  PERFORM public.grant_privileges_to_user('web_api_user');
  RAISE NOTICE 'skipping creation of web_api_user -- already exists; privileges re-created';
END
$$;
