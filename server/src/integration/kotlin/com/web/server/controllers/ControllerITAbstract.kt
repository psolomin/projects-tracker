package com.web.server.controllers

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.web.server.constants.Uris.COMMENTS_RESOURCE_URI
import com.web.server.constants.Uris.ISSUES_RESOURCE_URI
import com.web.server.constants.Uris.PROJECTS_RESOURCE_URI
import com.web.server.constants.Uris.USERS_RESOURCE_URI
import com.web.server.controllers.requests.CreateCommentRequest
import com.web.server.controllers.requests.CreateIssueRequest
import com.web.server.controllers.requests.CreateProjectRequest
import com.web.server.controllers.requests.CreateUserRequest
import com.web.server.controllers.requests.ProjectSettingsRequest
import com.web.server.controllers.requests.UpdateIssueStateRequest
import com.web.server.repositories.CommentsRepository
import com.web.server.repositories.IssueLabelsRepository
import com.web.server.repositories.IssueStateTransitionsRepository
import com.web.server.repositories.IssueStatesRepository
import com.web.server.repositories.IssuesRepository
import com.web.server.repositories.ProjectsRepository
import com.web.server.repositories.UsersRepository
import java.net.URL
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.apache.commons.codec.binary.Base64
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.mediatype.hal.HalLinkDiscoverer
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.util.MimeType

abstract class ControllerITAbstract {
    @Autowired lateinit var restTemplate: TestRestTemplate
    @Autowired lateinit var usersRepository: UsersRepository
    @Autowired lateinit var projectsRepository: ProjectsRepository
    @Autowired lateinit var issuesRepository: IssuesRepository
    @Autowired lateinit var issueStatesRepository: IssueStatesRepository
    @Autowired lateinit var issueStateTransitionsRepository: IssueStateTransitionsRepository
    @Autowired lateinit var issueLabelsRepository: IssueLabelsRepository
    @Autowired lateinit var commentsRepository: CommentsRepository
    @Autowired lateinit var objectMapper: ObjectMapper

    protected inline fun <reified T : Any> createNewUserExchange(
        name: String,
        password: String
    ): ResponseEntity<T> {
        val httpEntity = buildHttpEntity(
            mediaType = MediaType.APPLICATION_JSON,
            body = CreateUserRequest(name, password)
        )
        return restTemplate.exchange(
            USERS_RESOURCE_URI,
            HttpMethod.POST,
            httpEntity,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> getUserAuthenticatedExchange(
        id: String,
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        url: String? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val getUserHttpRequest = buildHttpEntity<String>(mediaType = mediaType, authData = authData)
        val urlToCall = url ?: "$USERS_RESOURCE_URI/$id"
        return restTemplate.exchange(
            urlToCall,
            HttpMethod.GET,
            getUserHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> createProjectAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        projectName: String,
        projectDescription: String,
        projectSettings: ProjectSettingsRequest? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val request = CreateProjectRequest(
            projectName,
            projectDescription,
            projectSettings
        )
        val postProjectHttpRequest = buildHttpEntity<CreateProjectRequest>(
            mediaType = mediaType,
            authData = authData,
            body = request
        )
        return restTemplate.exchange(
            PROJECTS_RESOURCE_URI,
            HttpMethod.POST,
            postProjectHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> getProjectAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        projectId: String
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val getProjectHttpRequest = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        return restTemplate.exchange(
            "$PROJECTS_RESOURCE_URI/$projectId",
            HttpMethod.GET,
            getProjectHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> searchProjectsByCreatorAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        creatorId: String,
        page: Int? = null,
        size: Int? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val getProjectHttpRequest = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        val query = (
            if (page != null && size != null) {
                "creatorId=$creatorId&page=$page&size=$size"
            } else {
                "creatorId=$creatorId"
            })
        return restTemplate.exchange(
            "$PROJECTS_RESOURCE_URI/filters?$query",
            HttpMethod.GET,
            getProjectHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> getIssueAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        issueId: String,
        ifNoneMatch: String? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val getIssueHttpRequest = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData,
            ifNoneMatch = ifNoneMatch
        )
        return restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/$issueId",
            HttpMethod.GET,
            getIssueHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> createIssueAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        projectId: String,
        issueName: String,
        issueDescription: String,
        issueLabels: ArrayList<String> = ArrayList()
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val createIssueHttpRequest = buildHttpEntity<CreateIssueRequest>(
            mediaType = mediaType,
            authData = authData,
            body = CreateIssueRequest(issueName, issueDescription, issueLabels)
        )
        return restTemplate.exchange(
            "$PROJECTS_RESOURCE_URI/$projectId$ISSUES_RESOURCE_URI",
            HttpMethod.POST,
            createIssueHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> updateIssueStateAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        issueId: String,
        newState: String,
        ifMatch: String? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val updateIssueHttpRequest = buildHttpEntity<UpdateIssueStateRequest>(
            mediaType = mediaType,
            authData = authData,
            body = UpdateIssueStateRequest(newState),
            ifMatch = ifMatch
        )
        return restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/$issueId",
            HttpMethod.PATCH,
            updateIssueHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> searchIssuesByParentProjectAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        projectId: String,
        page: Int? = null,
        size: Int? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val requestEntity = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        val query = (
            if (page != null && size != null) {
                "projectId=$projectId&page=$page&size=$size"
            } else {
                "projectId=$projectId"
            })
        return restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/filters?$query",
            HttpMethod.GET,
            requestEntity,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> searchIssuesByParentProjectAndLabelsAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        projectId: String,
        labels: List<String>,
        page: Int? = null,
        size: Int? = null,
        additionalQueryParams: String? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val requestEntity = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        // TODO: find more handy way for these ifs
        val set = labels.joinToString(",")
        val query = (
            if (page != null && size != null) {
                "projectId=$projectId&labelsContain=$set&page=$page&size=$size"
            } else {
                "projectId=$projectId&labelsContain=$set"
            })
        val finalQuery = if (additionalQueryParams != null) {
            query + additionalQueryParams
        } else {
            query
        }
        return restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/filters?$finalQuery",
            HttpMethod.GET,
            requestEntity,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> searchCommentsByParentIssueAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        issueId: String,
        page: Int? = null,
        size: Int? = null
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val requestEntity = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        val query = (
            if (page != null && size != null) {
                "issueId=$issueId&page=$page&size=$size"
            } else {
                "issueId=$issueId"
            })
        return restTemplate.exchange(
            "$COMMENTS_RESOURCE_URI/filters?$query",
            HttpMethod.GET,
            requestEntity,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> createCommentAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        issueId: String,
        commentBody: String
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val createCommentHttpRequest = buildHttpEntity(
            mediaType = mediaType,
            authData = authData,
            body = CreateCommentRequest(commentBody)
        )
        return restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/$issueId$COMMENTS_RESOURCE_URI",
            HttpMethod.POST,
            createCommentHttpRequest,
            T::class.java
        )
    }

    protected inline fun <reified T : Any> getCommentAuthenticatedExchange(
        mediaType: MediaType,
        userName: String,
        userPassword: String,
        commentId: String
    ): ResponseEntity<T> {
        val authData = basicAuthHeaderValue(userName, userPassword)
        val getCommentRequest = buildHttpEntity<String>(
            mediaType = mediaType,
            authData = authData
        )
        return restTemplate.exchange(
            "$COMMENTS_RESOURCE_URI/$commentId",
            HttpMethod.GET,
            getCommentRequest,
            T::class.java
        )
    }

    protected fun prepareRestTemplate() {
        val requestFactory = HttpComponentsClientHttpRequestFactory()
        restTemplate.restTemplate.requestFactory = requestFactory
    }

    protected fun basicAuthHeaderValue(userName: String, userPassword: String): String {
        val plainCredentials = "$userName:$userPassword"
        val encodedCredentials = String(Base64.encodeBase64(plainCredentials.toByteArray()))
        return "Basic $encodedCredentials"
    }

    protected fun <T> buildHttpEntity(
        mediaType: MediaType? = MediaType.APPLICATION_JSON,
        accept: MediaType? = MediaTypes.HAL_FORMS_JSON,
        body: T? = null,
        authData: String? = null,
        ifNoneMatch: String? = null,
        ifMatch: String? = null
    ): HttpEntity<T> {
        val headers = HttpHeaders()

        if (mediaType != null)
            headers.contentType = mediaType

        if (accept != null)
            headers.accept = listOf(accept)

        if (authData != null)
            headers.add("Authorization", authData)

        if (ifNoneMatch != null)
            headers.add("If-None-Match", ifNoneMatch)

        if (ifMatch != null)
            headers.add("If-Match", ifMatch)

        return if (body != null) {
            HttpEntity(body, headers)
        } else {
            HttpEntity(headers)
        }
    }

    protected fun cleanUpAllRepos() {
        commentsRepository.deleteAll()
        issuesRepository.deleteAll()
        issueStateTransitionsRepository.deleteAll()
        issueStatesRepository.deleteAll()
        issueLabelsRepository.deleteAll()
        projectsRepository.deleteAll()
        usersRepository.deleteAll()
    }

    protected fun extractLinkWithRelationFromBody(bodyWithLinks: String, relName: String): String {
        return HalLinkDiscoverer().findLinkWithRel(relName, bodyWithLinks).get().href
    }

    protected inline fun <reified T : Any> extractResource(bodyWithResource: String): T {
        val tree = objectMapper.readTree(bodyWithResource)
        return objectMapper.treeToValue(tree, T::class.java)
    }

    protected inline fun <reified T : Any> extractListOfEmbeddedResources(
        bodyWithEmbeddedResource: String,
        collectionName: String
    ): ArrayList<T> {
        val entities = arrayListOf<T>()
        val ifHasCollection = objectMapper.readTree(bodyWithEmbeddedResource).has("_embedded")
        if (!ifHasCollection) {
            return entities
        }
        val collection = objectMapper.readTree(bodyWithEmbeddedResource).get("_embedded")
        val elems = collection.withArray(collectionName).elements()
        elems.forEach { entities.add(objectMapper.treeToValue(it, T::class.java)) }
        return entities
    }

    protected fun submitRequestBasedOnTemplate(
        bodyWithActionTemplates: String,
        userName: String? = null,
        userPassword: String? = null
    ): ResponseEntity<String> {
        // get creation URI and form to be filled:
        val creationUri = HalLinkDiscoverer().findLinkWithRel("self", bodyWithActionTemplates).get().href
        val creationForm = objectMapper.readTree(bodyWithActionTemplates).get("_templates").get("default")
        val method = creationForm.get("method").asText()
        val media = creationForm.get("content_type").asText()
        val properties = creationForm.withArray("properties").elements()
        val templateModel = mutableMapOf<String, Any>()

        val authData = (
            if (userName != null && userPassword != null) basicAuthHeaderValue(userName, userPassword)
            else null
        )

        val templateFilled = collectPropertiesDefaultValuesRecursively(
            properties,
            templateModel,
            authData = authData
        )
        val filledForm = objectMapper.writeValueAsString(templateFilled)

        // make request with filled template:
        val requestWithFilledForm = buildHttpEntity<String>(
            mediaType = MediaType.asMediaType(MimeType.valueOf(media)),
            body = filledForm,
            authData = authData
        )
        return restTemplate.exchange(
            creationUri,
            HttpMethod.resolve(method),
            requestWithFilledForm,
            String::class.java
        )
    }

    protected fun fetchResourceRepresentationByUri(uri: String, authData: String? = null): ResponseEntity<String> {
        return restTemplate.exchange(
            uri,
            HttpMethod.GET,
            buildHttpEntity<String>(authData = authData),
            String::class.java
        )
    }

    private fun collectPropertiesDefaultValuesRecursively(
        propertiesObject: Iterator<JsonNode>,
        defaultValues: MutableMap<String, Any>,
        nestingLevel: Int = 0,
        authData: String? = null
    ): MutableMap<String, Any> {
        if (nestingLevel > 10)
            throw RuntimeException("Recursion is too deep.")
        propertiesObject.forEach {
            // if the property is an URI, fetch the content:
            if (it.has("templated") && (it.get("templated")?.asBoolean() == true)) {
                val body = fetchResourceRepresentationByUri(it.get("value").asText(), authData).body
                val tree = objectMapper.readTree(body)
                // if received an array, add the notion of array to the template:
                val embeddedDefaultsModel = mutableMapOf<String, Any>()
                if (tree.has("_embedded")) {
                    val embeddedProperties = tree
                        .get("_embedded")
                        .get("templateList")[0]
                        .get("properties")
                        .elements()
                    val embeddedDefaults = collectPropertiesDefaultValuesRecursively(
                        embeddedProperties,
                        embeddedDefaultsModel,
                        nestingLevel + 1,
                        authData
                    )
                    defaultValues[it.get("name").asText()] = arrayListOf(embeddedDefaults)
                } else {
                    val properties = tree.get("properties").elements()
                    val nestedDefaults = collectPropertiesDefaultValuesRecursively(
                        properties,
                        embeddedDefaultsModel,
                        nestingLevel + 1,
                        authData
                    )
                    defaultValues[it.get("name").asText()] = nestedDefaults
                }
            } else {
                defaultValues[it.get("name").asText()] = it.get("value").asText()
            }
        }
        return defaultValues
    }

    protected fun getPathFromHref(href: String): String = URL(href).path!!

    protected fun <T> assertUnauthorizedResponseValidity(responseEntity: ResponseEntity<T>) {
        // status code should be UNAUTHORIZED:
        assertEquals(expected = HttpStatus.UNAUTHORIZED, actual = responseEntity.statusCode)

        // response should carry auth methods supported:
        val authHeaderData = responseEntity.headers.getFirst(HttpHeaders.WWW_AUTHENTICATE)!!
        assertTrue(authHeaderData.contains("Basic"))
    }
}
