package com.web.server.controllers

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.constants.LinkRelations.ISSUE_COMMENTS
import com.web.server.constants.LinkRelations.ISSUE_CREATED_BY_USER
import com.web.server.constants.LinkRelations.ISSUE_PARENT_PROJECT
import com.web.server.constants.LinkRelations.ISSUE_UPDATED_BY_USER
import com.web.server.controllers.advices.APIError
import com.web.server.controllers.requests.ProjectSettingsRequest
import com.web.server.controllers.views.CommentView
import com.web.server.controllers.views.IssueView
import com.web.server.controllers.views.ProjectView
import com.web.server.controllers.views.UserView
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IssuesResourceControllerIT : ControllerITAbstract() {
    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val EXISTING_USER_PASS = "pass"
        const val UPDATER_USER_NAME = "Updater"
        const val UPDATER_USER_PASS = "pass"
        const val NON_EXISTING_PROJECT_ID = "non-existing-project-id"
        const val NON_EXISTING_ISSUE_ID = "non-existing-issue-id"
    }

    private lateinit var creator: UserView
    private lateinit var updater: UserView
    private lateinit var existingProjectDefaultSettings: ProjectView
    private lateinit var existingProjectWithLabels: ProjectView

    @Before
    fun prepareCurrentTestSet() {

        prepareRestTemplate()
        cleanUpAllRepos()

        creator = createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS).body!!
        updater = createNewUserExchange<UserView>(UPDATER_USER_NAME, UPDATER_USER_PASS).body!!

        existingProjectDefaultSettings = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        ).body!!

        existingProjectWithLabels = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project with labels",
            "web project desc with labels",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                ),
                arrayListOf(
                    IssueLabelSetting("bug"),
                    IssueLabelSetting("feature"),
                    IssueLabelSetting("enhancement")
                )
            )
        ).body!!
    }

    @After
    fun tearDown() {
        cleanUpAllRepos()
    }

    @Test
    fun `request of issue creation returns correct HTTP response with issue representation`() {
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        )
        val issueCreatedView = response.body!!
        assertEquals(expected = HttpStatus.CREATED, actual = response.statusCode)
        assertEquals(expected = "My issue", actual = issueCreatedView.issueName)
        assertEquals(expected = "My issue description", actual = issueCreatedView.description)
        assertEquals(expected = creator.id, actual = issueCreatedView.createdBy)
    }

    @Test
    fun `issue requested with If-None-Match header equal to current version returns NOT MODIFIED`() {
        // create issue (auth needed):
        val createResponse = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        )
        val issueId = createResponse.body!!.id
        val issueCreatedEtag = createResponse.headers.eTag!!
        val getResponse = getIssueAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            issueCreatedEtag
        )
        assertEquals(expected = HttpStatus.NOT_MODIFIED, actual = getResponse.statusCode)
    }

    @Test
    fun `requesting non-existing issue returns NOT FOUND`() {
        // try to get some non-existing issue:
        val response = getIssueAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_ISSUE_ID
        )
        val expectedErrorInfo = APIError("Issue ID = $NON_EXISTING_ISSUE_ID does not exist.")
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = response.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = response.body)
    }

    @Test
    fun `request of issue creation for non-existing project returns NOT FOUND`() {
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_PROJECT_ID,
            "My issue",
            "My issue description"
        )
        val expectedErrorInfo = APIError("Project ID or NAME $NON_EXISTING_PROJECT_ID does not exist.")
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = response.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = response.body)
    }

    @Test
    fun `valid request of state updating of an issue returns HTTP response with issue representation`() {
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        )
        val issueId = response.body!!.id
        val eTag = response.headers.eTag
        val patchResponse = updateIssueStateAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "archived",
            eTag
        )
        assertEquals(expected = HttpStatus.OK, actual = patchResponse.statusCode)
        assertEquals(expected = "archived", actual = patchResponse.body!!.state)
        assertEquals(expected = creator.id, actual = patchResponse.body!!.updatedBy)
        assertTrue(patchResponse.body!!.createdAt <= patchResponse.body!!.updatedAt)
        assertTrue(patchResponse.headers.eTag != eTag)
    }

    @Test
    fun `issue can not be updated without eTag sent by the client`() {
        // create project (auth needed):
        val projectId = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "NEW web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                )
            )
        ).body!!.id
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description"
        )
        // issue is attempted to be updated without eTag:
        val issueId = response.body!!.id
        val patchResponseWithoutETag = updateIssueStateAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "closed"
        )
        val expectedError = APIError("Required header If-Match not found or invalid.")
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = patchResponseWithoutETag.statusCode)
        assertEquals(expected = expectedError, actual = patchResponseWithoutETag.body)
    }

    @Test
    fun `concurrent issue state updates are blocked`() {
        // create project (auth needed):
        val projectId = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "NEW web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                )
            )
        ).body!!.id
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description"
        )
        val eTag = response.headers.eTag
        // issue is updated
        val issueId = response.body!!.id
        updateIssueStateAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "closed",
            eTag
        )
        val patchResponseTwo = updateIssueStateAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "archived",
            eTag
        )
        assertEquals(expected = HttpStatus.PRECONDITION_FAILED, actual = patchResponseTwo.statusCode)
    }

    @Test
    fun `attempt to set issue state to non-existing state returns client error`() {
        // create issue (auth needed):
        val projectId = existingProjectDefaultSettings.id
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description"
        )
        val issueId = response.body!!.id
        val eTag = response.headers.eTag
        val patchResponse = updateIssueStateAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "lol",
            ifMatch = eTag
        )
        val expectedErrorInfo = APIError("Issue state \"lol\" does not exist for project ID = $projectId.")
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = patchResponse.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = patchResponse.body)
    }

    @Test
    fun `attempt to set issue state without allowing project settings returns client error`() {
        // create project (auth needed):
        val projectId = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "NEW web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                )
            )
        ).body!!.id
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description"
        )
        val issueId = response.body!!.id
        val eTag = response.headers.eTag
        val patchResponse = updateIssueStateAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "archived",
            eTag
        )
        val expectedErrorInfo = APIError(
            "Issues of project ID = $projectId can not change state from \"open\" to \"archived\"."
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = patchResponse.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = patchResponse.body)
    }

    @Test
    fun `issue created HTTP response carries links to self and to creator and to parent project`() {
        // create issue (auth needed):
        val responseBody = createIssueAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        ).body!!
        // add a comment to it:
        val issueId = extractResource<IssueView>(responseBody).id
        createCommentAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "Comment 01"
        ).body!!

        // try browsing links which issue resource brings:
        val selfURI = extractLinkWithRelationFromBody(responseBody, IanaLinkRelations.SELF.value())
        val creatorURI = extractLinkWithRelationFromBody(responseBody, ISSUE_CREATED_BY_USER.relName)
        val parentProjectURI = extractLinkWithRelationFromBody(responseBody, ISSUE_PARENT_PROJECT.relName)
        val commentsURI = extractLinkWithRelationFromBody(responseBody, ISSUE_COMMENTS.relName)

        val selfResponse = fetchResourceRepresentationByUri(
            selfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val creatorResponse = fetchResourceRepresentationByUri(
            creatorURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val parentProjectResponse = fetchResourceRepresentationByUri(
            parentProjectURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val commentsResponse = fetchResourceRepresentationByUri(
            commentsURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = creatorResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = parentProjectResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = commentsResponse.statusCode)

        val selfEntity = extractResource<IssueView>(selfResponse.body!!)
        val creatorEntity = extractResource<UserView>(creatorResponse.body!!)
        val parentProjectEntity = extractResource<ProjectView>(parentProjectResponse.body!!)
        val commentsEntities = extractListOfEmbeddedResources<CommentView>(
            commentsResponse.body!!,
            "comments"
        )

        assertEquals(expected = "My issue", actual = selfEntity.issueName)
        assertEquals(expected = EXISTING_USER_NAME, actual = creatorEntity.userName)
        assertEquals(expected = existingProjectDefaultSettings.projectName, actual = parentProjectEntity.projectName)
        assertEquals(expected = "Comment 01", actual = commentsEntities[0].commentBody)
    }

    @Test
    fun `issue updated HTTP response carries link to user who last updated the issue`() {
        // create issue (auth needed):
        val issueCreatedView = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        )

        // update it (different user):
        val issueId = issueCreatedView.body!!.id
        val eTag = issueCreatedView.headers.eTag
        val issueUpdatedResponse = updateIssueStateAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            UPDATER_USER_NAME,
            UPDATER_USER_PASS,
            issueId,
            "archived",
            eTag
        )

        val actualUpdaterURI = extractLinkWithRelationFromBody(
            issueUpdatedResponse.body!!,
            ISSUE_UPDATED_BY_USER.relName
        )
        val updaterResponse = fetchResourceRepresentationByUri(
            actualUpdaterURI,
            basicAuthHeaderValue(UPDATER_USER_NAME, UPDATER_USER_PASS)
        )
        assertEquals(expected = HttpStatus.OK, actual = updaterResponse.statusCode)
        val updaterEntity = extractResource<UserView>(updaterResponse.body!!)
        assertEquals(expected = UPDATER_USER_NAME, actual = updaterEntity.userName)
    }

    @Test
    fun `attempt to search for issues of non-existing project returns OK with empty list`() {
        // try searching for issues by a given project ID which does not exist:
        val response = searchIssuesByParentProjectAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_PROJECT_ID
        )
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<IssueView>(
            response.body!!,
            "issues"
        )
        assertEquals(expected = 0, actual = collection.size)
    }

    @Test
    fun `given existing project ID and two existing issues for it, paged search gives two issues`() {
        val projectId = existingProjectDefaultSettings.id
        // phase 2 - create two issues
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 01",
            "Bug 01 desc"
        )
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 02",
            "Bug 02 desc"
        )
        // phase 3 - get page of issues:
        val response = searchIssuesByParentProjectAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            page = 0,
            size = 1
        )
        // check contents of the first page:
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<IssueView>(
            response.body!!,
            "issues"
        )
        assertEquals(expected = 1, actual = collection.size)

        val firstEmbeddedIssue = collection[0]
        assertEquals(expected = "Bug 01", actual = firstEmbeddedIssue.issueName)

        // phase 4 - try to go to pages' links and extract content:
        val actualSelfURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.SELF.value())
        val actualFirstURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.FIRST.value())
        val actualNextURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.NEXT.value())

        val selfResponse = fetchResourceRepresentationByUri(
            actualSelfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val firstPageResponse = fetchResourceRepresentationByUri(
            actualFirstURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val nextPageResponse = fetchResourceRepresentationByUri(
            actualNextURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = firstPageResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = nextPageResponse.statusCode)

        val selfPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(selfResponse.body!!, "issues")

        val firstPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(firstPageResponse.body!!, "issues")

        val nextPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(nextPageResponse.body!!, "issues")

        assertEquals(expected = "Bug 01", actual = selfPageEmbedded[0].issueName)
        assertEquals(expected = "Bug 01", actual = firstPageEmbedded[0].issueName)
        assertEquals(expected = "Bug 02", actual = nextPageEmbedded[0].issueName)
    }

    @Test
    fun `request to create issue with supported labels returns HTTP response with issue representation`() {
        // create project (auth needed):
        val projectId = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "NEW web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                ),
                arrayListOf(
                    IssueLabelSetting("bug"),
                    IssueLabelSetting("feature")
                )
            )
        ).body!!.id
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description",
            arrayListOf("bug", "feature")
        )
        assertEquals(expected = HttpStatus.CREATED, actual = response.statusCode)
        assertEquals(expected = sortedSetOf("bug", "feature"), actual = response.body!!.labels)
    }

    @Test
    fun `request to create issue with unsupported label returns client error`() {
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectWithLabels.id,
            "My issue",
            "My issue description",
            arrayListOf("foo")
        )
        val expectedErrorInfo = APIError(
            "Labels [foo] are not defiled for project ID = ${existingProjectWithLabels.id}."
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = response.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = response.body!!)
    }

    @Test
    fun `request to create issue with duplicated labels returns created issue with de-duplicated labels`() {
        // create project (auth needed):
        val projectId = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "NEW web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                ),
                arrayListOf(
                    IssueLabelSetting("bug"),
                    IssueLabelSetting("feature")
                )
            )
        ).body!!.id
        // create issue (auth needed):
        val response = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "My issue",
            "My issue description",
            arrayListOf("bug", "bug", "bug")
        )
        assertEquals(expected = HttpStatus.CREATED, actual = response.statusCode)
        assertEquals(expected = sortedSetOf("bug"), actual = response.body!!.labels)
    }

    @Test
    fun `given existing project ID and existing issues for it paged search with labels gives correct results`() {
        val projectId = existingProjectWithLabels.id
        // phase 2 - create three issues
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 01",
            "Bug 01 desc",
            arrayListOf("feature")
        )
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 02",
            "Bug 02 desc",
            arrayListOf("bug")
        )
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 03",
            "Bug 03 desc",
            arrayListOf("feature", "enhancement")
        )
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Bug 04",
            "Bug 04 desc",
            arrayListOf("enhancement")
        )
        // phase 3 - get page of issues:
        val response = searchIssuesByParentProjectAndLabelsAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            arrayListOf("feature"),
            page = 0,
            size = 1
        )
        // check contents of the first page:
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<IssueView>(
            response.body!!,
            "issues"
        )
        assertEquals(expected = 1, actual = collection.size)

        val firstEmbeddedIssue = collection[0]
        assertEquals(expected = "Bug 01", actual = firstEmbeddedIssue.issueName)

        // phase 4 - try to go to pages' links and extract content:
        val actualSelfURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.SELF.value())
        val actualFirstURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.FIRST.value())
        val actualNextURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.NEXT.value())
        val actualLastURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.LAST.value())

        val selfResponse = fetchResourceRepresentationByUri(
            actualSelfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val firstPageResponse = fetchResourceRepresentationByUri(
            actualFirstURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val nextPageResponse = fetchResourceRepresentationByUri(
            actualNextURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val lastPageResponse = fetchResourceRepresentationByUri(
            actualLastURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = firstPageResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = nextPageResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = lastPageResponse.statusCode)

        val selfPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(selfResponse.body!!, "issues")
        val firstPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(firstPageResponse.body!!, "issues")
        val nextPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(nextPageResponse.body!!, "issues")
        val lastPageEmbedded =
            extractListOfEmbeddedResources<IssueView>(lastPageResponse.body!!, "issues")

        assertEquals(expected = "Bug 01", actual = selfPageEmbedded[0].issueName)
        assertEquals(expected = "Bug 01", actual = firstPageEmbedded[0].issueName)
        assertEquals(expected = "Bug 03", actual = nextPageEmbedded[0].issueName)
        assertEquals(expected = "Bug 03", actual = lastPageEmbedded[0].issueName)
    }

    @Test
    fun `passing invalid query string in URL gives BAD REQUEST`() {
        // phase 3 - get page of issues:
        val response = searchIssuesByParentProjectAndLabelsAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectWithLabels.id,
            arrayListOf("blah foo"),
            page = 0,
            size = 1,
            additionalQueryParams = "&sort=blah desc"
        )
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = response.statusCode)
    }
}
