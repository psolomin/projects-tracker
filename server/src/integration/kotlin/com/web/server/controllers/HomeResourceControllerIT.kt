package com.web.server.controllers

import com.web.server.constants.LinkRelations
import com.web.server.controllers.views.ProjectView
import com.web.server.controllers.views.UserView
import kotlin.test.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HomeResourceControllerIT : ControllerITAbstract() {
    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val EXISTING_USER_PASS = "pass"
    }

    private lateinit var existingUser: UserView
    private lateinit var existingProjectDefaultSettings: ProjectView

    @Before
    fun prepareCurrentTestSet() {

        prepareRestTemplate()
        cleanUpAllRepos()

        existingUser = createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS).body!!

        existingProjectDefaultSettings = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        ).body!!
    }

    @After
    fun tearDown() {
        cleanUpAllRepos()
    }

    @Test
    fun `home resource brings valid links to other resources`() {
        val homeRequest = buildHttpEntity<String>(mediaType = MediaType.APPLICATION_JSON)
        val responseBody = restTemplate.exchange(
            "/",
            HttpMethod.GET,
            homeRequest,
            String::class.java
        ).body!!

        // try browsing links:
        val selfURI = extractLinkWithRelationFromBody(responseBody, IanaLinkRelations.SELF.value())
        val usersURI = extractLinkWithRelationFromBody(responseBody, LinkRelations.ALL_USERS.relName)
        val projectsURI = extractLinkWithRelationFromBody(responseBody, LinkRelations.ALL_PROJECTS.relName)
        val issuesURI = extractLinkWithRelationFromBody(responseBody, LinkRelations.ALL_ISSUES.relName)
        val commentsURI = extractLinkWithRelationFromBody(responseBody, LinkRelations.ALL_COMMENTS.relName)

        val selfResponse = fetchResourceRepresentationByUri(
            selfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val usersResponse = fetchResourceRepresentationByUri(
            usersURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val projectsResponse = fetchResourceRepresentationByUri(
            projectsURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        val issuesResponse = fetchResourceRepresentationByUri(
            issuesURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        val commentsResponse = fetchResourceRepresentationByUri(
            commentsURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = usersResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = projectsResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = issuesResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = commentsResponse.statusCode)
    }
}
