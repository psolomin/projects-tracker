package com.web.server.controllers

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.constants.LinkRelations.PROJECT_CREATED_BY_USER
import com.web.server.constants.LinkRelations.PROJECT_ISSUES
import com.web.server.constants.Uris.PROJECTS_RESOURCE_URI
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_ALLOWED_SYMBOLS
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MAX_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MIN_LENGTH
import com.web.server.controllers.advices.APIError
import com.web.server.controllers.requests.ProjectSettingsRequest
import com.web.server.controllers.views.IssueView
import com.web.server.controllers.views.ProjectView
import com.web.server.controllers.views.UserView
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.hateoas.MediaTypes
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProjectsResourceControllerIT : ControllerITAbstract() {
    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val EXISTING_USER_PASS = "pass"
        const val NON_EXISTING_PROJECT_ID = "non-existing-project-id"
        const val EXPECTED_TEXT_SIZE_ERR_MSG = "Name and description must have at least $TEXT_INPUT_MIN_LENGTH " +
            "and at most $TEXT_INPUT_MAX_LENGTH characters."
        const val NON_EXISTING_USER_ID = "non-existing-user-id"
    }

    private lateinit var existingUserView: ResponseEntity<UserView>

    @Before
    fun prepareCurrentTestSet() {
        prepareRestTemplate()
        cleanUpAllRepos()
        existingUserView = createNewUserExchange(EXISTING_USER_NAME, EXISTING_USER_PASS)
    }

    @After
    fun tearDown() {
        cleanUpAllRepos()
    }

    @Test
    fun `projects home endpoint returns correct instructions on how to create a project`() {
        // request projects home:
        val request = buildHttpEntity<String>(
            accept = MediaTypes.HAL_FORMS_JSON,
            authData = basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val response = restTemplate.exchange(
            PROJECTS_RESOURCE_URI,
            HttpMethod.GET,
            request,
            String::class.java
        )
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        // extract project creation form and submit it, getting back the response:
        val formSubmissionResponse = submitRequestBasedOnTemplate(
            response.body!!,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS
        )
        assertEquals(expected = HttpStatus.CREATED, actual = formSubmissionResponse.statusCode)
    }

    @Test
    fun `request of project creation returns correct HTTP response with project representation`() {
        val testStartTs = Instant.now()
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        )
        assertEquals(expected = HttpStatus.CREATED, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = "web project", actual = projectCreatedResponse.body!!.projectName)
        assertEquals(expected = "web project desc", actual = projectCreatedResponse.body!!.description)
        assertEquals(expected = existingUserView.body!!.id, actual = projectCreatedResponse.body!!.creator)
        assertTrue(projectCreatedResponse.body!!.createdAt >= testStartTs)
    }

    @Test
    fun `project creation response brings links to other relevant resources`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        ).body!!

        // create issue:
        val projectId = extractResource<ProjectView>(projectCreatedResponse).id
        createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectId,
            "Issue 01",
            "Issue desc"
        )

        // try browsing links which project resource brings:
        val selfUri = extractLinkWithRelationFromBody(projectCreatedResponse, IanaLinkRelations.SELF.value())
        val creatorUri = extractLinkWithRelationFromBody(projectCreatedResponse, PROJECT_CREATED_BY_USER.relName)
        val issuesUri = extractLinkWithRelationFromBody(projectCreatedResponse, PROJECT_ISSUES.relName)

        val selfResponse = fetchResourceRepresentationByUri(
            selfUri,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val creatorResponse = fetchResourceRepresentationByUri(
            creatorUri,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val projectIssuesResponse = fetchResourceRepresentationByUri(
            issuesUri,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = creatorResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = projectIssuesResponse.statusCode)

        val selfEntity = extractResource<ProjectView>(selfResponse.body!!)
        val creatorEntity = extractResource<UserView>(creatorResponse.body!!)
        val issuesEntities = extractListOfEmbeddedResources<IssueView>(
            projectIssuesResponse.body!!,
            "issues"
        )

        assertEquals(expected = "web project", actual = selfEntity.projectName)
        assertEquals(expected = EXISTING_USER_NAME, actual = creatorEntity.userName)
        assertEquals(expected = "Issue 01", actual = issuesEntities[0].issueName)
    }

    @Test
    fun `request of project creation with valid project settings returns CREATED`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("custom 1", true),
                    IssueStateSetting("custom 2", false)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("custom 1", "custom 2"),
                    IssueStateTransitionSetting("custom 2", "closed")
                )
            )
        )
        assertEquals(expected = HttpStatus.CREATED, actual = projectCreatedResponse.statusCode)
    }

    @Test
    fun `request of project creation with unreachable issue states returns client error`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true),
                    IssueStateSetting("unreachable", false)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed")
                )
            )
        )
        val expectedError = APIError("Unreachable state: \"unreachable\"")
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = expectedError, actual = projectCreatedResponse.body)
    }

    @Test
    fun `request of project creation with transitions to non-existing issue states returns client error`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed"),
                    IssueStateTransitionSetting("open", "non-existing")
                )
            )
        )
        val expectedError = APIError("Transition to non-existing state: no such vertex in graph: non-existing")
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = expectedError, actual = projectCreatedResponse.body)
    }

    @Test
    fun `request of project creation with issue states with no path to defaul last state returns client error`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(
                arrayListOf(
                    IssueStateSetting("open", true),
                    IssueStateSetting("block-forever", false)
                ),
                arrayListOf(
                    IssueStateTransitionSetting("open", "closed"),
                    IssueStateTransitionSetting("open", "block-forever")
                )
            )
        )
        val lastDefaultState = DEFAULT_LAST_ISSUE_STATE.name
        val expectedError = APIError(
            "No set of transitions from state: \"block-forever\" to state \"$lastDefaultState\"."
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = expectedError, actual = projectCreatedResponse.body)
    }

    @Test
    fun `project can be retrieved after creation`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        )
        val projectRetrievedResponse = getProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            projectCreatedResponse.body!!.id
        )
        assertEquals(expected = HttpStatus.OK, actual = projectRetrievedResponse.statusCode)
        assertEquals(expected = projectCreatedResponse.body!!, actual = projectRetrievedResponse.body!!)
    }

    @Test
    fun `attempt to get non-existing project returns NOT FOUND`() {
        // try to get a project with existing user auth:
        val getProjectResponse = getProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_PROJECT_ID
        )
        val expectedErrorInfo = APIError("Project ID or NAME $NON_EXISTING_PROJECT_ID does not exist.")
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = getProjectResponse.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = getProjectResponse.body)
    }

    @Test
    fun `given existing creator ID and two existing projects he (she) created, paged search gives two projects`() {
        val creatorId = existingUserView.body!!.id
        // phase 2 - create two projects with default configuration
        createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project 01",
            "web project desc 01"
        )
        createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project 02",
            "web project desc 02"
        )
        // phase 3 - get page of projects:
        val response = searchProjectsByCreatorAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            creatorId,
            page = 0,
            size = 1
        )
        // check contents of the first page:
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<ProjectView>(
            response.body!!,
            "projects"
        )
        assertEquals(expected = 1, actual = collection.size)

        val firstEmbeddedProject = collection[0]
        assertEquals(expected = "web project 01", actual = firstEmbeddedProject.projectName)

        // phase 4 - try to go to pages' links and extract content:
        val actualSelfURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.SELF.value())
        val actualFirstURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.FIRST.value())
        val actualNextURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.NEXT.value())

        val selfResponse = fetchResourceRepresentationByUri(
            actualSelfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val firstPageResponse = fetchResourceRepresentationByUri(
            actualFirstURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val nextPageResponse = fetchResourceRepresentationByUri(
            actualNextURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = firstPageResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = nextPageResponse.statusCode)

        val selfPageEmbedded =
            extractListOfEmbeddedResources<ProjectView>(selfResponse.body!!, "projects")

        val firstPageEmbedded =
            extractListOfEmbeddedResources<ProjectView>(firstPageResponse.body!!, "projects")

        val nextPageEmbedded =
            extractListOfEmbeddedResources<ProjectView>(nextPageResponse.body!!, "projects")

        assertEquals(expected = "web project 01", actual = selfPageEmbedded[0].projectName)
        assertEquals(expected = "web project 01", actual = firstPageEmbedded[0].projectName)
        assertEquals(expected = "web project 02", actual = nextPageEmbedded[0].projectName)
    }

    @Test
    fun `attempt to search for projects created by non-existing user returns OK with empty array`() {
        // try searching for projects by a given user ID which does not exist:
        val response = searchProjectsByCreatorAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_USER_ID
        )
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<ProjectView>(
            response.body!!,
            "projects"
        )
        assertEquals(expected = 0, actual = collection.size)
    }

    @Test
    fun `attempt to create project with same name twice returns BAD REQUEST`() {
        // create project with some name:
        val sameProjectName = "web project"
        createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            sameProjectName,
            "web project desc"
        )
        // phase 3 - try to create project with same name:
        val response = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            sameProjectName,
            "web project desc"
        )
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = response.statusCode)
        assertEquals(expected = APIError("Project with name $sameProjectName already exists."), actual = response.body)
    }

    @Test
    fun `request of project creation with duplicated labels returns client error`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(
                issueLabels = arrayListOf(
                    IssueLabelSetting("abc"),
                    IssueLabelSetting("abc")
                )
            )
        )
        val expectedError = APIError("Duplicated labels found.")
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = expectedError, actual = projectCreatedResponse.body)
    }

    @Test
    fun `creating a project with non-alphanumeric labels returns client error`() {
        // create project (auth needed):
        val projectCreatedResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc",
            ProjectSettingsRequest(issueLabels = arrayListOf(IssueLabelSetting(":-)")))
        )
        val expectedError = APIError("Only $ISSUE_LABELS_ALLOWED_SYMBOLS are allowed for labels.")
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = projectCreatedResponse.statusCode)
        assertEquals(expected = expectedError, actual = projectCreatedResponse.body)
    }

    @Test
    fun `creating a project with name beyond text input limits returns client error`() {
        val tooSmallNameProjectResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "o".repeat((TEXT_INPUT_MIN_LENGTH - 1).toInt()),
            "web project desc"
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = tooSmallNameProjectResponse.statusCode)
        assertEquals(expected = APIError(EXPECTED_TEXT_SIZE_ERR_MSG), actual = tooSmallNameProjectResponse.body)

        val tooBigNameProjectResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "o".repeat((TEXT_INPUT_MAX_LENGTH + 1).toInt()),
            "web project desc"
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = tooBigNameProjectResponse.statusCode)
        assertEquals(expected = APIError(EXPECTED_TEXT_SIZE_ERR_MSG), actual = tooBigNameProjectResponse.body)
    }

    @Test
    fun `creating a project with description beyond text input limits returns client error`() {
        val tooSmallDescriptionProjectResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project 02",
            "o".repeat((TEXT_INPUT_MIN_LENGTH - 1).toInt())
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = tooSmallDescriptionProjectResponse.statusCode)
        assertEquals(expected = APIError(EXPECTED_TEXT_SIZE_ERR_MSG), actual = tooSmallDescriptionProjectResponse.body)

        val tooBigDescriptionProjectResponse = createProjectAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project 02",
            "o".repeat((TEXT_INPUT_MAX_LENGTH + 1).toInt())
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = tooBigDescriptionProjectResponse.statusCode)
        assertEquals(expected = APIError(EXPECTED_TEXT_SIZE_ERR_MSG), actual = tooBigDescriptionProjectResponse.body)
    }
}
