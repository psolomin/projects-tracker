package com.web.server.controllers

import com.web.server.constants.DefaultFieldsValues
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.constants.LinkRelations.COMMENT_CREATED_BY_USER
import com.web.server.constants.LinkRelations.COMMENT_PARENT_ISSUE
import com.web.server.constants.LinkRelations.COMMENT_PARENT_PROJECT
import com.web.server.constants.Uris.COMMENTS_RESOURCE_URI
import com.web.server.constants.Uris.ISSUES_RESOURCE_URI
import com.web.server.controllers.advices.APIError
import com.web.server.controllers.views.CommentView
import com.web.server.controllers.views.IssueView
import com.web.server.controllers.views.ProjectView
import com.web.server.controllers.views.UserView
import kotlin.test.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommentsResourceControllerIT : ControllerITAbstract() {
    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val EXISTING_USER_PASS = "pass"
        const val NON_EXISTING_COMMENT_ID = "non-existing-comment-id"
        val NON_EXISTING_ISSUE_ID = DefaultFieldsValues.generateNewId()
    }

    private lateinit var creator: UserView
    private lateinit var existingProjectDefaultSettings: ProjectView
    private lateinit var existingIssue: IssueView

    @Before
    fun prepareCurrentTestSet() {

        prepareRestTemplate()
        cleanUpAllRepos()

        creator = createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS).body!!

        existingProjectDefaultSettings = createProjectAuthenticatedExchange<ProjectView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            "web project",
            "web project desc"
        ).body!!

        existingIssue = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "Bug",
            "Bug description"
        ).body!!
    }

    @After
    fun tearDown() {
        cleanUpAllRepos()
    }

    @Test
    fun `request of comment creation returns correct HTTP response with comment representation`() {
        val response = createCommentAuthenticatedExchange<CommentView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingIssue.id,
            "This is a comment"
        )
        val commentCreatedView = response.body!!
        assertEquals(expected = HttpStatus.CREATED, actual = response.statusCode)
        assertEquals(expected = "This is a comment", actual = commentCreatedView.commentBody)
        assertEquals(expected = creator.id, actual = commentCreatedView.createdBy)
        assertEquals(expected = existingIssue.id, actual = commentCreatedView.parentIssueId)
    }

    @Test
    fun `request of a non-existing comment returns NOT FOUND`() {
        val getResponse = getCommentAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_COMMENT_ID
        )
        // TODO: add more sophisticated body testing
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = getResponse.statusCode)
    }

    @Test
    fun `comment can be read after it was created`() {
        val createResponse = createCommentAuthenticatedExchange<CommentView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingIssue.id,
            "This is a comment"
        )
        val commentId = createResponse.body!!.id
        val getResponse = getCommentAuthenticatedExchange<CommentView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            commentId
        )
        assertEquals(expected = HttpStatus.OK, actual = getResponse.statusCode)
        assertEquals(expected = createResponse.body, actual = getResponse.body)
        assertEquals(expected = creator.id, actual = getResponse.body!!.createdBy)
    }

    @Test
    fun `comment state representation comes with valid links to relevant resources`() {
        val createResponseBody = createCommentAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingIssue.id,
            "This is a comment"
        ).body!!

        // try browsing links which comment state representation brings:
        val selfURI = extractLinkWithRelationFromBody(createResponseBody, IanaLinkRelations.SELF.value())
        val creatorURI = extractLinkWithRelationFromBody(createResponseBody, COMMENT_CREATED_BY_USER.relName)
        val parentIssueURI = extractLinkWithRelationFromBody(createResponseBody, COMMENT_PARENT_ISSUE.relName)
        val parentProjectURI = extractLinkWithRelationFromBody(createResponseBody, COMMENT_PARENT_PROJECT.relName)

        val selfResponse = fetchResourceRepresentationByUri(
            selfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val creatorResponse = fetchResourceRepresentationByUri(
            creatorURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val parentIssueResponse = fetchResourceRepresentationByUri(
            parentIssueURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val parentProjectResponse = fetchResourceRepresentationByUri(
            parentProjectURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = creatorResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = parentIssueResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = parentProjectResponse.statusCode)

        val selfEntity = extractResource<CommentView>(selfResponse.body!!)
        val creatorEntity = extractResource<UserView>(creatorResponse.body!!)
        val parentIssueEntity = extractResource<IssueView>(parentIssueResponse.body!!)
        val parentProjectEntity = extractResource<ProjectView>(parentProjectResponse.body!!)

        assertEquals(expected = "This is a comment", actual = selfEntity.commentBody)
        assertEquals(expected = creator, actual = creatorEntity)
        assertEquals(expected = existingIssue, actual = parentIssueEntity)
        assertEquals(expected = existingProjectDefaultSettings, actual = parentProjectEntity)
    }

    @Test
    fun `request of comment creation for non-existing issue returns NOT FOUND`() {
        val nonExistingIssueId = DefaultFieldsValues.generateNewId()
        val response = createCommentAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            nonExistingIssueId,
            "Some comment"
        )
        val expectedErrorInfo = APIError("Issue ID = $nonExistingIssueId does not exist.")
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = response.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = response.body)
    }

    @Test
    fun `request of comment creation with invalid body returns BAD REQUEST`() {
        val authData = basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        val createCommentHttpRequest = buildHttpEntity(
            mediaType = MediaType.APPLICATION_JSON,
            authData = authData,
            body = "{"
        )
        val errorResponse = restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/${existingIssue.id}$COMMENTS_RESOURCE_URI",
            HttpMethod.POST,
            createCommentHttpRequest,
            APIError::class.java
        )
        // TODO: improve representation of the error
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = errorResponse.statusCode)
    }

    @Test
    fun `request of comment creation without body returns BAD REQUEST`() {
        val authData = basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        val createCommentHttpRequest = buildHttpEntity(
            mediaType = MediaType.APPLICATION_JSON,
            authData = authData,
            body = null
        )
        val errorResponse = restTemplate.exchange(
            "$ISSUES_RESOURCE_URI/${existingIssue.id}$COMMENTS_RESOURCE_URI",
            HttpMethod.POST,
            createCommentHttpRequest,
            APIError::class.java
        )
        // TODO: improve representation of the error
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = errorResponse.statusCode)
    }

    @Test
    fun `request of comment creation when issue state does not allow such returns client error`() {
        val issueCreatedResponse = createIssueAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            existingProjectDefaultSettings.id,
            "My issue",
            "My issue description"
        )
        val issueId = issueCreatedResponse.body!!.id
        val eTag = issueCreatedResponse.headers.eTag
        updateIssueStateAuthenticatedExchange<IssueView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            DEFAULT_LAST_ISSUE_STATE.name, // state not allowing new comments to be placed
            eTag
        )
        val createCommentResponse = createCommentAuthenticatedExchange<APIError>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "Some comment"
        )
        val expectedErrorInfo = APIError(
            "Comments are not allowed for issues with state == ${DEFAULT_LAST_ISSUE_STATE.name}."
        )
        assertEquals(expected = HttpStatus.UNPROCESSABLE_ENTITY, actual = createCommentResponse.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = createCommentResponse.body)
    }

    @Test
    fun `given existing issue ID and two existing comments for it, paged search gives two comments`() {
        val issueId = existingIssue.id
        // create two comments
        createCommentAuthenticatedExchange<CommentView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "Comment 01"
        )
        createCommentAuthenticatedExchange<CommentView>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            "Comment 02"
        )
        // get page of comments:
        val response = searchCommentsByParentIssueAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            issueId,
            page = 0,
            size = 1
        )
        // check contents of the first page:
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<CommentView>(
            response.body!!,
            "comments"
        )
        assertEquals(expected = 1, actual = collection.size)

        val firstEmbedded = collection[0]
        assertEquals(expected = "Comment 01", actual = firstEmbedded.commentBody)

        // try to go to pages' links and extract content:
        val actualSelfURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.SELF.value())
        val actualFirstURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.FIRST.value())
        val actualNextURI = extractLinkWithRelationFromBody(response.body!!, IanaLinkRelations.NEXT.value())

        val selfResponse = fetchResourceRepresentationByUri(
            actualSelfURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val firstPageResponse = fetchResourceRepresentationByUri(
            actualFirstURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )
        val nextPageResponse = fetchResourceRepresentationByUri(
            actualNextURI,
            basicAuthHeaderValue(EXISTING_USER_NAME, EXISTING_USER_PASS)
        )

        assertEquals(expected = HttpStatus.OK, actual = selfResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = firstPageResponse.statusCode)
        assertEquals(expected = HttpStatus.OK, actual = nextPageResponse.statusCode)

        val selfPageEmbedded =
            extractListOfEmbeddedResources<CommentView>(selfResponse.body!!, "comments")

        val firstPageEmbedded =
            extractListOfEmbeddedResources<CommentView>(firstPageResponse.body!!, "comments")

        val nextPageEmbedded =
            extractListOfEmbeddedResources<CommentView>(nextPageResponse.body!!, "comments")

        assertEquals(expected = "Comment 01", actual = selfPageEmbedded[0].commentBody)
        assertEquals(expected = "Comment 01", actual = firstPageEmbedded[0].commentBody)
        assertEquals(expected = "Comment 02", actual = nextPageEmbedded[0].commentBody)
    }

    @Test
    fun `attempt to search for comments of non-existing issue returns OK with empty list`() {
        val response = searchCommentsByParentIssueAuthenticatedExchange<String>(
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS,
            NON_EXISTING_ISSUE_ID
        )
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        val collection = extractListOfEmbeddedResources<IssueView>(
            response.body!!,
            "comments"
        )
        assertEquals(expected = 0, actual = collection.size)
    }
}
