package com.web.server.controllers

import com.web.server.constants.DefaultFieldsValues
import com.web.server.constants.Uris.USERS_RESOURCE_URI
import com.web.server.controllers.advices.APIError
import com.web.server.controllers.views.UserView
import kotlin.test.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.hateoas.MediaTypes
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UsersResourceControllerIT : ControllerITAbstract() {
    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val EXISTING_USER_PASS = "pass"
        const val NEW_USER_PASS = "pass 2"
        val NON_EXISTING_USER_ID = DefaultFieldsValues.generateNewId()
    }

    @Before
    fun prepareCurrentTestSet() {
        prepareRestTemplate()
        cleanUpAllRepos()
    }

    @After
    fun tearDown() {
        cleanUpAllRepos()
    }

    @Test
    fun `users home endpoint returns correct instructions on how to create a user without auth needed`() {
        val request = buildHttpEntity<String>(accept = MediaTypes.HAL_FORMS_JSON)
        val response = restTemplate.exchange(
            USERS_RESOURCE_URI,
            HttpMethod.GET,
            request,
            String::class.java
        )
        assertEquals(expected = HttpStatus.OK, actual = response.statusCode)
        // extract user creation form and submit it, getting back the response:
        val formSubmissionResponse = submitRequestBasedOnTemplate(response.body!!)
        assertEquals(expected = HttpStatus.CREATED, actual = formSubmissionResponse.statusCode)
    }

    @Test
    fun `authenticated retrieval of user by ID after non-authenticated creation, with expected HAL`() {
        // phase 1 - create user (no auth needed):
        val response = createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS)
        val responseBody = response.body!!
        assertEquals(expected = HttpStatus.CREATED, actual = response.statusCode)

        // phase 2 - get back and verify the user created (with auth):
        val getUserResponse = getUserAuthenticatedExchange<UserView>(
            responseBody.id,
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS
        )
        assertEquals(expected = HttpStatus.OK, actual = getUserResponse.statusCode)
        assertEquals(expected = EXISTING_USER_NAME, actual = responseBody.userName)

        // phase 3 - verify Hypermedia formats:
        val getUserResponseHal = getUserAuthenticatedExchange<String>(
            responseBody.id,
            MediaTypes.HAL_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS
        ).body!!
        val expectedSelfURI = getPathFromHref(
            "http://whatever:8989$USERS_RESOURCE_URI/${responseBody.id}"
        )
        val actualSelfURI = extractLinkWithRelationFromBody(getUserResponseHal, IanaLinkRelations.SELF.value())
        assertEquals(expected = expectedSelfURI, actual = getPathFromHref(actualSelfURI))
    }

    @Test
    fun `Attempt to GET non-existing user returns NOT FOUND`() {
        // phase 1 - create user (no auth needed):
        createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS)
        // try to get a user who is not in the DB:
        val getUserResponse = getUserAuthenticatedExchange<APIError>(
            NON_EXISTING_USER_ID,
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            EXISTING_USER_PASS
        )
        val expectedErrorInfo = APIError("User with ID or NAME $NON_EXISTING_USER_ID does not exist.")
        assertEquals(expected = HttpStatus.NOT_FOUND, actual = getUserResponse.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = getUserResponse.body)
    }

    @Test
    fun `Attempt to create user with same name twice returns BAD REQUEST`() {
        // phase 1 - create user 1:
        val sameUserName = "Mr. Mo"
        createNewUserExchange<UserView>(sameUserName, EXISTING_USER_PASS)
        // phase 2 - try to create user with same name:
        val response = createNewUserExchange<APIError>(sameUserName, NEW_USER_PASS)
        val expectedErrorInfo = APIError("User with name $sameUserName already exists.")
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = response.statusCode)
        assertEquals(expected = expectedErrorInfo, actual = response.body)
    }

    @Test
    fun `Attempt to GET an existing user with non-existing credentials pair results in UNAUTHORIZED`() {
        val myUser = createNewUserExchange<UserView>(EXISTING_USER_NAME, EXISTING_USER_PASS).body!!
        val incorrectPassword = "this-is-incorrect-password"
        val getUserResponse = getUserAuthenticatedExchange<String>(
            myUser.id,
            MediaType.APPLICATION_JSON,
            myUser.userName,
            incorrectPassword
        )
        assertUnauthorizedResponseValidity(getUserResponse)
    }

    @Test
    fun `Attempt to GET a non-existing user with non-existing credentials pair results in UNAUTHORIZED`() {
        val incorrectPassword = "this-is-incorrect-password"
        val getUserResponse = getUserAuthenticatedExchange<String>(
            NON_EXISTING_USER_ID,
            MediaType.APPLICATION_JSON,
            EXISTING_USER_NAME,
            incorrectPassword
        )
        assertUnauthorizedResponseValidity(getUserResponse)
    }

    @Test
    fun `Attempt to create user with wrong body returns BAD REQUEST`() {
        val httpEntity = buildHttpEntity(
            mediaType = MediaType.APPLICATION_JSON,
            body = "{}"
        )
        val response = restTemplate.exchange(
            USERS_RESOURCE_URI,
            HttpMethod.POST,
            httpEntity,
            APIError::class.java
        )
        assertEquals(expected = HttpStatus.BAD_REQUEST, actual = response.statusCode)
        // TODO: define nice body parsing errors
    }
}
