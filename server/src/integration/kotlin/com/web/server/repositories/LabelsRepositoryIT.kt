package com.web.server.repositories

import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LabelsRepositoryIT : RepositoryITAbstract() {
    private lateinit var creator: User
    private lateinit var projectSaved: Project

    @Before
    fun cleanAllTablesBefore() {
        cleanUpAllRepos()

        creator = usersRepository.save(CREATOR_PROTO)
        projectSaved = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )
    }

    @After
    fun cleanAllTablesAfter() {
        cleanUpAllRepos()
    }

    @Test
    fun `issue labels config can be retrieved with its parent project`() {
        val labelsProto = listOf(
            IssueLabel(label = "bug", parentProject = projectSaved),
            IssueLabel(label = "new feature", parentProject = projectSaved)
        )
        issueLabelsRepository.saveAll(labelsProto)
        val labelsRetrieved = projectsRepository.findByIdAndFetchLabelsEagerly(projectSaved.id).get().labels
        assertEquals(labelsRetrieved.map { it.parentProject }, labelsProto.map { it.parentProject })
        assertEquals(labelsRetrieved.map { it.label }, labelsProto.map { it.label })
    }

    @Test
    fun `issue labels config with duplicated label text is not allowed`() {
        val labelsProto = listOf(
            IssueLabel(label = "new feature", parentProject = projectSaved),
            IssueLabel(label = "new feature", parentProject = projectSaved)
        )
        assertFailsWith<DataIntegrityViolationException> {
            issueLabelsRepository.saveAll(labelsProto)
        }
    }

    @Test
    fun `same issue label text for different projects is allowed`() {
        val projectTwoSaved = projectsRepository.save(
            Project(projectName = "Another web project", description = "description", creator = creator)
        )
        val projectOneLabelsProto = listOf(IssueLabel(label = "new feature", parentProject = projectSaved))
        val projectTwoLabelsProto = listOf(IssueLabel(label = "new feature", parentProject = projectTwoSaved))

        issueLabelsRepository.saveAll(projectOneLabelsProto)
        issueLabelsRepository.saveAll(projectTwoLabelsProto)

        val labelsOneRetrieved = projectsRepository.findByIdAndFetchLabelsEagerly(projectSaved.id).get().labels
        assertEquals(labelsOneRetrieved.map { it.parentProject }, projectOneLabelsProto.map { it.parentProject })
        assertEquals(labelsOneRetrieved.map { it.label }, projectOneLabelsProto.map { it.label })

        val labelsTwoRetrieved = projectsRepository.findByIdAndFetchLabelsEagerly(projectTwoSaved.id).get().labels
        assertEquals(labelsTwoRetrieved.map { it.parentProject }, projectTwoLabelsProto.map { it.parentProject })
        assertEquals(labelsTwoRetrieved.map { it.label }, projectTwoLabelsProto.map { it.label })
    }

    @Test
    fun `issue labels config can be retrieved with its parent project and label text`() {
        val labelsProto = listOf(
            IssueLabel(label = "feature A", parentProject = projectSaved),
            IssueLabel(label = "feature B", parentProject = projectSaved),
            IssueLabel(label = "feature C", parentProject = projectSaved)
        )
        issueLabelsRepository.saveAll(labelsProto)
        val labelsRetrieved = issueLabelsRepository.findByParentProjectIdAndByLabels(
            projectSaved.id, listOf("feature A", "feature B", "feature B")
        )
        val expectedLabels = labelsProto.take(2)
        assertEquals(labelsRetrieved.map { it.parentProject }, expectedLabels.map { it.parentProject })
        assertEquals(labelsRetrieved.map { it.label }, expectedLabels.map { it.label })
    }
}
