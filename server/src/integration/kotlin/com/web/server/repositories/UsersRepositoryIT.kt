package com.web.server.repositories

import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.orm.ObjectOptimisticLockingFailureException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UsersRepositoryIT : RepositoryITAbstract() {

    private companion object {
        val SOME_USER = User(userName = "John Doe", userPassword = "pass")
    }

    @Before
    fun cleanAllTablesBefore() {
        usersRepository.deleteAll()
    }

    @After
    fun cleanAllTablesAfter() {
        usersRepository.deleteAll()
    }

    @Test
    fun `saved user can be successfully retrieved`() {
        val entitySaved = usersRepository.save(SOME_USER)
        val entityRetrieved = usersRepository.getOne(entitySaved.id)
        assertEquals(entityRetrieved.userName, entitySaved.userName)
        assertEquals(entityRetrieved.userPassword, entitySaved.userPassword)
        assertTrue(entityRetrieved.createdAt > TEST_START_TS)
    }

    @Test
    fun `saved user can be successfully retrieved by its name`() {
        val entitySaved = usersRepository.save(SOME_USER)
        val entityRetrieved = usersRepository.findUserByUserName(entitySaved.userName).get()
        assertEquals(entityRetrieved.userName, entitySaved.userName)
        assertEquals(entityRetrieved.userPassword, entitySaved.userPassword)
        assertTrue(entityRetrieved.createdAt > TEST_START_TS)
    }

    @Test
    fun `two users with the same user name can not be saved`() {
        val someIdenticalNameForBoth = "Mo"
        usersRepository.save(User(userName = someIdenticalNameForBoth, userPassword = "pass"))
        assertFailsWith<DataIntegrityViolationException> {
            usersRepository.save(User(userName = someIdenticalNameForBoth, userPassword = "pass"))
        }
    }

    @Test
    fun `updating same user concurrently throws exception`() {
        val createdUserId = usersRepository.save(SOME_USER).id
        val sameUserForUpdatingInstanceOne = usersRepository.findById(createdUserId).get()
        val sameUserForUpdatingInstanceTwo = usersRepository.findById(createdUserId).get()
        sameUserForUpdatingInstanceOne.userName = "new name"
        sameUserForUpdatingInstanceTwo.userName = "new name 2"
        usersRepository.save(sameUserForUpdatingInstanceOne)
        assertFailsWith<ObjectOptimisticLockingFailureException> {
            usersRepository.save(sameUserForUpdatingInstanceTwo)
        }
    }
}
