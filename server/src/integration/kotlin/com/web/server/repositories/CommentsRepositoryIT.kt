package com.web.server.repositories

import com.web.server.repositories.entities.Comment
import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.orm.ObjectOptimisticLockingFailureException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CommentsRepositoryIT : RepositoryITAbstract() {

    private lateinit var creator: User
    private lateinit var projectSaved: Project
    private lateinit var issueSaved: Issue
    private lateinit var issueState: IssueState

    @Before
    fun cleanAllTablesBefore() {
        cleanUpAllRepos()

        creator = usersRepository.save(CREATOR_PROTO)
        projectSaved = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )
        issueState = issueStatesRepository.save(
            IssueState(stateName = "one", isFirst = true, parentProject = projectSaved)
        )
        issueSaved = issuesRepository.save(
            Issue(
                issueName = "Issue 01",
                description = "desc",
                parentProject = projectSaved,
                createdBy = creator,
                state = issueState
            )
        )
    }

    @After
    fun cleanAllTablesAfter() {
        cleanUpAllRepos()
    }

    @Test
    fun `saved comment can be successfully retrieved`() {
        val comment = Comment(commentBody = "comment", creator = creator, parentIssue = issueSaved)
        val id = commentsRepository.save(comment).id
        val commentRetrieved = commentsRepository.findById(id).get()
        assertEquals(commentRetrieved.parentIssue.issueLabels, issueSaved.issueLabels)
        assertEquals(commentRetrieved.creator, creator)
        assertTrue(commentRetrieved.createdAt > TEST_START_TS)
    }

    // @Test
    // TODO:
    // it is still possible to start the transaction of adding a comment
    // which will finish AFTER the issue changes the state >>> comment will be created when
    // it already should be rejected. So, this test is failing.
    // Technically, it is possible to oblige the client to fetch Issue before posting any comment,
    // and then, on comment creation request, check the Issue state eTag.
    fun `placing comment for issue which is outdated returns exception`() {
        val savedIssueId = issueSaved.id
        val issueForUpdating = issuesRepository.findById(savedIssueId).get()
        val issueForComment = issuesRepository.findById(savedIssueId).get()
        issueForUpdating.description = "bar"
        issuesRepository.save(issueForUpdating)

        val comment = Comment(commentBody = "comment", creator = creator, parentIssue = issueForComment)
        assertFailsWith<ObjectOptimisticLockingFailureException> {
            commentsRepository.save(comment)
        }
    }

    @Test
    fun `saved comments are correctly retrieved by issue ID in pages of desired size`() {
        // save multiple comments (they can be identical):
        val comments = listOf(
            Comment(commentBody = "comment 1", creator = creator, parentIssue = issueSaved),
            Comment(commentBody = "comment 2", creator = creator, parentIssue = issueSaved),
            Comment(commentBody = "comment 3", creator = creator, parentIssue = issueSaved)
        )
        commentsRepository.saveAll(comments)

        // Retrieving pages of comments given issue ID:
        val firstPageWithTwoElements = PageRequest.of(0, 2)
        val thirdPageWithOneElement = PageRequest.of(2, 1)

        val firstPageWithTwoElementsResult =
            commentsRepository.findAllByParentIssueId(issueSaved.id, firstPageWithTwoElements)
        val thirdPageWithOneElementResult =
            commentsRepository.findAllByParentIssueId(issueSaved.id, thirdPageWithOneElement)

        assertEquals(2, firstPageWithTwoElementsResult.size)
        assertEquals(issueSaved.id, firstPageWithTwoElementsResult.content[0].parentIssue.id)
        assertEquals(comments[0].commentBody, firstPageWithTwoElementsResult.content[0].commentBody)
        assertEquals(comments[1].commentBody, firstPageWithTwoElementsResult.content[1].commentBody)

        assertEquals(1, thirdPageWithOneElementResult.size)
        assertEquals(issueSaved.id, thirdPageWithOneElementResult.content[0].parentIssue.id)
        assertEquals(comments[2].commentBody, thirdPageWithOneElementResult.content[0].commentBody)
    }

    @Test
    fun `saved comments are correctly retrieved by user ID in pages of desired size`() {
        // save multiple comments (they can be identical):
        val comments = listOf(
            Comment(commentBody = "comment", creator = creator, parentIssue = issueSaved),
            Comment(commentBody = "comment", creator = creator, parentIssue = issueSaved),
            Comment(commentBody = "comment", creator = creator, parentIssue = issueSaved)
        )
        commentsRepository.saveAll(comments)
        // Retrieving pages of comments given creator ID:
        val firstPageWithTwoElements = PageRequest.of(0, 2)
        val thirdPageWithOneElement = PageRequest.of(2, 1)

        val firstPageWithTwoElementsResult =
            commentsRepository.findAllByCreatorId(creator.id, firstPageWithTwoElements)
        val thirdPageWithOneElementResult =
            commentsRepository.findAllByCreatorId(creator.id, thirdPageWithOneElement)

        assertEquals(2, firstPageWithTwoElementsResult.size)
        assertEquals(issueSaved.id, firstPageWithTwoElementsResult.content[0].parentIssue.id)
        assertEquals(comments[0].commentBody, firstPageWithTwoElementsResult.content[0].commentBody)
        assertEquals(comments[1].commentBody, firstPageWithTwoElementsResult.content[1].commentBody)

        assertEquals(1, thirdPageWithOneElementResult.size)
        assertEquals(issueSaved.id, thirdPageWithOneElementResult.content[0].parentIssue.id)
        assertEquals(comments[2].commentBody, thirdPageWithOneElementResult.content[0].commentBody)
    }
}
