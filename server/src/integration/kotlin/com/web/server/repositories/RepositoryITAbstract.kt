package com.web.server.repositories

import com.web.server.repositories.entities.User
import java.time.Instant
import org.springframework.beans.factory.annotation.Autowired

abstract class RepositoryITAbstract {
    @Autowired lateinit var usersRepository: UsersRepository
    @Autowired lateinit var projectsRepository: ProjectsRepository
    @Autowired lateinit var issuesRepository: IssuesRepository
    @Autowired lateinit var issueStatesRepository: IssueStatesRepository
    @Autowired lateinit var issueStateTransitionsRepository: IssueStateTransitionsRepository
    @Autowired lateinit var issueLabelsRepository: IssueLabelsRepository
    @Autowired lateinit var commentsRepository: CommentsRepository

    protected companion object {
        val CREATOR_PROTO = User(userName = "John Doe", userPassword = "pass")
        val CREATOR_TWO_PROTO = User(userName = "John Doe 2", userPassword = "pass")
        val TEST_START_TS = Instant.now()!!
    }

    protected fun cleanUpAllRepos() {
        commentsRepository.deleteAll()
        issuesRepository.deleteAll()
        issueStateTransitionsRepository.deleteAll()
        issueStatesRepository.deleteAll()
        issueLabelsRepository.deleteAll()
        projectsRepository.deleteAll()
        usersRepository.deleteAll()
    }
}
