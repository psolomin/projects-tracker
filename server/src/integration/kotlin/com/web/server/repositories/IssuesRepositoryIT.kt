package com.web.server.repositories

import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.orm.ObjectOptimisticLockingFailureException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IssuesRepositoryIT : RepositoryITAbstract() {
    private lateinit var creator: User
    private lateinit var projectSaved: Project
    private lateinit var projectLabels: List<IssueLabel>
    private lateinit var stateOne: IssueState
    private lateinit var stateTwo: IssueState
    private lateinit var issuePrototypeOne: Issue
    private lateinit var issuePrototypeTwo: Issue
    private lateinit var issuePrototypeThree: Issue
    private lateinit var issuePrototypeFour: Issue

    @Before
    fun cleanAllTablesBefore() {
        cleanUpAllRepos()

        creator = usersRepository.save(CREATOR_PROTO)
        projectSaved = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )
        stateOne = issueStatesRepository.save(
            IssueState(stateName = "one", isFirst = true, parentProject = projectSaved)
        )
        stateTwo = issueStatesRepository.save(
            IssueState(stateName = "two", isFirst = false, parentProject = projectSaved)
        )
        issueStateTransitionsRepository.saveAll(
            setOf(
                IssueStateTransition(fromState = stateOne, toState = stateTwo, parentProject = projectSaved),
                IssueStateTransition(fromState = stateTwo, toState = stateOne, parentProject = projectSaved)
            )
        )

        issueLabelsRepository.saveAll(
            setOf(
                IssueLabel(label = "feature A", parentProject = projectSaved),
                IssueLabel(label = "feature B", parentProject = projectSaved),
                IssueLabel(label = "feature C", parentProject = projectSaved)
            )
        )

        projectLabels = projectsRepository.findByIdAndFetchLabelsEagerly(projectSaved.id).get().labels

        issuePrototypeOne = Issue(
            issueName = "fix bug",
            description = "quick fix",
            createdBy = creator,
            parentProject = projectSaved,
            state = stateOne,
            issueLabels = projectLabels.take(2).toSortedSet()
        )

        issuePrototypeTwo = Issue(
            issueName = "add feature",
            description = "add feature",
            createdBy = creator,
            parentProject = projectSaved,
            state = stateOne,
            issueLabels = projectLabels.take(2).toSortedSet()
        )

        issuePrototypeThree = Issue(
            issueName = "add feature three",
            description = "add feature three",
            createdBy = creator,
            parentProject = projectSaved,
            state = stateOne,
            issueLabels = projectLabels.take(2).toSortedSet()
        )

        issuePrototypeFour = Issue(
            issueName = "add feature four",
            description = "add feature four",
            createdBy = creator,
            parentProject = projectSaved,
            state = stateOne,
            issueLabels = projectLabels.takeLast(2).toSortedSet()
        )
    }

    @After
    fun cleanAllTablesAfter() {
        cleanUpAllRepos()
    }

    @Test
    fun `saved issue can be successfully retrieved`() {
        val savedIssue = issuesRepository.save(issuePrototypeOne)
        assertEquals(savedIssue.parentProject, projectSaved)
        assertEquals(savedIssue.createdBy, creator)
        assertEquals(savedIssue.createdAt, savedIssue.updatedAt)
        assertTrue(savedIssue.createdAt > TEST_START_TS)
        assertEquals(savedIssue.state, stateOne)

        val issueRetrieved = issuesRepository.findById(savedIssue.id).get()
        assertEquals(issueRetrieved.parentProject, projectSaved)
        assertEquals(issueRetrieved.createdBy, creator)
        assertEquals(issueRetrieved.createdAt, issueRetrieved.updatedAt)
        assertTrue(issueRetrieved.createdAt > TEST_START_TS)
        assertEquals(issueRetrieved.state, stateOne)
    }

    @Test
    fun `updating same issue concurrently throws exception`() {
        val id = issuesRepository.save(issuePrototypeOne).id
        val issueForUpdatingInstanceOne = issuesRepository.findById(id).get()
        val issueForUpdatingInstanceTwo = issuesRepository.findById(id).get()
        issueForUpdatingInstanceOne.description = "bar"
        issueForUpdatingInstanceTwo.description = "foo"
        issuesRepository.save(issueForUpdatingInstanceOne)
        assertFailsWith<ObjectOptimisticLockingFailureException> {
            issuesRepository.save(issueForUpdatingInstanceTwo)
        }
    }

    @Test
    fun `saved issues are correctly retrieved by project ID in pages of desired size`() {
        // save multiple issues (they can be identical):
        val issueOne = issuesRepository.save(issuePrototypeOne)
        val issueTwo = issuesRepository.save(issuePrototypeTwo)
        val issueThree = issuesRepository.save(issuePrototypeThree)

        // Retrieving pages of issues given users' IDs:
        val firstPageWithTwoElements = PageRequest.of(0, 2)
        val thirdPageWithOneElement = PageRequest.of(2, 1)

        val firstPageWithTwoElementsResultOne =
            issuesRepository.findAllByParentProjectId(projectSaved.id, firstPageWithTwoElements)
        val secondPageWithOneElementResultOne =
            issuesRepository.findAllByParentProjectId(projectSaved.id, thirdPageWithOneElement)

        assertEquals(2, firstPageWithTwoElementsResultOne.size)
        assertEquals(projectSaved.id, firstPageWithTwoElementsResultOne.content[0].parentProject.id)
        assertEquals(issueOne.issueName, firstPageWithTwoElementsResultOne.content[0].issueName)
        assertEquals(issueTwo.issueName, firstPageWithTwoElementsResultOne.content[1].issueName)

        assertEquals(1, secondPageWithOneElementResultOne.size)
        assertEquals(projectSaved.id, secondPageWithOneElementResultOne.content[0].parentProject.id)
        assertEquals(issueThree.issueName, secondPageWithOneElementResultOne.content[0].issueName)
    }

    @Test
    fun `saved issues are correctly retrieved by project ID and labels in pages of correct size`() {
        // save multiple issues (they can be identical):
        val issueOne = issuesRepository.save(issuePrototypeOne)
        val issueTwo = issuesRepository.save(issuePrototypeTwo)
        val issueThree = issuesRepository.save(issuePrototypeThree)
        val issueFour = issuesRepository.save(issuePrototypeFour)

        // Retrieving pages of issues given labels:
        val firstPageWithTwoElements = PageRequest.of(0, 5)
        val thirdPageWithOneElement = PageRequest.of(0, 5)

        val firstPageWithTwoElementsResultOne = issuesRepository.findAllByProjectIdAndByLabels(
            projectSaved.id, setOf("feature A"), firstPageWithTwoElements
        )
        val secondPageWithOneElementResultOne = issuesRepository.findAllByProjectIdAndByLabels(
            projectSaved.id, setOf("feature C"), thirdPageWithOneElement
        )

        assertEquals(3, firstPageWithTwoElementsResultOne.totalElements)
        assertEquals(projectSaved.id, firstPageWithTwoElementsResultOne.content[0].parentProject.id)
        assertEquals(issueOne.issueName, firstPageWithTwoElementsResultOne.content[0].issueName)
        assertEquals(issueTwo.issueName, firstPageWithTwoElementsResultOne.content[1].issueName)
        assertEquals(issueThree.issueName, firstPageWithTwoElementsResultOne.content[2].issueName)

        assertEquals(1, secondPageWithOneElementResultOne.totalElements)
        assertEquals(projectSaved.id, secondPageWithOneElementResultOne.content[0].parentProject.id)
        assertEquals(issueFour.issueName, secondPageWithOneElementResultOne.content[0].issueName)
    }

    @Test
    fun `updating same issue yelds correct issue update timestamp`() {
        val id = issuesRepository.save(issuePrototypeOne).id
        val issueRetrieved = issuesRepository.findById(id).get()
        issueRetrieved.state = stateTwo
        issuesRepository.save(issueRetrieved)
        val updatedIssueRetrieved = issuesRepository.findById(id).get()
        assertEquals(updatedIssueRetrieved.parentProject, projectSaved)
        assertEquals(updatedIssueRetrieved.createdBy, creator)
        assertTrue(updatedIssueRetrieved.updatedAt > issueRetrieved.createdAt)
        assertEquals(issueRetrieved.state, stateTwo)
    }
}
