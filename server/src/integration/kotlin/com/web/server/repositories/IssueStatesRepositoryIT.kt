package com.web.server.repositories

import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IssueStatesRepositoryIT : RepositoryITAbstract() {
    private lateinit var creator: User
    private lateinit var projectSaved: Project
    private lateinit var projectSavedTwo: Project
    private lateinit var stateOne: IssueState

    @Before
    fun cleanAllTablesBefore() {
        cleanUpAllRepos()

        creator = usersRepository.save(CREATOR_PROTO)
        projectSaved = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )
        projectSavedTwo = projectsRepository.save(
            Project(projectName = "web project 2", description = "description", creator = creator)
        )
    }

    @After
    fun cleanAllTablesAfter() {
        cleanUpAllRepos()
    }

    @Test
    fun `scenario of saving multiple issue states config for multiple projects is successful`() {
        issueStatesRepository.save(IssueState(stateName = "one", isFirst = true, parentProject = projectSaved))
        issueStatesRepository.save(IssueState(stateName = "two", isFirst = false, parentProject = projectSaved))
        issueStatesRepository.save(IssueState(stateName = "one", isFirst = true, parentProject = projectSavedTwo))
        issueStatesRepository.save(IssueState(stateName = "two", isFirst = false, parentProject = projectSavedTwo))
        issueStatesRepository.save(IssueState(stateName = "three", isFirst = false, parentProject = projectSavedTwo))
    }

    @Test
    fun `when project's issues' states are set, the first default issue state can be found`() {
        val savedIssueState = issueStatesRepository.save(
            IssueState(stateName = "one", isFirst = true, parentProject = projectSaved)
        )
        val firstDefaultIssueState = issueStatesRepository.findByParentProjectIdAndIsFirst(projectSaved.id)
        assertEquals(savedIssueState, firstDefaultIssueState)
    }

    @Test
    fun `attempt to save two issue states with same name for same project throws exception`() {
        val sameStateName = "some-state"
        issueStatesRepository.save(IssueState(stateName = sameStateName, isFirst = true, parentProject = projectSaved))
        assertFailsWith<DataIntegrityViolationException> {
            issueStatesRepository.save(
                IssueState(stateName = sameStateName, isFirst = false, parentProject = projectSaved)
            )
        }
    }

    @Test
    fun `attempt to save two issues as FIRST default issues for same project throws exception`() {
        stateOne = issueStatesRepository.save(
            IssueState(stateName = "one", isFirst = true, parentProject = projectSaved)
        )
        assertFailsWith<DataIntegrityViolationException> {
            issueStatesRepository.save(
                IssueState(stateName = "two", isFirst = true, parentProject = projectSaved)
            )
        }
    }
}
