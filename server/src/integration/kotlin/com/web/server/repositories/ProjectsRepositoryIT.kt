package com.web.server.repositories

import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProjectsRepositoryIT : RepositoryITAbstract() {
    private lateinit var creator: User
    private lateinit var creatorTwo: User

    @Before
    fun cleanAllTablesBefore() {
        cleanUpAllRepos()
        creator = usersRepository.save(CREATOR_PROTO)
        creatorTwo = usersRepository.save(CREATOR_TWO_PROTO)
    }

    @After
    fun cleanAllTablesAfter() {
        cleanUpAllRepos()
    }

    @Test
    fun `saved project can be successfully retrieved`() {
        val project = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )

        val stateOne = issueStatesRepository.save(
            IssueState(stateName = "one", isFirst = true, parentProject = project)
        )
        val stateTwo = issueStatesRepository.save(
            IssueState(stateName = "two", isFirst = false, parentProject = project)
        )

        val transitionOne = issueStateTransitionsRepository.save(
            IssueStateTransition(fromState = stateOne, toState = stateTwo, parentProject = project)
        )
        val transitionTwo = issueStateTransitionsRepository.save(
            IssueStateTransition(fromState = stateTwo, toState = stateOne, parentProject = project)
        )

        val projectWithStates = projectsRepository.findByIdAndFetchStatesEagerly(project.id).get()
        val projectWithTransitions = projectsRepository.findByIdAndFetchTransitionsEagerly(project.id).get()

        assertEquals(project.projectName, projectWithStates.projectName)
        assertEquals(project.description, projectWithStates.description)
        assertTrue(projectWithStates.createdAt > TEST_START_TS)
        assertEquals(creator, projectWithStates.creator)
        assertEquals(setOf(stateOne, stateTwo), projectWithStates.states.toSet())
        assertEquals(setOf(transitionOne, transitionTwo), projectWithTransitions.transitions.toSet())
    }

    @Test
    fun `saved projects are correctly retrieved by creator ID in pages of desired size`() {
        // Saving some projects created by users:
        val projectsPrototypes = listOf(
            // creatorOne's projects:
            Project(projectName = "web project 1", description = "dsc", creator = creator),
            Project(projectName = "web project 2", description = "dsc", creator = creator),
            Project(projectName = "web project 3", description = "dsc", creator = creator),
            // creatorTwo's Projects:
            Project(projectName = "MDLE project 1", description = "dsc", creator = creatorTwo),
            Project(projectName = "MDLE project 2", description = "dsc", creator = creatorTwo)
        )
        projectsRepository.saveAll(projectsPrototypes)

        // Retrieving pages of projects given users' IDs:
        val firstPageWithTwoElements = PageRequest.of(0, 2)
        val secondPageWithOneElement = PageRequest.of(1, 1)

        val firstPageWithTwoElementsResultOne =
            projectsRepository.findAllByCreatorId(
                creator.id,
                firstPageWithTwoElements
            )
        val secondPageWithOneElementResultOne =
            projectsRepository.findAllByCreatorId(
                creator.id,
                secondPageWithOneElement
            )
        assertEquals(2, firstPageWithTwoElementsResultOne.size)
        assertEquals(creator.id, firstPageWithTwoElementsResultOne.content[0].creator.id)
        assertEquals("web project 2", secondPageWithOneElementResultOne.content[0].projectName)

        val firstPageWithTwoElementsResultTwo =
            projectsRepository.findAllByCreatorId(
                creatorTwo.id,
                firstPageWithTwoElements
            )
        val secondPageWithOneElementResultTwo =
            projectsRepository.findAllByCreatorId(
                creatorTwo.id,
                secondPageWithOneElement
            )
        assertEquals(2, firstPageWithTwoElementsResultTwo.size)
        assertEquals(creatorTwo.id, firstPageWithTwoElementsResultTwo.content[0].creator.id)
        assertEquals("MDLE project 2", secondPageWithOneElementResultTwo.content[0].projectName)
    }

    @Test
    fun `saved project can be successfully retrieved by its name`() {
        val project = projectsRepository.save(
            Project(projectName = "web project", description = "description", creator = creator)
        )
        val retrievedProject = projectsRepository.findProjectByProjectName("web project").get()
        assertEquals(project.id, retrievedProject.id)
        assertEquals(project.projectName, retrievedProject.projectName)
        assertEquals(project.description, retrievedProject.description)
    }

    @Test
    fun `two projects with the same name can not be saved`() {
        val someIdenticalNameForBoth = "Mo Project"
        projectsRepository.save(
            Project(projectName = someIdenticalNameForBoth, description = "description", creator = creator)
        )
        assertFailsWith<DataIntegrityViolationException> {
            projectsRepository.save(
                Project(projectName = someIdenticalNameForBoth, description = "description", creator = creator)
            )
        }
    }
}
