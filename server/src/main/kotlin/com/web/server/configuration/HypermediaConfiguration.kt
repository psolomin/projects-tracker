package com.web.server.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.hateoas.config.EnableHypermediaSupport

@Configuration
@EnableHypermediaSupport(type = [EnableHypermediaSupport.HypermediaType.HAL_FORMS])
class HypermediaConfiguration
