package com.web.server.configuration

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.SerializationFeature
import javax.annotation.PostConstruct
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@Configuration
class ObjectMapperConfiguration(
    private val mapper: ObjectMapper
) {

    @PostConstruct
    fun after() {
        mapper.apply {
            configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
            setDefaultPropertyInclusion(JsonInclude.Include.NON_EMPTY)
            propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE
        }
    }

    @Bean
    fun mappingJacksonHttpMessageConverter() =
        MappingJackson2HttpMessageConverter().apply { objectMapper = mapper }
}
