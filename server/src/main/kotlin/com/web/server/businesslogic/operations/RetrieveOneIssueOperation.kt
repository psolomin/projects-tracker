package com.web.server.businesslogic.operations

data class RetrieveOneIssueOperation(val id: String)
