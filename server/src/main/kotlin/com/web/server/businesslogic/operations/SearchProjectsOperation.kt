package com.web.server.businesslogic.operations

import com.web.server.repositories.entities.Project
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification

data class SearchProjectsOperation(
    val spec: Specification<Project>?,
    val requestPageable: Pageable
)
