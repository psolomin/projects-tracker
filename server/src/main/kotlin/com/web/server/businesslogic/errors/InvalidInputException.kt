package com.web.server.businesslogic.errors

class InvalidInputException(message: String) : RuntimeException(message)
