package com.web.server.businesslogic.operations

data class UpdateIssueStateOperation(
    val issueId: String,
    val newStateName: String,
    val updatedByUserId: String,
    val versionAssumed: Long
)
