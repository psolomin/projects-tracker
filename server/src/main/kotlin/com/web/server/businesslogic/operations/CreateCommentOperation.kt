package com.web.server.businesslogic.operations

// TODO: add constructor which accepts Request as an argument
//  (applicable for all *-operation classes)
data class CreateCommentOperation(
    val commentBody: String,
    val creatorId: String,
    val parentIssueId: String
)
