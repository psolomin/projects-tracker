package com.web.server.businesslogic.errors

class ProjectNotFoundException(id: String) : RuntimeException("Project ID or NAME $id does not exist.")
