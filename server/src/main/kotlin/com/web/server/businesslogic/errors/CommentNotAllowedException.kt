package com.web.server.businesslogic.errors

class CommentNotAllowedException(issueState: String) : RuntimeException(
    "Comments are not allowed for issues with state == $issueState."
)
