package com.web.server.businesslogic

import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.SearchIssuesOperation
import com.web.server.businesslogic.operations.UpdateIssueStateOperation
import com.web.server.repositories.entities.Issue
import org.springframework.data.domain.Page

interface IssueLogic {
    fun createIssue(operation: CreateIssueOperation): Issue
    fun retrieveOneIssue(operation: RetrieveOneIssueOperation): Issue
    fun updateState(operation: UpdateIssueStateOperation): Issue
    fun searchIssues(op: SearchIssuesOperation): Page<Issue>
}
