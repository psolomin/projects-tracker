package com.web.server.businesslogic

import com.web.server.businesslogic.errors.ProjectAlreadyExistsException
import com.web.server.businesslogic.errors.ProjectNotFoundException
import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.operations.SearchProjectsOperation
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.repositories.ProjectsRepository
import com.web.server.repositories.entities.Project
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class ProjectLogicImpl(
    private val userLogic: UserLogic,
    private val projectSettingsLogic: ProjectSettingsLogic,
    private val projectsRepository: ProjectsRepository,
    private val requiredFieldsValidator: RequiredFieldsValidator
) : ProjectLogic {
    override fun createProject(operation: CreateProjectOperation, settingsOperation: SetProjectSettingsOperation?): Project {
        requiredFieldsValidator.validateProjectCreationData(operation)

        val creator = userLogic.retrieveOneUser(RetrieveOneUserOperation(operation.creatorId))
        projectsRepository.findProjectByProjectName(operation.projectName).ifPresent {
            throw ProjectAlreadyExistsException(operation.projectName)
        }
        val newProject = Project(
            projectName = operation.projectName,
            description = operation.projectDescription,
            creator = creator
        )
        val savedProject = try {
            projectsRepository.save(newProject)
        } catch (e: DataIntegrityViolationException) {
            throw ProjectAlreadyExistsException(operation.projectName)
        }
        projectSettingsLogic.setProjectSettings(savedProject, settingsOperation)
        return savedProject
    }

    override fun retrieveOneProject(operation: RetrieveOneProjectOperation): Project {
        return projectsRepository.findById(operation.id).orElseThrow {
            throw ProjectNotFoundException(operation.id)
        }
    }

    override fun searchProjects(op: SearchProjectsOperation): Page<Project> {
        return projectsRepository.findAll(op.spec, op.requestPageable)
    }
}
