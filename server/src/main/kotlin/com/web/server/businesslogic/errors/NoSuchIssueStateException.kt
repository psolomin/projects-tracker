package com.web.server.businesslogic.errors

class NoSuchIssueStateException(projectId: String, issueState: String) : RuntimeException(
    "Issue state \"$issueState\" does not exist for project ID = $projectId."
)
