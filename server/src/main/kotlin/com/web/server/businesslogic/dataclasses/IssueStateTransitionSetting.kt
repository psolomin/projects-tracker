package com.web.server.businesslogic.dataclasses

import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.MediaType

data class IssueStateTransitionSetting(
    val stateFrom: String,
    val stateTo: String
) {
    fun getTemplate(): Template {
        return Template(
            title = "Project settings - issue state transitions",
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                // TODO: make `name` values passing through JSON serializer rules (camel or snake)
                TextProperty(name = "state_from", required = true, value = "open"),
                TextProperty(name = "state_to", required = true, value = "closed")
            )
        )
    }
}
