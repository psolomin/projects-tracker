package com.web.server.businesslogic

import com.web.server.businesslogic.errors.AttemptToUpdateStaleStateException
import com.web.server.businesslogic.errors.ConflictingUpdatesDetectedException
import com.web.server.businesslogic.errors.IssueNotFoundException
import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.operations.SearchIssuesOperation
import com.web.server.businesslogic.operations.UpdateIssueStateOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.repositories.IssuesRepository
import com.web.server.repositories.entities.Issue
import org.springframework.data.domain.Page
import org.springframework.orm.ObjectOptimisticLockingFailureException
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class IssueLogicImpl(
    private val userLogic: UserLogic,
    private val projectLogic: ProjectLogic,
    private val projectSettingsLogic: ProjectSettingsLogic,
    private val issuesRepository: IssuesRepository,
    private val requiredFieldsValidator: RequiredFieldsValidator
) : IssueLogic {
    override fun createIssue(operation: CreateIssueOperation): Issue {

        requiredFieldsValidator.validateIssueCreationData(operation)

        val creator = userLogic
            .retrieveOneUser(RetrieveOneUserOperation(operation.creatorId))
        val parentProject = projectLogic
            .retrieveOneProject(RetrieveOneProjectOperation(operation.parentProjectId))
        val firstIssueState = projectSettingsLogic
            .findFirstIssueStateInProjectConfigs(operation.parentProjectId)
        val finalLabels = projectSettingsLogic
            .findMatchingLabels(operation.parentProjectId, operation.labels)

        return issuesRepository.save(
            Issue(
                issueName = operation.issueName,
                description = operation.description,
                parentProject = parentProject,
                createdBy = creator,
                state = firstIssueState,
                issueLabels = finalLabels
            )
        )
    }

    override fun retrieveOneIssue(operation: RetrieveOneIssueOperation): Issue {
        return issuesRepository.findById(operation.id).orElseThrow {
            throw IssueNotFoundException(operation.id)
        }
    }

    override fun updateState(operation: UpdateIssueStateOperation): Issue {
        val updater = userLogic.retrieveOneUser(RetrieveOneUserOperation(operation.updatedByUserId))
        val issue = retrieveOneIssue(RetrieveOneIssueOperation(operation.issueId))
        if (issue.issueVersion != operation.versionAssumed)
            throw AttemptToUpdateStaleStateException("Issue state to be updated is out of date.")
        val transition = findTransitionByProjectAndStatesNames(issue, operation)
        issue.state = transition.toState
        issue.updatedBy = updater
        try {
            return issuesRepository.save(issue)
        } catch (e: ObjectOptimisticLockingFailureException) {
            throw ConflictingUpdatesDetectedException(
                "Conflict update attempt detected while updating issue ${issue.id}."
            )
        }
    }

    override fun searchIssues(op: SearchIssuesOperation): Page<Issue> {
        return issuesRepository.findAll(op.spec, op.requestPageable)
    }

    private fun findTransitionByProjectAndStatesNames(issue: Issue, operation: UpdateIssueStateOperation) =
        projectSettingsLogic.findTransitionByProjectAndStatesNames(
            issue.parentProject.id,
            issue.state.stateName,
            operation.newStateName
        )
}
