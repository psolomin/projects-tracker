package com.web.server.businesslogic.operations

data class CreateIssueOperation(
    val issueName: String,
    val description: String,
    val creatorId: String,
    val parentProjectId: String,
    val labels: List<String>
)
