package com.web.server.businesslogic

import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.SearchProjectsOperation
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.repositories.entities.Project
import org.springframework.data.domain.Page

interface ProjectLogic {
    fun createProject(operation: CreateProjectOperation, settingsOperation: SetProjectSettingsOperation?): Project
    fun retrieveOneProject(operation: RetrieveOneProjectOperation): Project
    fun searchProjects(op: SearchProjectsOperation): Page<Project>
}
