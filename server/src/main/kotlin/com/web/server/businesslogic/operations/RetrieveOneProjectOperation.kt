package com.web.server.businesslogic.operations

data class RetrieveOneProjectOperation(val id: String)
