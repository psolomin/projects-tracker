package com.web.server.businesslogic.errors

class UserAlreadyExistsException(name: String) : RuntimeException(
    "User with name $name already exists."
)
