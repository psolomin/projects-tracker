package com.web.server.businesslogic.operations

import org.springframework.data.domain.Pageable

data class RetrieveAllUsersOperation(
    val requestPageable: Pageable
)
