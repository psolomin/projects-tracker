package com.web.server.businesslogic.operations

data class RetrieveOneCommentOperation(val commentId: String)
