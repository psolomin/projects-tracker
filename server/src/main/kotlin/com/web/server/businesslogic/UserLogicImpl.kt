package com.web.server.businesslogic

import com.web.server.businesslogic.errors.UserAlreadyExistsException
import com.web.server.businesslogic.errors.UserNotFoundException
import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.businesslogic.operations.RetrieveAllUsersOperation
import com.web.server.businesslogic.operations.RetrieveOneUserByNameOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.repositories.UsersRepository
import com.web.server.repositories.entities.User
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class UserLogicImpl(
    private val usersRepository: UsersRepository,
    private val requiredFieldsValidator: RequiredFieldsValidator
) : UserLogic {
    override fun createUser(operation: CreateUserOperation): User {

        requiredFieldsValidator.validateUserCreationData(operation)

        usersRepository.findUserByUserName(operation.userName).ifPresent {
            throw UserAlreadyExistsException(operation.userName)
        }
        val newUser = User(userName = operation.userName, userPassword = operation.userPasswordEncoded)
        try {
            return usersRepository.save(newUser)
        } catch (e: DataIntegrityViolationException) {
            throw UserAlreadyExistsException(operation.userName)
        }
    }

    override fun retrieveOneUser(operation: RetrieveOneUserOperation): User {
        return usersRepository.findById(operation.id).orElseThrow {
            throw UserNotFoundException(operation.id)
        }
    }

    override fun retrieveOneUserByName(operation: RetrieveOneUserByNameOperation): User {
        return usersRepository.findUserByUserName(operation.userName).orElseThrow {
            throw UserNotFoundException(operation.userName)
        }
    }

    override fun retrievePageOfAllUsers(op: RetrieveAllUsersOperation): Page<User> {
        return usersRepository.findAll(op.requestPageable)
    }
}
