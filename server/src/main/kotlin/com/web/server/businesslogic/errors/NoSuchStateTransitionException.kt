package com.web.server.businesslogic.errors

class NoSuchStateTransitionException(
    projectId: String,
    stateFrom: String,
    stateTo: String
) : RuntimeException(
    "Issues of project ID = $projectId can not change state from \"$stateFrom\" to \"$stateTo\"."
)
