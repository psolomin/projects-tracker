package com.web.server.businesslogic.errors

class ProjectAlreadyExistsException(name: String) : RuntimeException(
    "Project with name $name already exists."
)
