package com.web.server.businesslogic.validators

import com.web.server.businesslogic.operations.SetProjectSettingsOperation

interface SetProjectSettingsOperationValidator {
    fun validateSettings(operation: SetProjectSettingsOperation): Boolean
}
