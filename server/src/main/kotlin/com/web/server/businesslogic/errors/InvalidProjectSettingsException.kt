package com.web.server.businesslogic.errors

class InvalidProjectSettingsException(message: String) : RuntimeException(message)
