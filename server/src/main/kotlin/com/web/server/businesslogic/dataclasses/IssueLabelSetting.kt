package com.web.server.businesslogic.dataclasses

import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.MediaType

data class IssueLabelSetting(
    val label: String
) {
    fun getTemplate(): Template {
        return Template(
            title = "Project settings - issue labels",
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                TextProperty(name = "label", required = true, value = "feature")
            )
        )
    }
}
