package com.web.server.businesslogic.dataclasses

import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.MediaType

data class IssueStateSetting(
    val name: String,
    var isFirst: Boolean
) {
    // for some reason, when the body does not have explicit project settings,
    // and the default "constant" is used, `isFirst` field was getting
    // always `false` value.
    // Related discussion: https://stackoverflow.com/q/21913955/6257424
    fun getIsFirst(): Boolean {
        return isFirst
    }

    fun setIsFirst(isActive: Boolean) {
        this.isFirst = isActive
    }

    fun getTemplate(): Template {
        return Template(
            title = "Project settings - issue state",
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                // TODO: make `name` values passing through JSON serializer rules (camel or snake)
                TextProperty(name = "name", required = true, value = "open"),
                TextProperty(name = "is_first", required = true, value = "true", regex = "^(true|false)$")
            )
        )
    }
}
