package com.web.server.businesslogic.errors

class AttemptToUpdateStaleStateException(message: String) : RuntimeException(message)
