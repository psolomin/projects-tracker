package com.web.server.businesslogic.operations

import com.web.server.constants.SecurityConstants.PASSWORD_ENCODER

data class CreateUserOperation(
    val userName: String,
    val userPassword: String,
    var userPasswordEncoded: String = ""
) {
    init {
        userPasswordEncoded = PASSWORD_ENCODER.encode(userPassword)
    }
}
