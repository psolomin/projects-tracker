package com.web.server.businesslogic.validators

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.businesslogic.errors.InvalidProjectSettingsException
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_ALLOWED_SYMBOLS
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_MAX_COUNT
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_REGEX
import com.web.server.constants.UserInputConstraints.ISSUE_STATES_MAX_COUNT
import com.web.server.constants.UserInputConstraints.ISSUE_STATE_TRANSITIONS_MAX_COUNT
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MAX_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MIN_LENGTH
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import org.jgrapht.graph.DefaultEdge
import org.jgrapht.graph.DirectedPseudograph
import org.springframework.stereotype.Component

@Component
class SetProjectSettingsOperationValidatorImpl : SetProjectSettingsOperationValidator {
    private val textInputLimitErrorMsg = "Text input must have at least $TEXT_INPUT_MIN_LENGTH " +
                "and at most $TEXT_INPUT_MAX_LENGTH symbols."

    override fun validateSettings(operation: SetProjectSettingsOperation): Boolean {
        // early checks before passing to heavy checks:
        val inputsSatisfyLimits = inputsAreInLimits(operation)

        // computationally heavy validations:
        val labelsValid = validateLabels(operation.issueLabelSettings)
        val statesValid = validateIssueStates(operation.issueStateSettings)
        val transitionsValid = validateIssueTransitions(operation.issueStateTransitionSettings)
        val configsAreConsistent = validateProjectSettingsConsistency(
            operation.issueStateSettings, operation.issueStateTransitionSettings
        )
        return inputsSatisfyLimits and labelsValid and statesValid and transitionsValid and configsAreConsistent
    }

    private fun validateLabels(issueLabelSettings: List<IssueLabelSetting>): Boolean {
        if (!labelsHaveOnlyAllowedSymbols(issueLabelSettings)) {
            throw InvalidProjectSettingsException("Only $ISSUE_LABELS_ALLOWED_SYMBOLS are allowed for labels.")
        }
        if (!labelsDoNotHaveRepeats(issueLabelSettings)) {
            throw InvalidProjectSettingsException("Duplicated labels found.")
        }
        return true
    }

    private fun labelsHaveOnlyAllowedSymbols(issueLabelSettings: List<IssueLabelSetting>): Boolean {
        return issueLabelSettings.all { it.label.matches(ISSUE_LABELS_REGEX) }
    }

    private fun labelsDoNotHaveRepeats(issueLabelSettings: List<IssueLabelSetting>): Boolean {
        val labelsCnt = issueLabelSettings.size
        val distinctLabelsCnt = issueLabelSettings.distinctBy { it.label }.size
        if (labelsCnt != distinctLabelsCnt) {
            return false
        }
        return true
    }

    private fun validateIssueStates(states: List<IssueStateSetting>): Boolean {
        val firstStates = states.filter { st -> st.isFirst }
        if (firstStates.isEmpty())
            throw InvalidProjectSettingsException("At least one state must be \"first\".")
        if (firstStates.size > 1)
            throw InvalidProjectSettingsException("Only one state can be \"first\".")

        val distinctStateNames = states.distinctBy { it.name }
        if (states.size > distinctStateNames.size)
            throw InvalidProjectSettingsException("Duplicated state names found.")
        return true
    }

    private fun validateIssueTransitions(transitions: List<IssueStateTransitionSetting>): Boolean {
        val distinctTransitions = transitions.distinctBy { Pair(it.stateFrom, it.stateTo) }
        if (transitions.size > distinctTransitions.size)
            throw InvalidProjectSettingsException("Duplicated transitions found.")

        val transitionsToSelf = transitions.filter { it.stateFrom == it.stateTo }
        if (transitionsToSelf.isNotEmpty()) {
            val stateName = transitionsToSelf[0].stateFrom
            throw InvalidProjectSettingsException("Transition to same state: \"$stateName\".")
        }
        return true
    }

    private fun validateProjectSettingsConsistency(
        states: List<IssueStateSetting>,
        transitions: List<IssueStateTransitionSetting>
    ): Boolean {
        // already know the first state is only one:
        val firstStateName = states.filter { it -> it.isFirst }[0].name
        // build graph:
        val g = DirectedPseudograph<String, DefaultEdge>(DefaultEdge::class.java)
        states.forEach { g.addVertex(it.name) }
        try {
            transitions.forEach { g.addEdge(it.stateFrom, it.stateTo) }
        } catch (e: IllegalArgumentException) {
            throw InvalidProjectSettingsException("Transition to non-existing state: " + e.message)
        }
        val pathFinder = DijkstraShortestPath(g)
        // test if all states except the first are reachable from the first:
        states.filter {
            it -> !it.isFirst
        }.forEach {
            val path = pathFinder.getPath(firstStateName, it.name)
            path ?: throw InvalidProjectSettingsException("Unreachable state: \"${it.name}\"")
        }
        // test if all states except the last can reach the default last state:
        states.filter {
            it -> it.name != DEFAULT_LAST_ISSUE_STATE.name
        }.forEach {
            val defaultLatState = DEFAULT_LAST_ISSUE_STATE.name
            val path = pathFinder.getPath(it.name, defaultLatState)
            val msg = "No set of transitions from state: \"${it.name}\" to state \"$defaultLatState\"."
            path ?: throw InvalidProjectSettingsException(msg)
        }
        return true
    }

    private fun inputsAreInLimits(operation: SetProjectSettingsOperation): Boolean {
        val issueStatesAreInLimits = issueStatesAreInLimits(
            operation.issueStateSettings,
            operation.issueStateTransitionSettings
        )

        val labelsAreInLimits = labelsAreInLimits(operation.issueLabelSettings)

        if (!issueStatesAreInLimits.first) {
            throw InvalidProjectSettingsException(issueStatesAreInLimits.second)
        }

        if (!labelsAreInLimits.first) {
            throw InvalidProjectSettingsException(labelsAreInLimits.second)
        }

        return true
    }

    private fun issueStatesAreInLimits(
        states: List<IssueStateSetting>,
        transitions: List<IssueStateTransitionSetting>
    ): Pair<Boolean, String> {
        val statesCountOk = (states.size <= ISSUE_STATES_MAX_COUNT)
        val transitionsCountOk = (transitions.size <= ISSUE_STATE_TRANSITIONS_MAX_COUNT)

        val statesTextSizeOk = !states.any { sizeIsOutOfBounds(it.name) }
        val transitionsTextSizeOk = !transitions.any {
            sizeIsOutOfBounds(it.stateFrom) || sizeIsOutOfBounds(it.stateTo)
        }

        return issueStatesInputCheckingDetailedResult(statesCountOk,
            transitionsCountOk,
            statesTextSizeOk,
            transitionsTextSizeOk
        )
    }

    private fun labelsAreInLimits(labels: List<IssueLabelSetting>): Pair<Boolean, String> {
        val labelsCountOk = (labels.size <= ISSUE_LABELS_MAX_COUNT)
        val labelsTextSizeOk = !labels.any { sizeIsOutOfBounds(it.label) }
        return issueLabelsInputCheckingDetailedResult(
            labelsCountOk,
            labelsTextSizeOk
        )
    }

    private fun sizeIsOutOfBounds(input: String): Boolean {
        return input.length < TEXT_INPUT_MIN_LENGTH || input.length > TEXT_INPUT_MAX_LENGTH
    }

    private fun issueStatesInputCheckingDetailedResult(
        statesCountOk: Boolean,
        transitionsCountOk: Boolean,
        statesTextSizeOk: Boolean,
        transitionsTextSizeOk: Boolean
    ): Pair<Boolean, String> {
        // there is no need to provide output for *all* possible combinations
        return if (!statesCountOk) {
            val msg = "Too many issue states. Max is $ISSUE_STATES_MAX_COUNT."
            Pair(false, msg)
        } else if (!transitionsCountOk) {
            val msg = "Too many issue state transitions. Max is $ISSUE_STATE_TRANSITIONS_MAX_COUNT."
            Pair(false, msg)
        } else if (!statesTextSizeOk) {
            Pair(false, textInputLimitErrorMsg)
        } else if (!transitionsTextSizeOk) {
            Pair(false, textInputLimitErrorMsg)
        } else Pair(true, "OK")
    }

    private fun issueLabelsInputCheckingDetailedResult(
        labelsCountOk: Boolean,
        labelsTextSizeOk: Boolean
    ): Pair<Boolean, String> {
        return if (!labelsCountOk) {
            val msg = "Too many issue labels. Max is $ISSUE_LABELS_MAX_COUNT."
            Pair(false, msg)
        } else if (!labelsTextSizeOk) {
            Pair(false, textInputLimitErrorMsg)
        } else Pair(true, "OK")
    }
}
