package com.web.server.businesslogic.operations

data class RetrieveOneUserByNameOperation(val userName: String)
