package com.web.server.businesslogic

import com.web.server.businesslogic.errors.NoSuchIssueStateException
import com.web.server.businesslogic.errors.NoSuchLabelException
import com.web.server.businesslogic.errors.NoSuchStateTransitionException
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.businesslogic.validators.SetProjectSettingsOperationValidator
import com.web.server.constants.DefaultFieldsValues.DEFAULT_ISSUE_LABELS
import com.web.server.repositories.IssueLabelsRepository
import com.web.server.repositories.IssueStateTransitionsRepository
import com.web.server.repositories.IssueStatesRepository
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import java.util.SortedSet
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class ProjectSettingsLogicImpl(
    private val validator: SetProjectSettingsOperationValidator,
    private val issueStatesRepository: IssueStatesRepository,
    private val issueStateTransitionsRepository: IssueStateTransitionsRepository,
    private val issueLabelsRepository: IssueLabelsRepository
) : ProjectSettingsLogic {
    override fun setProjectSettings(project: Project, userSetSettingsOp: SetProjectSettingsOperation?) {
        val completedOp = buildMergedProjectSettings(userSetSettingsOp)
        validator.validateSettings(completedOp)
        val savedStates = saveIssueStates(project, completedOp)
        saveIssueStateTransitions(project, completedOp, savedStates)
        saveLabels(project, completedOp)
    }

    override fun findFirstIssueStateInProjectConfigs(projectId: String): IssueState {
        return issueStatesRepository.findByParentProjectIdAndIsFirst(projectId)
    }

    override fun findIssueStateByName(projectId: String, stateName: String): IssueState {
        return issueStatesRepository.findByParentProjectIdAndByName(projectId, stateName).orElseThrow {
            NoSuchIssueStateException(projectId, stateName)
        }
    }

    override fun findTransitionByProjectAndStatesNames(
        projectId: String,
        fromState: String,
        toState: String
    ): IssueStateTransition {
        // check first if such state exists:
        findIssueStateByName(projectId, toState)
        return issueStateTransitionsRepository.findByParentProjectIdAndStateNames(
            projectId, fromState, toState
        ).orElseThrow {
            NoSuchStateTransitionException(projectId, fromState, toState)
        }
    }

    override fun findMatchingLabels(projectId: String, labels: List<String>): SortedSet<IssueLabel> {
        if (labels.isEmpty()) return DEFAULT_ISSUE_LABELS
        val candidateLabels = issueLabelsRepository.findByParentProjectIdAndByLabels(projectId, labels)
        val candidateLabelsSet = candidateLabels.map { it.label }.toSet()
        val nonMatchingLabels = labels.filter { it !in candidateLabelsSet }
        if (nonMatchingLabels.isNotEmpty()) {
            throw NoSuchLabelException(projectId, nonMatchingLabels)
        }
        return candidateLabels.toSortedSet(compareBy { l -> l.label })
    }

    override fun findAvailableLabels(projectId: String): SortedSet<IssueLabel> {
        return issueLabelsRepository.findByParentProjectId(projectId).toSortedSet(compareBy { l -> l.label })
    }

    private fun buildMergedProjectSettings(
        userSetSettingsOp: SetProjectSettingsOperation?
    ): SetProjectSettingsOperation {
        val defaultOp = SetProjectSettingsOperation()
        return userSetSettingsOp?.let {
            defaultOp.mergeSetSettingsOperation(it)
        } ?: run {
            defaultOp
        }
    }

    private fun saveIssueStates(parentProject: Project, op: SetProjectSettingsOperation): Iterable<IssueState> {
        val states = op.issueStateSettings.map {
            IssueState(stateName = it.name, isFirst = it.isFirst, parentProject = parentProject)
        }
        return issueStatesRepository.saveAll(states)
    }

    private fun saveIssueStateTransitions(
        parentProject: Project,
        op: SetProjectSettingsOperation,
        savedStates: Iterable<IssueState>
    ): Iterable<IssueStateTransition> {
        val nameToSavedStateMap = savedStates.map { it.stateName to it }.toMap()
        val transitions = op.issueStateTransitionSettings.map {
            IssueStateTransition(
                fromState = nameToSavedStateMap.getValue(it.stateFrom),
                toState = nameToSavedStateMap.getValue(it.stateTo),
                parentProject = parentProject
            )
        }
        return issueStateTransitionsRepository.saveAll(transitions)
    }

    private fun saveLabels(parentProject: Project, op: SetProjectSettingsOperation): Iterable<IssueLabel> {
        val labels = op.issueLabelSettings.map { IssueLabel(label = it.label, parentProject = parentProject) }
        return issueLabelsRepository.saveAll(labels)
    }
}
