package com.web.server.businesslogic.errors

class NoSuchLabelException(projectId: String, nonMatchingLabels: List<String>) : RuntimeException(
    "Labels $nonMatchingLabels are not defiled for project ID = $projectId."
)
