package com.web.server.businesslogic.operations

data class RetrieveOneUserOperation(val id: String)
