package com.web.server.businesslogic.errors

class ConflictingUpdatesDetectedException(message: String) : RuntimeException(message)
