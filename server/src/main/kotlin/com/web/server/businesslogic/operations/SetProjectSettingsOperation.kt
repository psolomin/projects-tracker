package com.web.server.businesslogic.operations

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_LABELS
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_STATES
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_STATE_TRANSITIONS

data class SetProjectSettingsOperation(
    var issueStateSettings: List<IssueStateSetting> = DEFAULT_ISSUE_STATES,
    var issueStateTransitionSettings: List<IssueStateTransitionSetting> = DEFAULT_ISSUE_STATE_TRANSITIONS,
    var issueLabelSettings: List<IssueLabelSetting> = DEFAULT_ISSUE_LABELS
) {
    fun mergeSetSettingsOperation(userOp: SetProjectSettingsOperation): SetProjectSettingsOperation {
        val userOpWithoutDefaults = dropDefaultsFromUserRequestedSettings(userOp)
        val userOpFirstStates = selectFirstStates(userOpWithoutDefaults)
        if (userOpFirstStates.isNotEmpty()) {
            markAllPreviousStatesAsNonFirst()
        }
        if (userOp.issueLabelSettings.isNotEmpty()) {
            this.issueLabelSettings = userOp.issueLabelSettings
        }
        return buildMergedSettings(userOpWithoutDefaults)
    }

    private fun dropDefaultsFromUserRequestedSettings(op: SetProjectSettingsOperation): SetProjectSettingsOperation =
        SetProjectSettingsOperation(
            op.issueStateSettings.filter { st -> !DEFAULT_ISSUE_STATES.contains(st) },
            op.issueStateTransitionSettings.filter { st -> !DEFAULT_ISSUE_STATE_TRANSITIONS.contains(st) }
        )

    private fun <T> concatLists(listOne: List<T>, listTwo: List<T>) = listOf(listOne, listTwo).flatten()

    private fun mergeStatesPreferSecond(
        first: List<IssueStateSetting>,
        second: List<IssueStateSetting>
    ): List<IssueStateSetting> {
        val secondNames = second.map { it.name }
        val firstFiltered = first.filter { st -> !secondNames.contains(st.name) }
        return concatLists(firstFiltered, second)
    }

    private fun selectFirstStates(op: SetProjectSettingsOperation) = op.issueStateSettings.filter { st -> st.isFirst }

    private fun markAllPreviousStatesAsNonFirst() {
        this.issueStateSettings = this.issueStateSettings.map {
            st ->
            IssueStateSetting(st.name, false)
        }
    }

    private fun buildMergedSettings(userOpWithoutDefaults: SetProjectSettingsOperation): SetProjectSettingsOperation =
        SetProjectSettingsOperation(
            mergeStatesPreferSecond(this.issueStateSettings, userOpWithoutDefaults.issueStateSettings),
            concatLists(this.issueStateTransitionSettings, userOpWithoutDefaults.issueStateTransitionSettings),
            this.issueLabelSettings
        )
}
