package com.web.server.businesslogic.operations

import com.web.server.repositories.entities.Issue
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification

data class SearchIssuesOperation(
    val spec: Specification<Issue>?,
    val requestPageable: Pageable
)
