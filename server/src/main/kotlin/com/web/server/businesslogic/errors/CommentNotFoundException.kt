package com.web.server.businesslogic.errors

class CommentNotFoundException(commentId: String) : RuntimeException(
    "Comment ID = $commentId does not exist."
)
