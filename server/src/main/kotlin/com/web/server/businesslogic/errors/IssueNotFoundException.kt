package com.web.server.businesslogic.errors

class IssueNotFoundException(id: String) : RuntimeException(
    "Issue ID = $id does not exist."
)
