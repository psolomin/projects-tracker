package com.web.server.businesslogic

import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import java.util.SortedSet

interface ProjectSettingsLogic {
    fun setProjectSettings(project: Project, userSetSettingsOp: SetProjectSettingsOperation?)
    fun findFirstIssueStateInProjectConfigs(projectId: String): IssueState
    fun findIssueStateByName(projectId: String, stateName: String): IssueState
    fun findTransitionByProjectAndStatesNames(projectId: String, fromState: String, toState: String): IssueStateTransition
    fun findMatchingLabels(projectId: String, labels: List<String>): SortedSet<IssueLabel>
    fun findAvailableLabels(projectId: String): SortedSet<IssueLabel>
}
