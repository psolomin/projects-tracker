package com.web.server.businesslogic.validators

import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.CreateUserOperation

interface RequiredFieldsValidator {
    fun validateUserCreationData(operation: CreateUserOperation): Boolean
    fun validateProjectCreationData(operation: CreateProjectOperation): Boolean
    fun validateIssueCreationData(operation: CreateIssueOperation): Boolean
    fun validateCommentCreationData(operation: CreateCommentOperation): Boolean
}
