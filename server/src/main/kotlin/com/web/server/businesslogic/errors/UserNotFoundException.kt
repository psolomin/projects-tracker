package com.web.server.businesslogic.errors

class UserNotFoundException(id: String) : RuntimeException("User with ID or NAME $id does not exist.")
