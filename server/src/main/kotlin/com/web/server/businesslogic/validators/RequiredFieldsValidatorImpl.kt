package com.web.server.businesslogic.validators

import com.web.server.businesslogic.errors.InvalidInputException
import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MAX_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MIN_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_SIZE_RANGE
import org.springframework.stereotype.Component

@Component
class RequiredFieldsValidatorImpl : RequiredFieldsValidator {

    override fun validateUserCreationData(operation: CreateUserOperation): Boolean {
        val nameOk = stringIsWithinBorders(operation.userName, TEXT_INPUT_SIZE_RANGE)
        val passwordOk = stringIsWithinBorders(operation.userPassword, TEXT_INPUT_SIZE_RANGE)
        if (!(nameOk && passwordOk)) {
            throw InvalidInputException(
                "Name and password must have at least $TEXT_INPUT_MIN_LENGTH " +
                    "and at most $TEXT_INPUT_MAX_LENGTH characters."
            )
        }
        return nameOk && passwordOk
    }

    override fun validateProjectCreationData(operation: CreateProjectOperation): Boolean {
        val nameOk = stringIsWithinBorders(operation.projectName, TEXT_INPUT_SIZE_RANGE)
        val descriptionOk = stringIsWithinBorders(operation.projectDescription, TEXT_INPUT_SIZE_RANGE)
        if (!(nameOk && descriptionOk)) {
            throw InvalidInputException(
                "Name and description must have at least $TEXT_INPUT_MIN_LENGTH " +
                    "and at most $TEXT_INPUT_MAX_LENGTH characters."
            )
        }
        return nameOk && descriptionOk
    }

    override fun validateIssueCreationData(operation: CreateIssueOperation): Boolean {
        val nameOk = stringIsWithinBorders(operation.issueName, TEXT_INPUT_SIZE_RANGE)
        val descriptionOk = stringIsWithinBorders(operation.description, TEXT_INPUT_SIZE_RANGE)
        if (!(nameOk && descriptionOk)) {
            throw InvalidInputException(
                "Name and description must have at least $TEXT_INPUT_MIN_LENGTH " +
                    "and at most $TEXT_INPUT_MAX_LENGTH characters."
            )
        }
        return nameOk && descriptionOk
    }

    override fun validateCommentCreationData(operation: CreateCommentOperation): Boolean {
        val bodyOk = stringIsWithinBorders(operation.commentBody, TEXT_INPUT_SIZE_RANGE)
        if (!bodyOk) {
            throw InvalidInputException(
                "Comment must have at least $TEXT_INPUT_MIN_LENGTH " +
                    "and at most $TEXT_INPUT_MAX_LENGTH characters."
            )
        }
        return bodyOk
    }

    private fun stringIsWithinBorders(inputString: String, range: LongRange): Boolean {
        return inputString.length in range
    }
}
