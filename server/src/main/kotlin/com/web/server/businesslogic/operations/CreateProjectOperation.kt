package com.web.server.businesslogic.operations

data class CreateProjectOperation(
    val projectName: String,
    val projectDescription: String,
    val creatorId: String
)
