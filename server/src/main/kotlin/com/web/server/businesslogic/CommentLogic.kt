package com.web.server.businesslogic

import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneCommentOperation
import com.web.server.businesslogic.operations.SearchCommentsOperation
import com.web.server.repositories.entities.Comment
import org.springframework.data.domain.Page

interface CommentLogic {
    fun createComment(operation: CreateCommentOperation): Comment
    fun retrieveOneComment(operation: RetrieveOneCommentOperation): Comment
    fun searchComments(op: SearchCommentsOperation): Page<Comment>
}
