package com.web.server.businesslogic.operations

import com.web.server.repositories.entities.Comment
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification

data class SearchCommentsOperation(
    val spec: Specification<Comment>?,
    val requestPageable: Pageable
)
