package com.web.server.businesslogic

import com.web.server.businesslogic.errors.CommentNotAllowedException
import com.web.server.businesslogic.errors.CommentNotFoundException
import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.operations.SearchCommentsOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.repositories.CommentsRepository
import com.web.server.repositories.entities.Comment
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class CommentLogicImpl(
    private val userLogic: UserLogic,
    private val issueLogic: IssueLogic,
    private val commentsRepository: CommentsRepository,
    private val requiredFieldsValidator: RequiredFieldsValidator
) : CommentLogic {
    override fun createComment(operation: CreateCommentOperation): Comment {

        requiredFieldsValidator.validateCommentCreationData(operation)

        val creator = userLogic
            .retrieveOneUser(RetrieveOneUserOperation(operation.creatorId))
        val parentIssue = issueLogic
            .retrieveOneIssue(RetrieveOneIssueOperation(operation.parentIssueId))

        // prohibit creation of comments for issues which are in their last state:
        // TODO:
        // this can't guarantee that a comment can't be added to the issue.
        // Current transaction may see stale value.
        if (parentIssue.state.stateName == DEFAULT_LAST_ISSUE_STATE.name)
            throw CommentNotAllowedException(DEFAULT_LAST_ISSUE_STATE.name)

        return commentsRepository.save(
            Comment(
                commentBody = operation.commentBody,
                creator = creator,
                parentIssue = parentIssue
            )
        )
    }

    override fun retrieveOneComment(operation: RetrieveOneCommentOperation): Comment {
        return commentsRepository.findById(operation.commentId).orElseThrow {
            throw CommentNotFoundException(operation.commentId)
        }
    }

    override fun searchComments(op: SearchCommentsOperation): Page<Comment> {
        return commentsRepository.findAll(op.spec, op.requestPageable)
    }
}
