package com.web.server.businesslogic

import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.businesslogic.operations.RetrieveAllUsersOperation
import com.web.server.businesslogic.operations.RetrieveOneUserByNameOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.repositories.entities.User
import org.springframework.data.domain.Page

interface UserLogic {
    fun createUser(operation: CreateUserOperation): User
    fun retrieveOneUser(operation: RetrieveOneUserOperation): User
    fun retrieveOneUserByName(operation: RetrieveOneUserByNameOperation): User
    fun retrievePageOfAllUsers(op: RetrieveAllUsersOperation): Page<User>
}
