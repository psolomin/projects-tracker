package com.web.server.constants

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

object SecurityConstants {
    val PASSWORD_ENCODER = BCryptPasswordEncoder()
}
