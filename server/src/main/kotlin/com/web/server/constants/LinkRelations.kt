package com.web.server.constants

enum class LinkRelations(val relName: String, val relTitle: String) {
    TO_USERS_HOME("users", "Users"),

    ALL_USERS("users-all", "All Users"),
    ALL_PROJECTS("projects-all", "All Projects"),
    ALL_ISSUES("issues-all", "All Issues"),
    ALL_COMMENTS("comments-all", "All Comments"),

    CREATE_NEW_USER("sign-up", "SignUp"),
    CREATE_NEW_PROJECT("add-new-project", "Create project"),
    CREATE_NEW_ISSUE("add-new-issue", "Create issue"),
    CREATE_NEW_COMMENT("add-new-comment", "Create comment"),

    PROJECT_CREATED_BY_USER("created-by", "Created by"),
    PROJECT_ISSUES("issues", "Issues"),
    PROJECT_ADD_ISSUE("add-new-issue", "Add new issue"),

    USER_CREATED_PROJECTS("projects", "Projects created"),
    USER_CREATED_ISSUES("issues", "Issues created"),
    USER_CREATED_COMMENTS("comments", "Comments created"),

    ISSUE_CREATED_BY_USER("created-by", "Created by"),
    ISSUE_UPDATED_BY_USER("updated-by", "Updated by"),
    ISSUE_PARENT_PROJECT("parent-project", "Project"),
    ISSUE_PARENT_PROJECT_ISSUES("parent-project-issues", "All project issues"),
    ISSUE_SAME_LABELS_ISSUES("same-labels-issues", "Issues with same labels"),
    ISSUE_COMMENTS("comments", "Comments"),
    ISSUE_ADD_COMMENT("add-new-comment", "Add new comment"),
    ISSUE_UPDATE_STATE("update-state", "Update state"),

    COMMENT_CREATED_BY_USER("created-by", "Created by"),
    COMMENT_PARENT_ISSUE("parent-issue", "Issue"),
    COMMENT_PARENT_PROJECT("parent-project", "Project"),
    COMMENT_PARENT_ISSUE_COMMENTS("parent-issue-comments", "All issue comments")
}
