package com.web.server.constants

object Uris {
    const val PROJECT_SETTINGS_TEMPLATE_ID = "project-settings-tpl"
    const val PROJECT_SETTINGS_ISSUE_STATES_TEMPLATE_ID = "project-settings-issue-states-tpl"
    const val PROJECT_SETTINGS_ISSUE_STATE_TRANSITIONS_TEMPLATE_ID = "project-settings-issue-states-transitions-tpl"
    const val PROJECT_SETTINGS_ISSUE_LABELS_TEMPLATE_ID = "project-settings-issue-labels-tpl"

    const val TEMPLATES_RESOURCE_URI = "/templates"
    const val USERS_RESOURCE_URI = "/users"
    const val PROJECTS_RESOURCE_URI = "/projects"
    const val ISSUES_RESOURCE_URI = "/issues"
    const val COMMENTS_RESOURCE_URI = "/comments"
}
