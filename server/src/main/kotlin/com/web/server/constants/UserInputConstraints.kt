package com.web.server.constants

object UserInputConstraints {
    const val TEXT_INPUT_MIN_LENGTH = 3L
    const val TEXT_INPUT_MAX_LENGTH = 125L
    val TEXT_INPUT_SIZE_RANGE = TEXT_INPUT_MIN_LENGTH..TEXT_INPUT_MAX_LENGTH
    const val ISSUE_STATES_MAX_COUNT = 15L
    const val ISSUE_STATE_TRANSITIONS_MAX_COUNT = 15L
    const val ISSUE_LABELS_MAX_COUNT = 15L
    const val ISSUE_LABELS_ALLOWED_SYMBOLS = "[A-Za-z0-9]+"
    val ISSUE_LABELS_REGEX = Regex(ISSUE_LABELS_ALLOWED_SYMBOLS)
}
