package com.web.server.constants

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting

object DefaultProjectSettings {
    val DEFAULT_LAST_ISSUE_STATE = IssueStateSetting(name = "archived", isFirst = false)

    val DEFAULT_ISSUE_STATES = arrayListOf(
        IssueStateSetting(name = "closed", isFirst = true),
        DEFAULT_LAST_ISSUE_STATE
    )
    val DEFAULT_ISSUE_STATE_TRANSITIONS = arrayListOf(
        IssueStateTransitionSetting(stateFrom = "closed", stateTo = DEFAULT_LAST_ISSUE_STATE.name)
    )
    val DEFAULT_ISSUE_LABELS = arrayListOf<IssueLabelSetting>()
}
