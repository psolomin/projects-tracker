package com.web.server.constants

import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueLabel
import java.time.Instant
import java.util.TreeSet
import java.util.UUID

object DefaultFieldsValues {
    const val DEFAULT_VERSION = -1L
    const val DEFAULT_ID = -1L
    val DEFAULT_TS = Instant.EPOCH!!
    val DEFAULT_ISSUE_LABELS = TreeSet<IssueLabel>()
    val DEFAULT_LABEL_ISSUES = TreeSet<Issue>()
    fun generateNewId() = UUID.randomUUID().toString()
    fun generateNewTs() = Instant.now()!!
}
