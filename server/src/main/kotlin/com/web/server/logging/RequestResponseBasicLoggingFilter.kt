package com.web.server.logging

import com.web.server.DemoApplication
import java.util.UUID
import javax.servlet.FilterChain
import javax.servlet.http.HttpFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class RequestResponseBasicLoggingFilter : HttpFilter() {

    private companion object {
        const val REQUEST_ID = "X-Request-Id"
        const val REQUEST_URI = "X-Request-Uri"
        const val REQUEST_METHOD = "X-Request-Method"
        const val RESPONSE_STATUS_CODE = "X-Response-Status-Code"
        const val RESPONSE_TIME = "X-Response-Time"
    }

    private val log = LoggerFactory.getLogger(DemoApplication::class.java.name)

    override fun doFilter(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {

        val requestId = UUID.randomUUID().toString()
        val requestUri = request.requestURI
        val requestMethod = request.method
        val requestStartTime = System.currentTimeMillis()

        MDC.put(REQUEST_ID, requestId)
        MDC.put(REQUEST_URI, requestUri)
        MDC.put(REQUEST_METHOD, requestMethod)

        try {
            log.info("request received")
            chain.doFilter(request, response)
        } finally {
            val serviceTime = (System.currentTimeMillis() - requestStartTime).toString()
            val responseCode = response.status.toString()

            MDC.put(RESPONSE_STATUS_CODE, responseCode)
            MDC.put(RESPONSE_TIME, serviceTime)

            log.info("response sent")
        }
    }
}
