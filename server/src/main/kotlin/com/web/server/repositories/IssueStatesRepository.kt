package com.web.server.repositories

import com.web.server.repositories.entities.IssueState
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface IssueStatesRepository : JpaRepository<IssueState, Long> {
    @Query(
        """ SELECT ist FROM IssueState ist
            JOIN ist.parentProject p
            WHERE ist.isFirst = true AND p.id = (:id) """
    )
    fun findByParentProjectIdAndIsFirst(
        @Param("id") parentProjectId: String
    ): IssueState

    @Query(
        """ SELECT ist FROM IssueState ist
            JOIN ist.parentProject p
            WHERE ist.stateName = (:stateName) AND p.id = (:projectId) """
    )
    fun findByParentProjectIdAndByName(
        @Param("projectId") parentProjectId: String,
        @Param("stateName") stateName: String
    ): Optional<IssueState>
}
