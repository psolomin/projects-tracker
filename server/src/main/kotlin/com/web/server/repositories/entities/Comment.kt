package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_TS
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.DefaultFieldsValues.generateNewTs
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.PrePersist
import javax.persistence.Table

@Entity
@Table(name = "comments")
data class Comment(
    @Id
    var id: String = generateNewId(),

    var commentBody: String,

    var createdAt: Instant = DEFAULT_TS,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    var creator: User,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_issue_id")
    var parentIssue: Issue
) {
    @PrePersist
    fun onCreate() {
        createdAt = generateNewTs()
    }
}
