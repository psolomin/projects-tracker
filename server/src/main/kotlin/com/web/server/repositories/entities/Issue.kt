package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_ISSUE_LABELS
import com.web.server.constants.DefaultFieldsValues.DEFAULT_TS
import com.web.server.constants.DefaultFieldsValues.DEFAULT_VERSION
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.DefaultFieldsValues.generateNewTs
import java.time.Instant
import java.util.SortedSet
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OrderBy
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import javax.persistence.Table
import javax.persistence.Version
import org.hibernate.annotations.SortNatural

@Entity
@Table(name = "issues")
data class Issue(
    @Id
    var id: String = generateNewId(),

    var issueName: String,

    var description: String,

    var createdAt: Instant = DEFAULT_TS,

    var updatedAt: Instant = DEFAULT_TS,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    var createdBy: User,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updated_by")
    var updatedBy: User? = null,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_project_id")
    var parentProject: Project,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_id")
    var state: IssueState,

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "issues_labels",
        joinColumns = [JoinColumn(name = "issue_id")],
        inverseJoinColumns = [JoinColumn(name = "label_id")])
    @SortNatural
    @OrderBy("label ASC")
    var issueLabels: SortedSet<IssueLabel> = DEFAULT_ISSUE_LABELS,

    @Version
    var issueVersion: Long = DEFAULT_VERSION
) : Comparable<Issue> {
    override fun compareTo(other: Issue) = compareValuesBy(this, other, { it.parentProject.id }, { it.createdAt })

    @PrePersist
    fun onCreate() {
        val ts = generateNewTs()
        createdAt = ts
        updatedAt = ts
    }

    @PreUpdate
    fun onUpdate() {
        // otherwise this callback is executed on first save() call
        // due to the implementation of @Version
        if (issueVersion > 0L) updatedAt = generateNewTs()
    }
}
