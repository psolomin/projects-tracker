package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_TS
import com.web.server.constants.DefaultFieldsValues.DEFAULT_VERSION
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.DefaultFieldsValues.generateNewTs
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.persistence.Version

@Entity
@Table(name = "projects")
data class Project(
    @Id
    var id: String = generateNewId(),

    var projectName: String,

    var description: String,

    var createdAt: Instant = DEFAULT_TS,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    var creator: User,

    @Version
    var projectVersion: Long = DEFAULT_VERSION
) {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentProject")
    var states: List<IssueState> = emptyList()

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentProject")
    var transitions: List<IssueStateTransition> = emptyList()

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentProject")
    var labels: List<IssueLabel> = emptyList()

    @PrePersist
    fun onCreate() {
        createdAt = generateNewTs()
    }
}
