package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_ID
import com.web.server.constants.DefaultFieldsValues.DEFAULT_LABEL_ISSUES
import java.util.SortedSet
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OrderBy
import javax.persistence.Table
import org.hibernate.annotations.SortNatural

@Entity
@Table(name = "issues_labels_config")
data class IssueLabel(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = DEFAULT_ID,

    var label: String,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_project_id")
    var parentProject: Project
) : Comparable<IssueLabel> {
    override fun compareTo(other: IssueLabel) = compareValuesBy(this, other, { it.parentProject.id }, { it.label })

    @ManyToMany(mappedBy = "issueLabels", fetch = FetchType.LAZY)
    @SortNatural
    @OrderBy("createdAt DESC")
    var issues: SortedSet<Issue> = DEFAULT_LABEL_ISSUES
}
