package com.web.server.repositories

import com.web.server.repositories.entities.IssueLabel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface IssueLabelsRepository : JpaRepository<IssueLabel, Long> {
    @Query(
        """ SELECT il FROM IssueLabel il
            JOIN il.parentProject p
            WHERE p.id = (:projectId) 
            AND il.label in (:labels) """
    )
    fun findByParentProjectIdAndByLabels(
        @Param("projectId") parentProjectId: String,
        @Param("labels") labels: List<String>
    ): List<IssueLabel>

    fun findByParentProjectId(@Param("projectId") parentProjectId: String): List<IssueLabel>
}
