package com.web.server.repositories

import com.web.server.repositories.entities.Issue
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query

interface IssuesRepository : JpaRepository<Issue, String>, JpaSpecificationExecutor<Issue> {
    fun findAllByParentProjectId(parentProjectId: String, pageable: Pageable): Page<Issue>

    @Query(
        """ SELECT i FROM Issue i
            JOIN i.parentProject p
            JOIN i.issueLabels il
            WHERE p.id = (:projectId)
            AND il.label in (:labels) """
    )
    fun findAllByProjectIdAndByLabels(projectId: String, labels: Set<String>, pageable: Pageable): Page<Issue>
}
