package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_ID
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "issues_states_config")
data class IssueState(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = DEFAULT_ID,

    var stateName: String,

    var isFirst: Boolean,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_project_id")
    var parentProject: Project
)
