package com.web.server.repositories

import com.web.server.repositories.entities.Comment
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface CommentsRepository : JpaRepository<Comment, String>, JpaSpecificationExecutor<Comment> {
    fun findAllByParentIssueId(parentIssueId: String, pageable: Pageable): Page<Comment>

    fun findAllByCreatorId(creatorId: String, pageable: Pageable): Page<Comment>
}
