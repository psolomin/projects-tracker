package com.web.server.repositories

import com.web.server.repositories.entities.User
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository

interface UsersRepository : JpaRepository<User, String> {
    fun findUserByUserName(userName: String): Optional<User>
}
