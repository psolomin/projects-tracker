package com.web.server.repositories

import com.web.server.repositories.entities.Project
import java.util.Optional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface ProjectsRepository : JpaRepository<Project, String>, JpaSpecificationExecutor<Project> {
    fun findProjectByProjectName(projectName: String): Optional<Project>

    @Query("SELECT p FROM Project p JOIN FETCH p.states WHERE p.id = (:id)")
    fun findByIdAndFetchStatesEagerly(@Param("id") id: String): Optional<Project>

    @Query("SELECT p FROM Project p JOIN FETCH p.transitions WHERE p.id = (:id)")
    fun findByIdAndFetchTransitionsEagerly(@Param("id") id: String): Optional<Project>

    @Query("SELECT p FROM Project p JOIN FETCH p.labels WHERE p.id = (:id)")
    fun findByIdAndFetchLabelsEagerly(@Param("id") id: String): Optional<Project>

    fun findAllByCreatorId(creatorId: String, pageable: Pageable): Page<Project>
}
