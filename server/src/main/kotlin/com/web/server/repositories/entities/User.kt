package com.web.server.repositories.entities

import com.web.server.constants.DefaultFieldsValues.DEFAULT_TS
import com.web.server.constants.DefaultFieldsValues.DEFAULT_VERSION
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.DefaultFieldsValues.generateNewTs
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.PrePersist
import javax.persistence.Table
import javax.persistence.Version

@Entity
@Table(name = "users")
data class User(
    @Id
    var id: String = generateNewId(),

    var userName: String,

    var userPassword: String,

    var createdAt: Instant = DEFAULT_TS,

    @Version
    var userVersion: Long = DEFAULT_VERSION
) {
    @PrePersist
    fun onCreate() {
        createdAt = generateNewTs()
    }
}
