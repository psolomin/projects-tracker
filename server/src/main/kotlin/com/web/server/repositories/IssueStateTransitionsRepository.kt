package com.web.server.repositories

import com.web.server.repositories.entities.IssueStateTransition
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface IssueStateTransitionsRepository : JpaRepository<IssueStateTransition, Long> {
    @Query(
        """ SELECT ist FROM IssueStateTransition ist
            JOIN ist.parentProject p
            JOIN ist.fromState fromSt
            JOIN ist.toState toSt
            WHERE p.id = (:projectId)
            AND fromSt.stateName = (:fromState)
            AND toSt.stateName = (:toState)
            AND p.id = (:projectId) """
    )
    fun findByParentProjectIdAndStateNames(
        projectId: String,
        fromState: String,
        toState: String
    ): Optional<IssueStateTransition>
}
