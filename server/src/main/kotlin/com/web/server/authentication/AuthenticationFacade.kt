package com.web.server.authentication

import com.web.server.businesslogic.UserLogic
import com.web.server.businesslogic.operations.RetrieveOneUserByNameOperation
import com.web.server.repositories.entities.User
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class AuthenticationFacade(
    private val userLogic: UserLogic
) {
    fun getCurrentUserEntity(): User {
        val currentUserName = SecurityContextHolder.getContext().authentication.name
        return userLogic.retrieveOneUserByName(RetrieveOneUserByNameOperation(currentUserName))
    }
}
