package com.web.server.authentication

import com.web.server.businesslogic.UserLogic
import com.web.server.businesslogic.operations.RetrieveOneUserByNameOperation
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component

@Component
class UserDetailsServiceLogic(private val userLogic: UserLogic) : UserDetailsService {
    override fun loadUserByUsername(userName: String): UserDetails {
        val usrEntity = userLogic.retrieveOneUserByName(RetrieveOneUserByNameOperation(userName))
        return org.springframework.security.core.userdetails.User(
            usrEntity.userName,
            usrEntity.userPassword,
            AuthorityUtils.createAuthorityList("all_users")
        )
    }
}
