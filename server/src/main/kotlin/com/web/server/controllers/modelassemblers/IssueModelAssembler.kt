package com.web.server.controllers.modelassemblers

import com.web.server.constants.LinkRelations.ISSUE_ADD_COMMENT
import com.web.server.constants.LinkRelations.ISSUE_COMMENTS
import com.web.server.constants.LinkRelations.ISSUE_CREATED_BY_USER
import com.web.server.constants.LinkRelations.ISSUE_PARENT_PROJECT
import com.web.server.constants.LinkRelations.ISSUE_PARENT_PROJECT_ISSUES
import com.web.server.constants.LinkRelations.ISSUE_SAME_LABELS_ISSUES
import com.web.server.constants.LinkRelations.ISSUE_UPDATED_BY_USER
import com.web.server.constants.LinkRelations.ISSUE_UPDATE_STATE
import com.web.server.controllers.CommentsResourceController
import com.web.server.controllers.IssuesResourceController
import com.web.server.controllers.ProjectsResourceController
import com.web.server.controllers.UsersResourceController
import com.web.server.controllers.requests.CreateCommentRequest
import com.web.server.controllers.requests.UpdateIssueStateRequest
import com.web.server.controllers.views.IssueView
import com.web.server.repositories.entities.Issue
import java.util.Optional
import java.util.TreeSet
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.stereotype.Component

@Component
class IssueModelAssembler : RepresentationModelAssemblerSupport<Issue, IssueView>(
    IssuesResourceController::class.java,
    IssueView::class.java
) {
    override fun toModel(entity: Issue?): IssueView {
        return populateIssueViewModel(entity!!)
    }

    private fun populateIssueViewModel(issue: Issue): IssueView {
        val issueView = populateIssueView(issue)

        val selfLink = linkTo(
            methodOn(IssuesResourceController::class.java).getIssue(issue.id))
            .withSelfRel()

        val linkToUpdateIssueState = linkTo(
            methodOn(IssuesResourceController::class.java).updateIssueState(
                id = issue.id,
                ifMatch = "some eTag",
                body = UpdateIssueStateRequest(newState = "some-new-state")
            ))
            .withRel(ISSUE_UPDATE_STATE.relName)
            .withTitle(ISSUE_UPDATE_STATE.relTitle)

        val linkToCreator = linkTo(
            methodOn(UsersResourceController::class.java).getOneUser(issue.createdBy.id))
            .withRel(ISSUE_CREATED_BY_USER.relName)
            .withTitle(ISSUE_CREATED_BY_USER.relTitle)

        val linkToParentProject = linkTo(
            methodOn(ProjectsResourceController::class.java).getProject(issue.parentProject.id))
            .withRel(ISSUE_PARENT_PROJECT.relName)
            .withTitle(ISSUE_PARENT_PROJECT.relTitle)

        val linkToParentProjectIssues = linkTo(
            methodOn(IssuesResourceController::class.java).searchIssues(
                projectId = Optional.of(issue.parentProject.id),
                creatorId = Optional.empty(),
                labelsContain = Optional.empty(),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(ISSUE_PARENT_PROJECT_ISSUES.relName)
            .expand()
            .withTitle(ISSUE_PARENT_PROJECT_ISSUES.relTitle)

        val linkToComments = linkTo(
            methodOn(CommentsResourceController::class.java).searchComments(
                issueId = Optional.of(issue.id),
                creatorId = Optional.empty(),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(ISSUE_COMMENTS.relName)
            .expand()
            .withTitle(ISSUE_COMMENTS.relTitle)

        val linkToIssuesWithSameLabels = linkTo(
            methodOn(IssuesResourceController::class.java).searchIssues(
                projectId = Optional.of(issue.parentProject.id),
                creatorId = Optional.empty(),
                labelsContain = Optional.of(issue.issueLabels.map { it.label }),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(ISSUE_SAME_LABELS_ISSUES.relName)
            .expand()
            .withTitle(ISSUE_SAME_LABELS_ISSUES.relTitle)

        val linkToCommentCreation = linkTo(
            methodOn(CommentsResourceController::class.java).createComment(
                issueId = issue.id,
                request = CreateCommentRequest("comment")
            ))
            .withRel(ISSUE_ADD_COMMENT.relName)
            .withTitle(ISSUE_ADD_COMMENT.relTitle)

        val links = arrayListOf(
            selfLink,
            linkToUpdateIssueState,
            linkToCreator,
            linkToParentProject,
            linkToParentProjectIssues,
            linkToIssuesWithSameLabels,
            linkToComments,
            linkToCommentCreation
        )

        // if issue was updated at least once, add link to user who updated:
        val updatedById = issue.updatedBy?.id
        if (updatedById != null) {
            val linkToUpdater = linkTo(
                methodOn(UsersResourceController::class.java).getOneUser(updatedById))
                .withRel(ISSUE_UPDATED_BY_USER.relName)
                .withTitle(ISSUE_UPDATED_BY_USER.relTitle)
            links.add(linkToUpdater)
        }

        return issueView.add(links)
    }

    private fun populateIssueView(issue: Issue): IssueView {
        return IssueView(
            issue.id,
            issue.issueName,
            issue.description,
            TreeSet(issue.issueLabels.map { it.label }),
            issue.createdAt,
            issue.createdBy.id,
            issue.updatedAt,
            issue.updatedBy?.id,
            issue.state.stateName
        )
    }
}
