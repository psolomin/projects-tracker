package com.web.server.controllers.requests

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_LABELS
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_STATES
import com.web.server.constants.DefaultProjectSettings.DEFAULT_ISSUE_STATE_TRANSITIONS
import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.MediaType

data class ProjectSettingsRequest(
    val issueStates: ArrayList<IssueStateSetting> = DEFAULT_ISSUE_STATES,
    val issueStateTransitions: ArrayList<IssueStateTransitionSetting> = DEFAULT_ISSUE_STATE_TRANSITIONS,
    val issueLabels: ArrayList<IssueLabelSetting> = DEFAULT_ISSUE_LABELS
) {
    fun getTemplate(
        issueStatesFormUri: String,
        issueStateTransitionsFormUri: String,
        labelFormUri: String
    ): Template {
        return Template(
            title = "Project settings",
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                // TODO: make `name` values passing through JSON serializer rules (camel or snake)
                TextProperty(name = "issue_states", required = true, templated = true, value = issueStatesFormUri),
                TextProperty(name = "issue_state_transitions", required = true, templated = true, value = issueStateTransitionsFormUri),
                TextProperty(name = "issue_labels", required = false, templated = true, value = labelFormUri)
            )
        )
    }

    fun toOperation(): SetProjectSettingsOperation =
        SetProjectSettingsOperation(issueStates, issueStateTransitions, issueLabels)
}
