package com.web.server.controllers.requestforms

import kotlin.collections.ArrayList

abstract class Property {
    abstract var name: String
    abstract var required: Boolean?
    abstract var prompt: Any?
    abstract var templated: Boolean?
    abstract var regex: String?
}

data class TextProperty(
    override var name: String,
    override var required: Boolean? = null,
    override var prompt: Any? = null,
    override var templated: Boolean? = null,
    override var regex: String? = null,
    val value: String? = null
) : Property()

data class ArrayProperty(
    override var name: String,
    override var required: Boolean? = null,
    override var prompt: Any? = null,
    override var templated: Boolean? = null,
    override var regex: String? = null,
    val value: ArrayList<String>? = ArrayList()
) : Property()
