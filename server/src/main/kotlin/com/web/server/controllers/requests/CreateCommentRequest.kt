package com.web.server.controllers.requests

import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

data class CreateCommentRequest(val commentBody: String) {
    fun getTemplate(): Template {
        return Template(
            title = "Create new comment",
            method = HttpMethod.POST.name,
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                TextProperty(name = "comment_body", required = true, value = commentBody)
            )
        )
    }
}
