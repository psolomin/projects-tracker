package com.web.server.controllers.views

import java.time.Instant
import org.springframework.hateoas.RepresentationModel
import org.springframework.hateoas.server.core.Relation

@Relation(collectionRelation = "users")
data class UserView(
    val id: String,
    val userName: String,
    val createdAt: Instant
) : RepresentationModel<UserView>()
