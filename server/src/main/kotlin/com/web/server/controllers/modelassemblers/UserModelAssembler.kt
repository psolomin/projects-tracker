package com.web.server.controllers.modelassemblers

import com.web.server.constants.LinkRelations.USER_CREATED_COMMENTS
import com.web.server.constants.LinkRelations.USER_CREATED_ISSUES
import com.web.server.constants.LinkRelations.USER_CREATED_PROJECTS
import com.web.server.controllers.CommentsResourceController
import com.web.server.controllers.IssuesResourceController
import com.web.server.controllers.ProjectsResourceController
import com.web.server.controllers.UsersResourceController
import com.web.server.controllers.views.UserView
import com.web.server.repositories.entities.User
import java.util.Optional
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.stereotype.Component

@Component
class UserModelAssembler : RepresentationModelAssemblerSupport<User, UserView>(
    UsersResourceController::class.java,
    UserView::class.java
) {
    override fun toModel(entity: User?): UserView {
        return populateUserViewModel(entity!!)
    }

    private fun populateUserViewModel(user: User): UserView {
        val userView = UserView(user.id, user.userName, user.createdAt)

        val selfLink = linkTo(
            methodOn(UsersResourceController::class.java).getOneUser(user.id))
            .withSelfRel()

        val linkToProjects = linkTo(
            methodOn(ProjectsResourceController::class.java).searchProjects(
                creatorId = Optional.of(userView.id),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(USER_CREATED_PROJECTS.relName)
            .expand()
            .withTitle(USER_CREATED_PROJECTS.relTitle)

        val linkToIssues = linkTo(
            methodOn(IssuesResourceController::class.java).searchIssues(
                projectId = Optional.empty(),
                creatorId = Optional.of(userView.id),
                labelsContain = Optional.empty(),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(USER_CREATED_ISSUES.relName)
            .expand()
            .withTitle(USER_CREATED_ISSUES.relTitle)

        val linkToComments = linkTo(
            methodOn(CommentsResourceController::class.java).searchComments(
                issueId = Optional.empty(),
                creatorId = Optional.of(userView.id),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(USER_CREATED_COMMENTS.relName)
            .expand()
            .withTitle(USER_CREATED_COMMENTS.relTitle)

        userView.add(selfLink, linkToProjects, linkToIssues, linkToComments)
        return userView
    }
}
