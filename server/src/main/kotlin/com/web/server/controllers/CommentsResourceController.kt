package com.web.server.controllers

import com.web.server.authentication.AuthenticationFacade
import com.web.server.businesslogic.CommentLogic
import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneCommentOperation
import com.web.server.businesslogic.operations.SearchCommentsOperation
import com.web.server.constants.LinkRelations
import com.web.server.constants.Uris.COMMENTS_RESOURCE_URI
import com.web.server.constants.Uris.ISSUES_RESOURCE_URI
import com.web.server.controllers.modelassemblers.CommentModelAssembler
import com.web.server.controllers.modelassemblers.ResourcePageLinkBuilder.buildLinksToPages
import com.web.server.controllers.requestforms.RepresentationModelWithTemplates
import com.web.server.controllers.requests.CreateCommentRequest
import com.web.server.controllers.searchspecifications.CommentSpecWithCreatorId
import com.web.server.controllers.searchspecifications.CommentSpecWithIssueId
import com.web.server.controllers.views.CommentView
import com.web.server.repositories.entities.Comment
import com.web.server.repositories.entities.User
import java.util.Optional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CommentsResourceController(
    private val commentLogic: CommentLogic,
    private val authenticationFacade: AuthenticationFacade,
    private val modelAssembler: CommentModelAssembler
) {
    @GetMapping("$ISSUES_RESOURCE_URI/{issueId}$COMMENTS_RESOURCE_URI")
    fun createCommentTemplate(
        @PathVariable("issueId") issueId: String
    ): ResponseEntity<RepresentationModelWithTemplates> {
        val em = createCommentActionTemplate(issueId)
        return ResponseEntity.status(HttpStatus.OK).body(em)
    }

    @PostMapping("$ISSUES_RESOURCE_URI/{issueId}$COMMENTS_RESOURCE_URI")
    fun createComment(
        @PathVariable("issueId") issueId: String,
        @RequestBody request: CreateCommentRequest
    ): ResponseEntity<CommentView> {
        val operation = CreateCommentOperation(request.commentBody, currentUser().id, issueId)
        val comment = commentLogic.createComment(operation)
        return ResponseEntity.status(HttpStatus.CREATED).body(modelAssembler.toModel(comment))
    }

    @GetMapping("$COMMENTS_RESOURCE_URI/{id}")
    fun getComment(@PathVariable("id") id: String): ResponseEntity<CommentView> {
        val operation = RetrieveOneCommentOperation(id)
        val comment = commentLogic.retrieveOneComment(operation)
        return ResponseEntity.status(HttpStatus.OK).body(modelAssembler.toModel(comment))
    }

    @GetMapping("$COMMENTS_RESOURCE_URI/filters")
    fun searchComments(
        @RequestParam("issueId", required = false) issueId: Optional<String>,
        @RequestParam("creatorId", required = false) creatorId: Optional<String>,
        requestPageable: Pageable,
        assembler: PagedResourcesAssembler<Comment>
    ): ResponseEntity<PagedModel<CommentView>> {
        val pageOfComments = getSearchResults(requestPageable, issueId, creatorId)
        val linkBuilder = linkTo(thisClass().searchComments(issueId, creatorId, requestPageable, assembler))
            .toUriComponentsBuilder()

        val resource = buildLinksToPages(
            linkBuilder,
            assembler,
            modelAssembler,
            requestPageable,
            pageOfComments
        )

        issueId.ifPresent {
            val linkToParentIssue = linkToParentIssue(it)
            val linkToCreateComment = linkToCreateComment(it)
            resource.add(linkToParentIssue, linkToCreateComment)
        }

        creatorId.ifPresent {
            val linkToCreator = linkToCreator(it)
            resource.add(linkToCreator)
        }

        return ResponseEntity.status(HttpStatus.OK).body(resource)
    }

    private fun thisClass() = methodOn(this::class.java)

    private fun currentUser(): User = authenticationFacade.getCurrentUserEntity()

    private fun linkToCreator(userId: String) = linkTo(methodOn(UsersResourceController::class.java)
        .getOneUser(userId))
        .withRel(LinkRelations.COMMENT_CREATED_BY_USER.relName)
        .withTitle(LinkRelations.COMMENT_CREATED_BY_USER.relTitle)

    private fun linkToParentIssue(issueId: String) = linkTo(methodOn(IssuesResourceController::class.java)
        .getIssue(issueId))
        .withRel(LinkRelations.COMMENT_PARENT_ISSUE_COMMENTS.relName)
        .withTitle(LinkRelations.COMMENT_PARENT_ISSUE_COMMENTS.relTitle)

    private fun linkToCreateComment(issueId: String) = linkTo(thisClass().createCommentTemplate(issueId))
        .withRel(LinkRelations.CREATE_NEW_COMMENT.relName)
        .withTitle(LinkRelations.CREATE_NEW_COMMENT.relTitle)

    private fun getSearchResults(
        requestPageable: Pageable,
        issueId: Optional<String>,
        creatorId: Optional<String>
    ): Page<Comment> {
        val spec = CommentSpecWithIssueId(issueId.orElse(null))
            .and(CommentSpecWithCreatorId(creatorId.orElse(null)))

        val op = SearchCommentsOperation(spec, requestPageable)
        return commentLogic.searchComments(op)
    }

    private fun createCommentActionTemplate(issueId: String): RepresentationModelWithTemplates {
        val r = CreateCommentRequest("this is a comment")
        val selfLink = linkTo(thisClass().createComment(issueId, r)).withSelfRel()
        val tpl = r.getTemplate()
        return RepresentationModelWithTemplates(mutableMapOf("default" to tpl)).add(selfLink)
    }
}
