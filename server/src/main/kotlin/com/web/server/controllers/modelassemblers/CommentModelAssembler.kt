package com.web.server.controllers.modelassemblers

import com.web.server.constants.LinkRelations.COMMENT_CREATED_BY_USER
import com.web.server.constants.LinkRelations.COMMENT_PARENT_ISSUE
import com.web.server.constants.LinkRelations.COMMENT_PARENT_ISSUE_COMMENTS
import com.web.server.constants.LinkRelations.COMMENT_PARENT_PROJECT
import com.web.server.controllers.CommentsResourceController
import com.web.server.controllers.IssuesResourceController
import com.web.server.controllers.ProjectsResourceController
import com.web.server.controllers.UsersResourceController
import com.web.server.controllers.views.CommentView
import com.web.server.repositories.entities.Comment
import java.util.Optional
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.stereotype.Component

@Component
class CommentModelAssembler : RepresentationModelAssemblerSupport<Comment, CommentView>(
    CommentsResourceController::class.java,
    CommentView::class.java
) {
    override fun toModel(entity: Comment?): CommentView {
        return populateCommentViewModel(entity!!)
    }

    private fun populateCommentViewModel(comment: Comment): CommentView {
        val commentView = populateView(comment)

        val selfLink = linkTo(
            methodOn(CommentsResourceController::class.java).getComment(comment.id))
            .withSelfRel()

        val linkToCreator = linkTo(
            methodOn(UsersResourceController::class.java).getOneUser(comment.creator.id))
            .withRel(COMMENT_CREATED_BY_USER.relName)
            .withTitle(COMMENT_CREATED_BY_USER.relTitle)

        val linkToParentProject = linkTo(
            methodOn(ProjectsResourceController::class.java).getProject(comment.parentIssue.parentProject.id))
            .withRel(COMMENT_PARENT_PROJECT.relName)
            .withTitle(COMMENT_PARENT_PROJECT.relTitle)

        val linkToParentIssue = linkTo(
            methodOn(IssuesResourceController::class.java).getIssue(comment.parentIssue.id))
            .withRel(COMMENT_PARENT_ISSUE.relName)
            .withTitle(COMMENT_PARENT_ISSUE.relTitle)

        val linkToParentIssueComments = linkTo(
            methodOn(CommentsResourceController::class.java).searchComments(
                issueId = Optional.of(comment.parentIssue.id),
                creatorId = Optional.empty(),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(COMMENT_PARENT_ISSUE_COMMENTS.relName)
            .expand()
            .withTitle(COMMENT_PARENT_ISSUE_COMMENTS.relTitle)

        val links = arrayListOf(
            selfLink,
            linkToCreator,
            linkToParentIssue,
            linkToParentProject,
            linkToParentIssueComments
        )

        return commentView.add(links)
    }

    private fun populateView(c: Comment): CommentView {
        return CommentView(
            c.id,
            c.commentBody,
            c.createdAt,
            c.creator.id,
            c.parentIssue.id
        )
    }
}
