package com.web.server.controllers.requestforms

open class Template(
    val title: String = "",
    val method: String? = "",
    val contentType: String = "",
    val properties: Set<Property> = setOf()
)
