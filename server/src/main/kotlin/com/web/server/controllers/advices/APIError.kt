package com.web.server.controllers.advices

data class APIError(val message: String)
