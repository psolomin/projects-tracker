package com.web.server.controllers.views

import java.time.Instant
import org.springframework.hateoas.RepresentationModel
import org.springframework.hateoas.server.core.Relation

@Relation(collectionRelation = "comments")
data class CommentView(
    val id: String,
    val commentBody: String,
    val createdAt: Instant,
    val createdBy: String,
    val parentIssueId: String
) : RepresentationModel<CommentView>()
