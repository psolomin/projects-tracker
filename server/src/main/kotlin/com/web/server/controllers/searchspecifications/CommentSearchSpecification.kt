package com.web.server.controllers.searchspecifications

import com.web.server.repositories.entities.Comment
import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.User
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification

class CommentSpecWithCreatorId(val creatorId: String?) : Specification<Comment> {
    override fun toPredicate(
        root: Root<Comment>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (creatorId == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val creator = root.join<Comment, User>("creator")
        return criteriaBuilder.equal(creator.get<User>("id"), this.creatorId)
    }
}

class CommentSpecWithIssueId(val issueId: String?) : Specification<Comment> {
    override fun toPredicate(
        root: Root<Comment>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (issueId == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val parentIssue = root.join<Comment, Issue>("parentIssue")
        return criteriaBuilder.equal(parentIssue.get<Issue>("id"), this.issueId)
    }
}
