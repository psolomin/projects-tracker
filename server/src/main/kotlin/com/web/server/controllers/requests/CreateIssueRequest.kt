package com.web.server.controllers.requests

import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.controllers.requestforms.ArrayProperty
import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.User
import java.util.SortedSet
import kotlin.collections.ArrayList
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

data class CreateIssueRequest(
    val issueName: String,
    val description: String,
    val labels: ArrayList<String> = ArrayList()
) {
    fun getTemplate(labelsSet: SortedSet<IssueLabel>): Template {
        val tplLabels = ArrayList(labelsSet.map { it.label })
        return Template(
            title = "Create new issue",
            method = HttpMethod.POST.name,
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                TextProperty(name = "issue_name", required = true, value = issueName),
                TextProperty(name = "description", required = true, value = description),
                ArrayProperty(name = "labels", required = false, templated = false, value = tplLabels)
            )
        )
    }

    fun toOperation(projectId: String, creator: User): CreateIssueOperation =
        CreateIssueOperation(issueName, description, creator.id, projectId, labels)
}
