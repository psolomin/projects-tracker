package com.web.server.controllers.requests

import com.web.server.businesslogic.operations.UpdateIssueStateOperation
import com.web.server.repositories.entities.User

data class UpdateIssueStateRequest(
    val newState: String
) {
    // TODO: missing template
    fun toOperation(issueId: String, updater: User, assumedVersion: Long): UpdateIssueStateOperation =
        UpdateIssueStateOperation(issueId, newState, updater.id, assumedVersion)
}
