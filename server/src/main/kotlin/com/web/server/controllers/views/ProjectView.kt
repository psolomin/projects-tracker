package com.web.server.controllers.views

import java.time.Instant
import org.springframework.hateoas.RepresentationModel
import org.springframework.hateoas.server.core.Relation

@Relation(collectionRelation = "projects")
data class ProjectView(
    val id: String,
    val projectName: String,
    val description: String,
    val createdAt: Instant,
    val creator: String
) : RepresentationModel<ProjectView>()
