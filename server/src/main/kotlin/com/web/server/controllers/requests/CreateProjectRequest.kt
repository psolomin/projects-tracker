package com.web.server.controllers.requests

import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import com.web.server.repositories.entities.User
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

data class CreateProjectRequest(
    val projectName: String,
    val description: String,
    val projectSettings: ProjectSettingsRequest?
) {
    fun getTemplate(projectSettingsDocUri: String): Template {
        return Template(
            title = "Create new project",
            method = HttpMethod.POST.name,
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                TextProperty(name = "project_name", required = true, value = projectName),
                TextProperty(name = "description", required = true, value = description),
                TextProperty(name = "project_settings", templated = true, required = false, value = projectSettingsDocUri)
            )
        )
    }

    fun toOperation(user: User): CreateProjectOperation = CreateProjectOperation(projectName, description, user.id)
}
