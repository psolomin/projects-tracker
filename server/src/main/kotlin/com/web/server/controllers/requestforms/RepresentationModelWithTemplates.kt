package com.web.server.controllers.requestforms

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.hateoas.RepresentationModel

data class RepresentationModelWithTemplates(
    @JsonProperty("_templates") var templates: MutableMap<String, Template>
) : RepresentationModel<RepresentationModelWithTemplates>() {
    fun addTemplate(name: String, template: Template): RepresentationModelWithTemplates {
        templates[name] = template
        return this
    }
}
