package com.web.server.controllers.searchspecifications

import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification

class IssueSpecWithCreatorId(val creatorId: String?) : Specification<Issue> {
    override fun toPredicate(
        root: Root<Issue>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (creatorId == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val creator = root.join<Issue, User>("createdBy")
        return criteriaBuilder.equal(creator.get<User>("id"), this.creatorId)
    }
}

class IssueSpecWithProjectId(val projectId: String?) : Specification<Issue> {
    override fun toPredicate(
        root: Root<Issue>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (projectId == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val parentProject = root.join<Issue, Project>("parentProject")
        return criteriaBuilder.equal(parentProject.get<Project>("id"), this.projectId)
    }
}

class IssueSpecWithLabels(val labels: List<String>?) : Specification<Issue> {
    override fun toPredicate(
        root: Root<Issue>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (labels == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val issueLabels = root.join<Issue, IssueLabel>("issueLabels").get<String>("label")
        val inClause = criteriaBuilder.`in`(issueLabels)
        this.labels.forEach { inClause.value(it) }
        return inClause
    }
}
