package com.web.server.controllers

import com.web.server.authentication.AuthenticationFacade
import com.web.server.businesslogic.ProjectLogic
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.SearchProjectsOperation
import com.web.server.constants.LinkRelations
import com.web.server.constants.Uris.PROJECTS_RESOURCE_URI
import com.web.server.constants.Uris.PROJECT_SETTINGS_TEMPLATE_ID
import com.web.server.controllers.modelassemblers.ProjectModelAssembler
import com.web.server.controllers.modelassemblers.ResourcePageLinkBuilder.buildLinksToPages
import com.web.server.controllers.requestforms.RepresentationModelWithTemplates
import com.web.server.controllers.requests.CreateProjectRequest
import com.web.server.controllers.requests.ProjectSettingsRequest
import com.web.server.controllers.searchspecifications.ProjectSpecWithCreatorId
import com.web.server.controllers.views.ProjectView
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import java.util.Optional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.Link
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(PROJECTS_RESOURCE_URI)
class ProjectsResourceController(
    private val projectLogic: ProjectLogic,
    private val authenticationFacade: AuthenticationFacade,
    private val modelAssembler: ProjectModelAssembler
) {
    @GetMapping
    fun createProjectTemplate(): ResponseEntity<RepresentationModelWithTemplates> {
        val r = createProjectRequestExample()
        val selfLink = selfLink(r)
        val tpl = linkToCreationTemplate(r)
        val em = RepresentationModelWithTemplates(mutableMapOf("default" to tpl)).add(selfLink)
        return ResponseEntity.status(HttpStatus.OK).body(em)
    }

    @PostMapping
    fun createProject(@RequestBody request: CreateProjectRequest): ResponseEntity<ProjectView> {
        val operation = request.toOperation(currentUser())
        val settingsOperation = request.projectSettings?.toOperation()
        val p = projectLogic.createProject(operation, settingsOperation)
        val model = modelAssembler.toModel(p)
        return ResponseEntity.status(HttpStatus.CREATED).body(model)
    }

    @GetMapping("/{projectId}")
    fun getProject(@PathVariable("projectId") projectId: String): ResponseEntity<ProjectView> {
        val operation = RetrieveOneProjectOperation(projectId)
        val p = projectLogic.retrieveOneProject(operation)
        val model = modelAssembler.toModel(p)
        return ResponseEntity.status(HttpStatus.OK).body(model)
    }

    @GetMapping("/filters")
    fun searchProjects(
        @RequestParam("creatorId", required = false) creatorId: Optional<String>,
        requestPageable: Pageable,
        assembler: PagedResourcesAssembler<Project>
    ): ResponseEntity<PagedModel<ProjectView>> {
        val pageOfProjects = searchPageOfProjects(creatorId, requestPageable)

        val linkBuilder = linkTo(thisClass().searchProjects(creatorId, requestPageable, assembler))
            .toUriComponentsBuilder()

        val resource = buildLinksToPages(
            linkBuilder,
            assembler,
            modelAssembler,
            requestPageable,
            pageOfProjects
        )

        creatorId.ifPresent {
            val linkToCreator = linkToCreator(it)
            resource.add(linkToCreator)
        }

        val linkToProjectCreation = linkToProjectCreation()
        resource.add(linkToProjectCreation)

        return ResponseEntity.status(HttpStatus.OK).body(resource)
    }

    @GetMapping("/home")
    fun projectsHome(
        requestPageable: Pageable,
        assembler: PagedResourcesAssembler<Project>
    ): ResponseEntity<PagedModel<ProjectView>> {
        return searchProjects(Optional.of(currentUser().id), requestPageable, assembler)
    }

    private fun selfLink(r: CreateProjectRequest): Link = linkTo(thisClass().createProject(r)).withSelfRel()

    private fun createProjectRequestExample(): CreateProjectRequest = CreateProjectRequest(
        "name",
        "desc",
        ProjectSettingsRequest(
            arrayListOf(IssueStateSetting("open", true)),
            arrayListOf(IssueStateTransitionSetting("open", "closed"))
        )
    )

    private fun linkToCreationTemplate(r: CreateProjectRequest) = r.getTemplate(
        linkTo(methodOn(TemplatesResourceController::class.java)
            .projectSettingsTemplates(PROJECT_SETTINGS_TEMPLATE_ID)).toString()
    )

    private fun thisClass() = methodOn(this::class.java)

    private fun linkToCreator(creatorId: String): Link = linkTo(methodOn(UsersResourceController::class.java)
        .getOneUser(creatorId))
        .withRel(LinkRelations.PROJECT_CREATED_BY_USER.relName)
        .withTitle(LinkRelations.PROJECT_CREATED_BY_USER.relTitle)

    private fun linkToProjectCreation(): Link = linkTo(thisClass().createProjectTemplate())
        .withRel(LinkRelations.CREATE_NEW_PROJECT.relName)
        .withTitle(LinkRelations.CREATE_NEW_PROJECT.relTitle)

    private fun searchPageOfProjects(creatorId: Optional<String>, requestPageable: Pageable): Page<Project> {
        val spec = ProjectSpecWithCreatorId(creatorId.orElse(null))
        val op = SearchProjectsOperation(spec, requestPageable)
        return projectLogic.searchProjects(op)
    }

    private fun currentUser(): User = authenticationFacade.getCurrentUserEntity()
}
