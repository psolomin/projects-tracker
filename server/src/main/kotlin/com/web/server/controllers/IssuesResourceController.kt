package com.web.server.controllers

import com.web.server.authentication.AuthenticationFacade
import com.web.server.businesslogic.IssueLogic
import com.web.server.businesslogic.ProjectSettingsLogic
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.SearchIssuesOperation
import com.web.server.constants.LinkRelations
import com.web.server.constants.Uris.ISSUES_RESOURCE_URI
import com.web.server.constants.Uris.PROJECTS_RESOURCE_URI
import com.web.server.controllers.controllererrors.RequiredHeaderNotFoundOrInvalidException
import com.web.server.controllers.modelassemblers.IssueModelAssembler
import com.web.server.controllers.modelassemblers.ResourcePageLinkBuilder.buildLinksToPages
import com.web.server.controllers.requestforms.RepresentationModelWithTemplates
import com.web.server.controllers.requests.CreateIssueRequest
import com.web.server.controllers.requests.UpdateIssueStateRequest
import com.web.server.controllers.searchspecifications.IssueSpecWithCreatorId
import com.web.server.controllers.searchspecifications.IssueSpecWithLabels
import com.web.server.controllers.searchspecifications.IssueSpecWithProjectId
import com.web.server.controllers.views.IssueView
import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.User
import java.util.Optional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class IssuesResourceController(
    private val issueLogic: IssueLogic,
    private val projectSettingsLogic: ProjectSettingsLogic,
    private val authenticationFacade: AuthenticationFacade,
    private val modelAssembler: IssueModelAssembler
) {
    @GetMapping("$PROJECTS_RESOURCE_URI/{projectId}$ISSUES_RESOURCE_URI")
    fun createIssueTemplate(
        @PathVariable("projectId") projectId: String
    ): ResponseEntity<RepresentationModelWithTemplates> {
        val r = CreateIssueRequest("name", "desc")
        val selfLink = linkTo(thisClass().createIssue(projectId, r)).withSelfRel()
        val allowedLabelsText = projectSettingsLogic.findAvailableLabels(projectId)
        val tpl = r.getTemplate(allowedLabelsText)
        val em = RepresentationModelWithTemplates(mutableMapOf("default" to tpl)).add(selfLink)
        return ResponseEntity.status(HttpStatus.OK).body(em)
    }

    @PostMapping("$PROJECTS_RESOURCE_URI/{projectId}$ISSUES_RESOURCE_URI")
    fun createIssue(
        @PathVariable("projectId") projectId: String,
        @RequestBody request: CreateIssueRequest
    ): ResponseEntity<IssueView> {
        val operation = request.toOperation(projectId, currentUser())
        val issue = issueLogic.createIssue(operation)
        return responseWithIssue(HttpStatus.CREATED, issue)
    }

    @GetMapping("$ISSUES_RESOURCE_URI/{id}")
    fun getIssue(@PathVariable("id") id: String): ResponseEntity<IssueView> {
        val operation = RetrieveOneIssueOperation(id)
        val issue = issueLogic.retrieveOneIssue(operation)
        return responseWithIssue(HttpStatus.OK, issue)
    }

    @PatchMapping("$ISSUES_RESOURCE_URI/{id}")
    fun updateIssueState(
        @PathVariable("id") id: String,
        // `required = false` - to avoid catching generic `ServletRequestBindingException`
        @RequestHeader("If-Match", required = false) ifMatch: String?,
        @RequestBody body: UpdateIssueStateRequest
    ): ResponseEntity<IssueView> {
        val assumedVersion = tryParsingIssueVersion(ifMatch)
        val operation = body.toOperation(id, currentUser(), assumedVersion)
        val issue = issueLogic.updateState(operation)
        return responseWithIssue(HttpStatus.OK, issue)
    }

    @GetMapping("$ISSUES_RESOURCE_URI/filters")
    fun searchIssues(
        @RequestParam("projectId", required = false) projectId: Optional<String>,
        @RequestParam("creatorId", required = false) creatorId: Optional<String>,
        @RequestParam("labelsContain", required = false) labelsContain: Optional<List<String>>,
        requestPageable: Pageable,
        assembler: PagedResourcesAssembler<Issue>
    ): ResponseEntity<PagedModel<IssueView>> {
        val pageOfIssues = foundIssues(projectId, creatorId, labelsContain, requestPageable)
        val linkBuilder = linkTo(
            thisClass().searchIssues(projectId, creatorId, labelsContain, requestPageable, assembler)
        ).toUriComponentsBuilder()

        val resource = buildLinksToPages(
            linkBuilder,
            assembler,
            modelAssembler,
            requestPageable,
            pageOfIssues
        )

        projectId.ifPresent {
            val linkToParentProject = linkToParentProject(it)
            val linkToCreateIssue = linkToCreateIssue(it)
            resource.add(linkToParentProject, linkToCreateIssue)
        }

        creatorId.ifPresent {
            val linkToCreator = linkToCreator(it)
            resource.add(linkToCreator)
        }

        return ResponseEntity.status(HttpStatus.OK).body(resource)
    }

    private fun eTag(issue: Issue) = issue.issueVersion.toString()

    private fun responseWithIssue(status: HttpStatus, issue: Issue) =
        ResponseEntity.status(status).eTag(eTag(issue)).body(modelAssembler.toModel(issue))

    private fun tryParsingIssueVersion(ifMatch: String?) =
        try {
            val readVersion = ifMatch?.replace("\"", "")?.toLong()
            readVersion ?: throw RequiredHeaderNotFoundOrInvalidException("If-Match")
        } catch (e2: NumberFormatException) {
            throw RequiredHeaderNotFoundOrInvalidException("If-Match")
        }

    private fun thisClass() = methodOn(this::class.java)

    private fun searchIssuesSpec(
        projectId: Optional<String>,
        creatorId: Optional<String>,
        labelsContain: Optional<List<String>>
    ) = IssueSpecWithProjectId(projectId.orElse(null))
        .and(IssueSpecWithCreatorId(creatorId.orElse(null)))
        ?.and(IssueSpecWithLabels(labelsContain.orElse(null)))

    private fun foundIssues(
        projectId: Optional<String>,
        creatorId: Optional<String>,
        labelsContain: Optional<List<String>>,
        requestPageable: Pageable
    ): Page<Issue> {
        val spec = searchIssuesSpec(projectId, creatorId, labelsContain)
        val op = SearchIssuesOperation(spec, requestPageable)
        return issueLogic.searchIssues(op)
    }

    private fun linkToParentProject(projectId: String) =
        linkTo(methodOn(ProjectsResourceController::class.java).getProject(projectId))
            .withRel(LinkRelations.ISSUE_PARENT_PROJECT.relName)
            .withTitle(LinkRelations.ISSUE_PARENT_PROJECT.relTitle)

    private fun linkToCreateIssue(projectId: String) =
        linkTo(thisClass().createIssueTemplate(projectId))
            .withRel(LinkRelations.CREATE_NEW_ISSUE.relName)
            .withTitle(LinkRelations.CREATE_NEW_ISSUE.relTitle)

    private fun linkToCreator(creatorId: String) =
        linkTo(methodOn(UsersResourceController::class.java).getOneUser(creatorId))
            .withRel(LinkRelations.ISSUE_CREATED_BY_USER.relName)
            .withTitle(LinkRelations.ISSUE_CREATED_BY_USER.relTitle)

    private fun currentUser(): User = authenticationFacade.getCurrentUserEntity()
}
