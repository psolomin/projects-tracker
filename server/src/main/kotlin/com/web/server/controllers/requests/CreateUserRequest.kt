package com.web.server.controllers.requests

import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requestforms.TextProperty
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType

// TODO: create response body processor for cases
//  when a field is missing or is set to NULL
data class CreateUserRequest(
    val userName: String,
    val userPassword: String
) {
    fun getTemplate(): Template {
        return Template(
            title = "Create new user",
            method = HttpMethod.POST.name,
            contentType = MediaType.APPLICATION_JSON_VALUE,
            properties = setOf(
                // TODO: make `name` values passing through JSON serializer rules (camel or snake)
                TextProperty(name = "user_name", prompt = "User name", required = true, value = userName),
                TextProperty(name = "user_password", prompt = "Password", required = true, value = userPassword)
            )
        )
    }

    fun toOperation(): CreateUserOperation = CreateUserOperation(userName, userPassword)
}
