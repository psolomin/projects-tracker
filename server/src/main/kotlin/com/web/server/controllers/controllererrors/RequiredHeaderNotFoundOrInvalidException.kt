package com.web.server.controllers.controllererrors

class RequiredHeaderNotFoundOrInvalidException(headerName: String) : RuntimeException(
    "Required header $headerName not found or invalid."
)
