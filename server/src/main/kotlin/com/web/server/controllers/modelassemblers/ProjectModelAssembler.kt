package com.web.server.controllers.modelassemblers

import com.web.server.constants.LinkRelations.PROJECT_ADD_ISSUE
import com.web.server.constants.LinkRelations.PROJECT_CREATED_BY_USER
import com.web.server.constants.LinkRelations.PROJECT_ISSUES
import com.web.server.controllers.IssuesResourceController
import com.web.server.controllers.ProjectsResourceController
import com.web.server.controllers.UsersResourceController
import com.web.server.controllers.requests.CreateIssueRequest
import com.web.server.controllers.views.ProjectView
import com.web.server.repositories.entities.Project
import java.util.Optional
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.stereotype.Component

@Component
class ProjectModelAssembler : RepresentationModelAssemblerSupport<Project, ProjectView>(
    ProjectsResourceController::class.java,
    ProjectView::class.java
) {
    override fun toModel(entity: Project?): ProjectView {
        return populateProjectViewModel(entity!!)
    }

    private fun populateProjectViewModel(p: Project): ProjectView {
        val projectView = populateProjectView(p)

        val linkToSelf = linkTo(
            methodOn(ProjectsResourceController::class.java).getProject(p.id))
            .withSelfRel()

        val linkToCreator = linkTo(
            methodOn(UsersResourceController::class.java).getOneUser(projectView.creator))
            .withRel(PROJECT_CREATED_BY_USER.relName)
            .withTitle(PROJECT_CREATED_BY_USER.relTitle)

        val linkToIssues = linkTo(
            methodOn(IssuesResourceController::class.java).searchIssues(
                projectId = Optional.of(p.id),
                creatorId = Optional.empty(),
                labelsContain = Optional.empty(),
                requestPageable = PageRequest.of(0, 5),
                assembler = PagedResourcesAssembler(null, null)
            ))
            .withRel(PROJECT_ISSUES.relName)
            .expand()
            .withTitle(PROJECT_ISSUES.relTitle)

        val linkToIssueCreation = linkTo(
            methodOn(IssuesResourceController::class.java).createIssue(
                projectId = p.id,
                request = CreateIssueRequest("name", "desc")
            ))
            .withRel(PROJECT_ADD_ISSUE.relName)
            .withTitle(PROJECT_ADD_ISSUE.relTitle)

        return projectView.add(listOf(linkToSelf, linkToCreator, linkToIssues, linkToIssueCreation))
    }

    private fun populateProjectView(p: Project): ProjectView {
        return ProjectView(p.id, p.projectName, p.description, p.createdAt, p.creator.id)
    }
}
