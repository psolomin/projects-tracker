package com.web.server.controllers.modelassemblers

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.Link
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.RepresentationModel
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport
import org.springframework.web.util.UriComponentsBuilder

object ResourcePageLinkBuilder {

    fun <T, D : RepresentationModel<D>> buildLinksToPages(
        linkBuilder: UriComponentsBuilder,
        pagedResourceAssembler: PagedResourcesAssembler<T>,
        modelAssembler: RepresentationModelAssemblerSupport<T, D>,
        requestPageable: Pageable,
        pageOfEntities: Page<T>
    ): PagedModel<D> {
        pagedResourceAssembler.setForceFirstAndLastRels(true)

        // TODO: ordering in query string doesn't come to selfLink for some reason
        val configuredLinkBuilder = configureLinkBuilder(linkBuilder, requestPageable)
        val selfLink = Link(configuredLinkBuilder.toUriString()).withSelfRel().expand()

        return pagedResourceAssembler.toModel(pageOfEntities, modelAssembler, selfLink)
    }

    private fun configureLinkBuilder(
        linkBuilder: UriComponentsBuilder,
        requestPageable: Pageable
    ): UriComponentsBuilder {
        val pageNumber = requestPageable.pageNumber
        val pageSize = requestPageable.pageSize
        linkBuilder.replaceQueryParam("page", pageNumber)
        linkBuilder.replaceQueryParam("size", pageSize)
        return linkBuilder
    }
}
