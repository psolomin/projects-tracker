package com.web.server.controllers.views

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.Instant
import java.util.SortedSet
import org.springframework.hateoas.RepresentationModel
import org.springframework.hateoas.server.core.Relation

@Relation(collectionRelation = "issues")
data class IssueView(
    val id: String,
    val issueName: String,
    val description: String,
    @JsonInclude(JsonInclude.Include.ALWAYS)
    val labels: SortedSet<String>,
    val createdAt: Instant,
    val createdBy: String,
    val updatedAt: Instant?,
    val updatedBy: String?,
    val state: String
) : RepresentationModel<IssueView>()
