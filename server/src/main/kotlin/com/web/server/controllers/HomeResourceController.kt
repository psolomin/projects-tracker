package com.web.server.controllers

import com.web.server.constants.LinkRelations.ALL_COMMENTS
import com.web.server.constants.LinkRelations.ALL_ISSUES
import com.web.server.constants.LinkRelations.ALL_PROJECTS
import com.web.server.constants.LinkRelations.ALL_USERS
import java.util.Optional
import org.springframework.data.domain.PageRequest
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.CollectionModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeResourceController {
    @GetMapping
    fun home(): ResponseEntity<CollectionModel<Any>> {
        val em = CollectionModel<Any>(emptySet<Any>(),
            selfLink(),
            linkToUsers(),
            linkToProjects(),
            linkToIssues(),
            linkToComments()
        )
        return ResponseEntity.status(HttpStatus.OK).body(em)
    }

    private fun selfLink() = linkTo(methodOn(this::class.java).home()).withSelfRel()

    private fun linkToUsers() = linkTo(
        methodOn(UsersResourceController::class.java).getAllUsers(
            requestPageable = PageRequest.of(0, 5),
            assembler = PagedResourcesAssembler(null, null)
        ))
        .withRel(ALL_USERS.relName)
        .withTitle(ALL_USERS.relTitle)

    private fun linkToProjects() = linkTo(
        methodOn(ProjectsResourceController::class.java).searchProjects(
            creatorId = Optional.empty(),
            requestPageable = PageRequest.of(0, 5),
            assembler = PagedResourcesAssembler(null, null)
        ))
        .withRel(ALL_PROJECTS.relName)
        .expand()
        .withTitle(ALL_PROJECTS.relTitle)

    private fun linkToIssues() = linkTo(
        methodOn(IssuesResourceController::class.java).searchIssues(
            projectId = Optional.empty(),
            creatorId = Optional.empty(),
            labelsContain = Optional.empty(),
            requestPageable = PageRequest.of(0, 5),
            assembler = PagedResourcesAssembler(null, null)
        ))
        .withRel(ALL_ISSUES.relName)
        .expand()
        .withTitle(ALL_ISSUES.relTitle)

    private fun linkToComments() = linkTo(
        methodOn(CommentsResourceController::class.java).searchComments(
            issueId = Optional.empty(),
            creatorId = Optional.empty(),
            requestPageable = PageRequest.of(0, 5),
            assembler = PagedResourcesAssembler(null, null)
        ))
        .withRel(ALL_COMMENTS.relName)
        .expand()
        .withTitle(ALL_COMMENTS.relTitle)
}
