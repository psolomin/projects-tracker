package com.web.server.controllers

import com.web.server.authentication.AuthenticationFacade
import com.web.server.businesslogic.UserLogic
import com.web.server.businesslogic.operations.RetrieveAllUsersOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.constants.Uris.USERS_RESOURCE_URI
import com.web.server.controllers.modelassemblers.ResourcePageLinkBuilder.buildLinksToPages
import com.web.server.controllers.modelassemblers.UserModelAssembler
import com.web.server.controllers.requestforms.RepresentationModelWithTemplates
import com.web.server.controllers.requests.CreateUserRequest
import com.web.server.controllers.views.UserView
import com.web.server.repositories.entities.User
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(USERS_RESOURCE_URI)
class UsersResourceController(
    private val userLogic: UserLogic,
    private val modelAssembler: UserModelAssembler,
    private val authenticationFacade: AuthenticationFacade
) {
    @GetMapping
    fun signUpTemplate(): ResponseEntity<RepresentationModelWithTemplates> {
        val r = CreateUserRequest("usr", "pass")
        val model = RepresentationModelWithTemplates(mutableMapOf("default" to r.getTemplate()))
        model.add(linkTo(methodOn(UsersResourceController::class.java).createUser(r)).withSelfRel())
        return ResponseEntity.status(HttpStatus.OK).body(model)
    }

    @PostMapping
    fun createUser(@RequestBody request: CreateUserRequest): ResponseEntity<UserView> {
        val operation = request.toOperation()
        val savedUser = userLogic.createUser(operation)
        val model = modelAssembler.toModel(savedUser)
        return ResponseEntity.status(HttpStatus.CREATED).body(model)
    }

    @GetMapping("/{id}")
    fun getOneUser(@PathVariable id: String): ResponseEntity<UserView> {
        val user = userLogic.retrieveOneUser(RetrieveOneUserOperation(id))
        val model = modelAssembler.toModel(user)
        return ResponseEntity.status(HttpStatus.OK).body(model)
    }

    @GetMapping("/home")
    fun usersHome(): ResponseEntity<UserView> {
        return getOneUser(currentUser().id)
    }

    @GetMapping("/all")
    fun getAllUsers(
        requestPageable: Pageable,
        assembler: PagedResourcesAssembler<User>
    ): ResponseEntity<PagedModel<UserView>> {
        val op = RetrieveAllUsersOperation(requestPageable)
        val pageOfUsers = userLogic.retrievePageOfAllUsers(op)

        val linkBuilder = linkTo(methodOn(UsersResourceController::class.java)
            .getAllUsers(requestPageable, assembler))
            .toUriComponentsBuilder()

        val resource = buildLinksToPages(
            linkBuilder,
            assembler,
            modelAssembler,
            requestPageable,
            pageOfUsers
        )

        return ResponseEntity.status(HttpStatus.OK).body(resource)
    }

    private fun currentUser(): User = authenticationFacade.getCurrentUserEntity()
}
