package com.web.server.controllers.advices

import com.web.server.businesslogic.errors.AttemptToUpdateStaleStateException
import com.web.server.businesslogic.errors.CommentNotAllowedException
import com.web.server.businesslogic.errors.CommentNotFoundException
import com.web.server.businesslogic.errors.ConflictingUpdatesDetectedException
import com.web.server.businesslogic.errors.InvalidInputException
import com.web.server.businesslogic.errors.InvalidProjectSettingsException
import com.web.server.businesslogic.errors.IssueNotFoundException
import com.web.server.businesslogic.errors.NoSuchIssueStateException
import com.web.server.businesslogic.errors.NoSuchLabelException
import com.web.server.businesslogic.errors.NoSuchStateTransitionException
import com.web.server.businesslogic.errors.ProjectAlreadyExistsException
import com.web.server.businesslogic.errors.ProjectNotFoundException
import com.web.server.businesslogic.errors.UserAlreadyExistsException
import com.web.server.businesslogic.errors.UserNotFoundException
import com.web.server.controllers.controllererrors.RequiredHeaderNotFoundOrInvalidException
import org.springframework.data.mapping.PropertyReferenceException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException

@ControllerAdvice
class GlobalExceptionHandlerAdvice {
    private companion object {
        // Messages to be used when exceptions don't carry any.
        // these are normally not shown, and are made generic to serve as
        // "fail-over" messages for multiple error types.
        const val UNAUTHORIZED_DEFAULT_MESSAGE = "Bad or missing credentials."
        const val NOT_FOUND_DEFAULT_MESSAGE = "Requested resource was not found."
        const val ALREADY_EXISTS_DEFAULT_MESSAGE = "Resource already exists."
        const val NOT_READABLE_DEFAULT_MESSAGE = "Message not readable."
        const val INVALID_DATA_DEFAULT_MESSAGE = "Data supplied to fulfill the request is invalid."
        const val STALE_RESOURCE_MESSAGE = "Resource state representation provided is outdated."
        const val CONFLICT_DEFAULT_MESSAGE = "Conflicting requests detected."
        const val RESOURCE_IS_NOT_IN_CONDITIONS_MESSAGE = "Resource is not in conditions to fulfill request."
        const val UNEXPECTED_ERROR_MESSAGE = "Unexpected error. Please, contact Support Team."

        val ERROR_MEDIA_TYPE: MediaType = MediaType.APPLICATION_PROBLEM_JSON
    }

    @ExceptionHandler(RuntimeException::class)
    fun catchAllHandler(ex: RuntimeException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, ex, UNEXPECTED_ERROR_MESSAGE)
    }

    // TODO: this sends odd `full auth is required` message. Should be something better.
    @ExceptionHandler(AuthenticationException::class)
    fun catchAuthenticationException(ex: AuthenticationException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNAUTHORIZED, ex, UNAUTHORIZED_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun messageNotReadableHandler(ex: HttpMessageNotReadableException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, NOT_READABLE_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(UserNotFoundException::class)
    fun userNotFoundHandler(ex: UserNotFoundException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.NOT_FOUND, ex, NOT_FOUND_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(UserAlreadyExistsException::class)
    fun userAlreadyExistsHandler(ex: UserAlreadyExistsException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, ALREADY_EXISTS_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(ProjectNotFoundException::class)
    fun projectNotFoundHandler(ex: ProjectNotFoundException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.NOT_FOUND, ex, NOT_FOUND_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(ProjectAlreadyExistsException::class)
    fun projectAlreadyExistsHandler(ex: ProjectAlreadyExistsException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, ALREADY_EXISTS_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(InvalidProjectSettingsException::class)
    fun invalidProjectConfigurationHandler(ex: InvalidProjectSettingsException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(NoSuchIssueStateException::class)
    fun noSuchIssueStateHandler(ex: NoSuchIssueStateException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(NoSuchStateTransitionException::class)
    fun noSuchStateTransitionHandler(ex: NoSuchStateTransitionException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(NoSuchLabelException::class)
    fun noSuchLabelHandler(ex: NoSuchLabelException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(PropertyReferenceException::class)
    fun catchPropertyNotFoundHandler(ex: PropertyReferenceException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(RequiredHeaderNotFoundOrInvalidException::class)
    fun requiredHeaderNotFoundOrInvalidHandler(ex: RequiredHeaderNotFoundOrInvalidException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, NOT_READABLE_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(AttemptToUpdateStaleStateException::class)
    fun updateStaleStateHandler(ex: AttemptToUpdateStaleStateException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.PRECONDITION_FAILED, ex, STALE_RESOURCE_MESSAGE)
    }

    @ExceptionHandler(ConflictingUpdatesDetectedException::class)
    fun optimisticLockingFailureHandler(ex: ConflictingUpdatesDetectedException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.CONFLICT, ex, CONFLICT_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(IssueNotFoundException::class)
    fun issueNotFoundHandler(ex: IssueNotFoundException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.NOT_FOUND, ex, NOT_FOUND_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException::class)
    fun methodArgumentMismatchHandler(ex: MethodArgumentTypeMismatchException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, ex, NOT_READABLE_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(CommentNotFoundException::class)
    fun commentNotFoundHandler(ex: CommentNotFoundException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.NOT_FOUND, ex, NOT_FOUND_DEFAULT_MESSAGE)
    }

    @ExceptionHandler(CommentNotAllowedException::class)
    fun commentNotAllowedHandler(ex: CommentNotAllowedException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, RESOURCE_IS_NOT_IN_CONDITIONS_MESSAGE)
    }

    @ExceptionHandler(InvalidInputException::class)
    fun invalidInputHandler(ex: InvalidInputException): ResponseEntity<APIError> {
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex, INVALID_DATA_DEFAULT_MESSAGE)
    }

    private fun buildResponseEntity(
        httpStatus: HttpStatus,
        ex: RuntimeException,
        defaultMessage: String
    ): ResponseEntity<APIError> {
        return ResponseEntity.status(httpStatus)
            .contentType(ERROR_MEDIA_TYPE)
            .body(APIError(message = ex.message ?: defaultMessage))
    }
}
