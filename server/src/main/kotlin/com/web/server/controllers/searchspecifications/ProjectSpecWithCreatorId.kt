package com.web.server.controllers.searchspecifications

import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification

class ProjectSpecWithCreatorId(val creatorId: String?) : Specification<Project> {
    override fun toPredicate(
        root: Root<Project>,
        query: CriteriaQuery<*>,
        criteriaBuilder: CriteriaBuilder
    ): Predicate? {
        if (creatorId == null) {
            return criteriaBuilder.isTrue(criteriaBuilder.literal(true))
        }
        val creator = root.join<Project, User>("creator")
        return criteriaBuilder.equal(creator.get<User>("id"), this.creatorId)
    }
}
