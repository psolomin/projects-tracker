package com.web.server.controllers

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.constants.Uris.PROJECT_SETTINGS_ISSUE_LABELS_TEMPLATE_ID
import com.web.server.constants.Uris.PROJECT_SETTINGS_ISSUE_STATES_TEMPLATE_ID
import com.web.server.constants.Uris.PROJECT_SETTINGS_ISSUE_STATE_TRANSITIONS_TEMPLATE_ID
import com.web.server.constants.Uris.PROJECT_SETTINGS_TEMPLATE_ID
import com.web.server.constants.Uris.TEMPLATES_RESOURCE_URI
import com.web.server.controllers.requestforms.Template
import com.web.server.controllers.requests.ProjectSettingsRequest
import org.springframework.hateoas.CollectionModel
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(TEMPLATES_RESOURCE_URI)
class TemplatesResourceController {
    @GetMapping("/templates/{tplId}")
    fun projectSettingsTemplates(
        @PathVariable("tplId") tplId: String
    ): ResponseEntity<Any> {
        when (tplId) {
            PROJECT_SETTINGS_TEMPLATE_ID -> {
                val em = projectSettingsTemplateModel()
                return okResponse(em)
            }

            PROJECT_SETTINGS_ISSUE_STATES_TEMPLATE_ID -> {
                val em = issueStateSettingsTemplateModel()
                return okResponse(em)
            }

            PROJECT_SETTINGS_ISSUE_STATE_TRANSITIONS_TEMPLATE_ID -> {
                val em = issueStateTransitionSettingsTemplateModel()
                return okResponse(em)
            }

            PROJECT_SETTINGS_ISSUE_LABELS_TEMPLATE_ID -> {
                val em = issueLabelSettingTemplateModel()
                return okResponse(em)
            }

            else -> return notFoundResponse()
        }
    }

    private fun thisClass() = methodOn(TemplatesResourceController::class.java)

    private fun selfLink(templateId: String) = linkTo(thisClass().projectSettingsTemplates(templateId)).withSelfRel()

    private fun linkString(templateId: String) = linkTo(thisClass().projectSettingsTemplates(templateId)).toString()

    private fun <T> okResponse(em: T): ResponseEntity<Any> = ResponseEntity.status(HttpStatus.OK).body(em)

    private fun notFoundResponse(): ResponseEntity<Any> = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    private fun projectSettingsTemplateModel(): EntityModel<Template> {
        val tpl = ProjectSettingsRequest().getTemplate(
            linkString(PROJECT_SETTINGS_ISSUE_STATES_TEMPLATE_ID),
            linkString(PROJECT_SETTINGS_ISSUE_STATE_TRANSITIONS_TEMPLATE_ID),
            linkString(PROJECT_SETTINGS_ISSUE_LABELS_TEMPLATE_ID)
        )
        val selfLink = selfLink(PROJECT_SETTINGS_TEMPLATE_ID)
        return EntityModel(tpl, selfLink)
    }

    private fun issueStateSettingsTemplateModel(): CollectionModel<Template> {
        val tpl = IssueStateSetting("dummy", true).getTemplate()
        return collectionModel(PROJECT_SETTINGS_ISSUE_STATES_TEMPLATE_ID, tpl)
    }

    private fun issueStateTransitionSettingsTemplateModel(): CollectionModel<Template> {
        val tpl = IssueStateTransitionSetting("dummy", "dummy").getTemplate()
        return collectionModel(PROJECT_SETTINGS_ISSUE_STATE_TRANSITIONS_TEMPLATE_ID, tpl)
    }

    private fun issueLabelSettingTemplateModel(): CollectionModel<Template> {
        val tpl = IssueLabelSetting("label").getTemplate()
        return collectionModel(PROJECT_SETTINGS_ISSUE_LABELS_TEMPLATE_ID, tpl)
    }

    private fun collectionModel(tplId: String, tpl: Template): CollectionModel<Template> {
        return CollectionModel(setOf(tpl), selfLink(tplId))
    }
}
