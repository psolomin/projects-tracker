package com.web.server.businesslogic

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.whenever
import com.web.server.businesslogic.errors.UserAlreadyExistsException
import com.web.server.businesslogic.errors.UserNotFoundException
import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.businesslogic.operations.RetrieveOneUserByNameOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.SecurityConstants.PASSWORD_ENCODER
import com.web.server.repositories.UsersRepository
import com.web.server.repositories.entities.User
import java.util.Optional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.AdditionalAnswers
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.dao.DataIntegrityViolationException

@RunWith(MockitoJUnitRunner::class)
class UserLogicImplTest {
    private lateinit var userLogic: UserLogic
    private lateinit var userLogicBrokenIntegrity: UserLogic
    @Mock private lateinit var usersRepository: UsersRepository
    @Mock private lateinit var usersRepoBrokenIntegrity: UsersRepository
    @Mock private lateinit var requiredFieldsValidator: RequiredFieldsValidator

    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val NEW_USER_NAME = "John Doe 2"
        const val USER_PASSWORD = "pass"
        const val NEW_USER_PASSWORD = "pass 2"
        const val NON_EXISTING_USER_ID = "non-existing-user-id"
        val EXISTING_USER_ID = generateNewId()
        val EXISTING_USER = User(EXISTING_USER_ID, EXISTING_USER_NAME, USER_PASSWORD)
    }

    @Before
    fun prepareTest() {
        whenever(usersRepository.save(any<User>())).then(AdditionalAnswers.returnsLastArg<User>())
        whenever(usersRepository.findById(EXISTING_USER_ID)).doReturn(Optional.of(EXISTING_USER))
        whenever(usersRepository.findUserByUserName(EXISTING_USER_NAME)).doReturn(Optional.of(EXISTING_USER))
        whenever(usersRepository.findById(NON_EXISTING_USER_ID)).doReturn(Optional.empty())
        whenever(usersRepository.findUserByUserName(NEW_USER_NAME)).doReturn(Optional.empty())
        userLogic = UserLogicImpl(usersRepository, requiredFieldsValidator)

        whenever(usersRepoBrokenIntegrity.save(any<User>())).doThrow(DataIntegrityViolationException::class)
        userLogicBrokenIntegrity = UserLogicImpl(usersRepoBrokenIntegrity, requiredFieldsValidator)
    }

    @Test
    fun `create user operation returns created user with correct data`() {
        val createUserOp = CreateUserOperation(NEW_USER_NAME, NEW_USER_PASSWORD)
        val createdUser = userLogic.createUser(createUserOp)
        assertEquals(expected = NEW_USER_NAME, actual = createdUser.userName)
        assertTrue(PASSWORD_ENCODER.matches(NEW_USER_PASSWORD, createdUser.userPassword))
    }

    @Test
    fun `creating a user with existing name throws exception`() {
        val createUserOp = CreateUserOperation(EXISTING_USER_NAME, NEW_USER_PASSWORD)
        val exception = assertFailsWith<UserAlreadyExistsException> {
            userLogic.createUser(createUserOp)
        }
        assertEquals(
            expected = "User with name $EXISTING_USER_NAME already exists.",
            actual = exception.message
        )
    }

    @Test
    fun `data integrity violation exception means that user already exists`() {
        val createUserOp = CreateUserOperation(EXISTING_USER_NAME, USER_PASSWORD)
        val exception = assertFailsWith<UserAlreadyExistsException> {
            userLogicBrokenIntegrity.createUser(createUserOp)
        }
        assertEquals(
            expected = "User with name $EXISTING_USER_NAME already exists.",
            actual = exception.message
        )
    }

    @Test
    fun `retrieve one user operation with existing user ID returns a user`() {
        val retrieveUserOp = RetrieveOneUserOperation(EXISTING_USER_ID)
        val retrievedUser = userLogic.retrieveOneUser(retrieveUserOp)
        assertEquals(expected = EXISTING_USER, actual = retrievedUser)
    }

    @Test
    fun `retrieve one user operation with existing user name returns a user`() {
        val retrieveUserOp = RetrieveOneUserByNameOperation(EXISTING_USER_NAME)
        val retrievedUser = userLogic.retrieveOneUserByName(retrieveUserOp)
        assertEquals(expected = EXISTING_USER, actual = retrievedUser)
    }

    @Test
    fun `retrieving non-existing user throws exception`() {
        val retrieveUserOp = RetrieveOneUserOperation(NON_EXISTING_USER_ID)
        assertFailsWith<UserNotFoundException> {
            userLogic.retrieveOneUser(retrieveUserOp)
        }
    }

    @Test
    fun `retrieving non-existing user by name throws exception`() {
        val retrieveUserOp = RetrieveOneUserByNameOperation(NEW_USER_NAME)
        val exception = assertFailsWith<UserNotFoundException> {
            userLogic.retrieveOneUserByName(retrieveUserOp)
        }
        assertEquals(
            expected = "User with ID or NAME $NEW_USER_NAME does not exist.",
            actual = exception.message
        )
    }
}
