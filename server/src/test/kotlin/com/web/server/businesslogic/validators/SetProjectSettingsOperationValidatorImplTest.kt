package com.web.server.businesslogic.validators

import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.businesslogic.errors.InvalidProjectSettingsException
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_ALLOWED_SYMBOLS
import com.web.server.constants.UserInputConstraints.ISSUE_LABELS_MAX_COUNT
import com.web.server.constants.UserInputConstraints.ISSUE_STATES_MAX_COUNT
import com.web.server.constants.UserInputConstraints.ISSUE_STATE_TRANSITIONS_MAX_COUNT
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MAX_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MIN_LENGTH
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SetProjectSettingsOperationValidatorImplTest {
    private val validator = SetProjectSettingsOperationValidatorImpl()
    private val textSizeErrorMsg = "Text input must have at least $TEXT_INPUT_MIN_LENGTH " +
            "and at most $TEXT_INPUT_MAX_LENGTH symbols."

    @Test
    fun `valid project configuration passes validation`() {
        val op = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "closed", isFirst = true),
                IssueStateSetting(name = "archived", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "archived")
            )
        )
        assertTrue(validator.validateSettings(op))
    }

    @Test
    fun `default project settings must be valid`() {
        val op = SetProjectSettingsOperation()
        assertTrue(validator.validateSettings(op))
    }

    @Test
    fun `number of states can not exceed hard max`() {
        val states = List((ISSUE_STATES_MAX_COUNT + 1).toInt()) {
            IssueStateSetting(it.toString(), it == 0)
        }
        val op = SetProjectSettingsOperation(issueStateSettings = states)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        val expectedMsg = "Too many issue states. Max is $ISSUE_STATES_MAX_COUNT."
        assertEquals(expected = expectedMsg, actual = e.message)
    }

    @Test
    fun `size of state text can not exceed hard max`() {
        val st = "o".repeat((TEXT_INPUT_MAX_LENGTH + 1).toInt())
        val op = SetProjectSettingsOperation(issueStateSettings = listOf(IssueStateSetting(st, true)))
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `size of state transition text can not exceed hard max`() {
        val state = "o".repeat((TEXT_INPUT_MAX_LENGTH + 1).toInt())
        val setting = listOf(IssueStateTransitionSetting(state, state))
        val op = SetProjectSettingsOperation(issueStateTransitionSettings = setting)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `size of state text must be greater than hard min`() {
        val st = "o".repeat((TEXT_INPUT_MIN_LENGTH - 1).toInt())
        val op = SetProjectSettingsOperation(issueStateSettings = listOf(IssueStateSetting(st, true)))
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `size of state transition text must be greater than hard min`() {
        val state = "o".repeat((TEXT_INPUT_MIN_LENGTH - 1).toInt())
        val setting = listOf(IssueStateTransitionSetting(state, state))
        val op = SetProjectSettingsOperation(issueStateTransitionSettings = setting)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `number of transitions can not exceed hard max`() {
        val transitions = List((ISSUE_STATE_TRANSITIONS_MAX_COUNT + 1).toInt()) {
            IssueStateTransitionSetting(it.toString(), it.toString())
        }
        val op = SetProjectSettingsOperation(issueStateTransitionSettings = transitions)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        val expectedMsg = "Too many issue state transitions. Max is $ISSUE_STATE_TRANSITIONS_MAX_COUNT."
        assertEquals(expected = expectedMsg, actual = e.message)
    }

    @Test
    fun `number of labels can not exceed hard max`() {
        val lbs = List((ISSUE_LABELS_MAX_COUNT + 1).toInt()) { IssueLabelSetting(it.toString()) }
        val op = SetProjectSettingsOperation(issueLabelSettings = lbs)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        val expectedMsg = "Too many issue labels. Max is $ISSUE_LABELS_MAX_COUNT."
        assertEquals(expected = expectedMsg, actual = e.message)
    }

    @Test
    fun `labels must contain alphanumeric symbols only`() {
        val label = "\"+hsa"
        val op = SetProjectSettingsOperation(issueLabelSettings = listOf(IssueLabelSetting(label)))
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        val expectedMsg = "Only $ISSUE_LABELS_ALLOWED_SYMBOLS are allowed for labels."
        assertEquals(expected = expectedMsg, actual = e.message)
    }

    @Test
    fun `size of labels text can not exceed hard max`() {
        val label = "o".repeat((TEXT_INPUT_MAX_LENGTH + 1).toInt())
        val op = SetProjectSettingsOperation(issueLabelSettings = listOf(IssueLabelSetting(label)))
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `size of labels text must exceed hard min`() {
        val label = "o".repeat((TEXT_INPUT_MIN_LENGTH - 1).toInt())
        val op = SetProjectSettingsOperation(issueLabelSettings = listOf(IssueLabelSetting(label)))
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = textSizeErrorMsg, actual = e.message)
    }

    @Test
    fun `duplicated labels should not be accepted`() {
        val lbs = listOf(IssueLabelSetting("abc"), IssueLabelSetting("abc"))
        val op = SetProjectSettingsOperation(issueLabelSettings = lbs)
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(op)
        }
        assertEquals(expected = "Duplicated labels found.", actual = e.message)
    }

    @Test
    fun `there must be only one "first" state`() {
        val opNoFirst = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "closed", isFirst = false),
                IssueStateSetting(name = "archived", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "archived")
            )
        )
        val e1 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opNoFirst)
        }
        assertEquals(expected = "At least one state must be \"first\".", actual = e1.message)

        val opMultipleFirst = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "closed", isFirst = true),
                IssueStateSetting(name = "archived", isFirst = true)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "archived")
            )
        )
        val e2 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opMultipleFirst)
        }
        assertEquals(expected = "Only one state can be \"first\".", actual = e2.message)
    }

    @Test
    fun `states must have no duplicated names`() {
        val opDuplicatedStateNames = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "closed", isFirst = true),
                IssueStateSetting(name = "closed", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "closed")
            )
        )
        val e1 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opDuplicatedStateNames)
        }
        assertEquals(expected = "Duplicated state names found.", actual = e1.message)
    }

    @Test
    fun `transitions must not be duplicated`() {
        val opDuplicatedTransitions = SetProjectSettingsOperation(
            issueStateSettings = listOf(IssueStateSetting(name = "closed", isFirst = true)),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "closed"),
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "closed")
            )
        )
        val e1 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opDuplicatedTransitions)
        }
        assertEquals(expected = "Duplicated transitions found.", actual = e1.message)
    }

    @Test
    fun `there should be no transitions from or to states that don't exist`() {
        val opDuplicatedTransitions = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "non-existing")
            )
        )
        val e1 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opDuplicatedTransitions)
        }
        val expectedMessage = "Transition to non-existing state: no such vertex in graph: non-existing"
        assertEquals(expected = expectedMessage, actual = e1.message)
    }

    @Test
    fun `there should be no self-transitions`() {
        val opDuplicatedTransitions = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "open")
            )
        )
        val e1 = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opDuplicatedTransitions)
        }
        assertEquals(expected = "Transition to same state: \"open\".", actual = e1.message)
    }

    @Test
    fun `all states, except the first, must be reachable from the first`() {
        val opUnreachableStates = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true),
                IssueStateSetting(name = "closed", isFirst = false),
                IssueStateSetting(name = "archived", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "closed")
            )
        )
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opUnreachableStates)
        }
        assertEquals(expected = "Unreachable state: \"archived\"", actual = e.message)
    }

    @Test
    fun `all states must have connection to the default last state`() {
        val lastDefaultState = DEFAULT_LAST_ISSUE_STATE.name
        val opUnreachableStates = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true),
                IssueStateSetting(name = "closed", isFirst = false),
                IssueStateSetting(name = lastDefaultState, isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "closed"),
                IssueStateTransitionSetting(stateFrom = "open", stateTo = lastDefaultState)
            )
        )
        val e = assertFailsWith<InvalidProjectSettingsException> {
            validator.validateSettings(opUnreachableStates)
        }
        val expectedMessage = "No set of transitions from state: \"closed\" to state \"$lastDefaultState\"."
        assertEquals(expected = expectedMessage, actual = e.message)
    }
}
