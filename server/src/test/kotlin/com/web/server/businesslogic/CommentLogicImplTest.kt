package com.web.server.businesslogic

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.whenever
import com.web.server.businesslogic.errors.CommentNotAllowedException
import com.web.server.businesslogic.errors.CommentNotFoundException
import com.web.server.businesslogic.errors.IssueNotFoundException
import com.web.server.businesslogic.errors.UserNotFoundException
import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneCommentOperation
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.constants.DefaultProjectSettings.DEFAULT_LAST_ISSUE_STATE
import com.web.server.repositories.CommentsRepository
import com.web.server.repositories.entities.Comment
import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import java.util.Optional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.AdditionalAnswers
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CommentLogicImplTest {
    private lateinit var commentLogic: CommentLogic
    @Mock private lateinit var userLogic: UserLogic
    @Mock private lateinit var issueLogic: IssueLogic
    @Mock private lateinit var commentsRepository: CommentsRepository
    @Mock private lateinit var requiredFieldsValidator: RequiredFieldsValidator

    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val USER_PASSWORD = "pass"
        const val NON_EXISTING_USER_ID = "non-existing-user-id"
        val EXISTING_USER_ID = generateNewId()
        val EXISTING_USER = User(EXISTING_USER_ID, EXISTING_USER_NAME, USER_PASSWORD)

        val EXISTING_PROJECT_ID = generateNewId()
        const val EXISTING_PROJECT_NAME = "existing project"
        const val EXISTING_PROJECT_DESCRIPTION = "existing project desc"
        val EXISTING_PROJECT = Project(
            id = EXISTING_PROJECT_ID,
            projectName = EXISTING_PROJECT_NAME,
            description = EXISTING_PROJECT_DESCRIPTION,
            creator = EXISTING_USER
        )

        val FIRST_ISSUE_STATE = IssueState(
            stateName = "one",
            isFirst = true,
            parentProject = EXISTING_PROJECT
        )

        val EXISTING_ISSUE_ID = generateNewId()
        val EXISTING_ISSUE = Issue(
            id = EXISTING_ISSUE_ID,
            issueName = "add test",
            description = "add test for feature X",
            createdBy = EXISTING_USER,
            parentProject = EXISTING_PROJECT,
            state = FIRST_ISSUE_STATE
        )

        val LAST_ISSUE_STATE = IssueState(
            stateName = DEFAULT_LAST_ISSUE_STATE.name,
            isFirst = DEFAULT_LAST_ISSUE_STATE.isFirst,
            parentProject = EXISTING_PROJECT
        )
        val EXISTING_ISSUE_IN_LAST_STATE_ID = generateNewId()
        val EXISTING_ISSUE_IN_LAST_STATE = Issue(
            id = EXISTING_ISSUE_IN_LAST_STATE_ID,
            issueName = "add test",
            description = "add test for feature X",
            createdBy = EXISTING_USER,
            parentProject = EXISTING_PROJECT,
            state = LAST_ISSUE_STATE
        )

        val EXISTING_COMMENT_ID = generateNewId()
        val EXISTING_COMMENT = Comment(
            commentBody = "comment",
            creator = EXISTING_USER,
            parentIssue = EXISTING_ISSUE
        )

        val NON_EXISTING_ISSUE_ID = generateNewId()
        val NON_EXISTING_COMMENT_ID = generateNewId()
    }

    @Before
    fun prepareTest() {
        whenever(userLogic.retrieveOneUser(RetrieveOneUserOperation(EXISTING_USER_ID)))
            .doReturn(EXISTING_USER)
        whenever(userLogic.retrieveOneUser(RetrieveOneUserOperation(NON_EXISTING_USER_ID)))
            .doThrow(UserNotFoundException(NON_EXISTING_USER_ID))

        whenever(issueLogic.retrieveOneIssue(RetrieveOneIssueOperation(EXISTING_ISSUE.id)))
            .doReturn(EXISTING_ISSUE)
        whenever(issueLogic.retrieveOneIssue(RetrieveOneIssueOperation(EXISTING_ISSUE_IN_LAST_STATE_ID)))
            .doReturn(EXISTING_ISSUE_IN_LAST_STATE)
        whenever(issueLogic.retrieveOneIssue(RetrieveOneIssueOperation(NON_EXISTING_ISSUE_ID)))
            .doThrow(IssueNotFoundException(NON_EXISTING_ISSUE_ID))

        whenever(commentsRepository.save(any<Comment>())).then(AdditionalAnswers.returnsLastArg<Comment>())
        whenever(commentsRepository.findById(EXISTING_COMMENT_ID)).doReturn(Optional.of(EXISTING_COMMENT))

        commentLogic = CommentLogicImpl(userLogic, issueLogic, commentsRepository, requiredFieldsValidator)
    }

    @Test
    fun `comment can be created for a given issue by a given user`() {
        val op = CreateCommentOperation(
            commentBody = "comment",
            creatorId = EXISTING_USER_ID,
            parentIssueId = EXISTING_ISSUE.id
        )
        val commentCreated = commentLogic.createComment(op)
        assertEquals(expected = EXISTING_USER, actual = commentCreated.creator)
        assertEquals(expected = EXISTING_ISSUE, actual = commentCreated.parentIssue)
        assertEquals(expected = "comment", actual = commentCreated.commentBody)
    }

    @Test
    fun `existing comment can be retrieved`() {
        val op = RetrieveOneCommentOperation(EXISTING_COMMENT_ID)
        val commentRetrieved = commentLogic.retrieveOneComment(op)
        assertEquals(expected = EXISTING_COMMENT.commentBody, actual = commentRetrieved.commentBody)
        assertEquals(expected = EXISTING_USER, actual = commentRetrieved.creator)
        assertEquals(expected = EXISTING_ISSUE, actual = commentRetrieved.parentIssue)
    }

    @Test
    fun `requesting non-existent comment throws exception`() {
        val op = RetrieveOneCommentOperation(NON_EXISTING_COMMENT_ID)
        val e = assertFailsWith<CommentNotFoundException> {
            commentLogic.retrieveOneComment(op)
        }
        assertEquals(expected = "Comment ID = $NON_EXISTING_COMMENT_ID does not exist.", actual = e.message)
    }

    @Test
    fun `attempt to create comment by a non-existing user throws exception`() {
        val op = CreateCommentOperation(
            commentBody = "comment",
            creatorId = NON_EXISTING_USER_ID,
            parentIssueId = EXISTING_ISSUE.id
        )
        val e = assertFailsWith<UserNotFoundException> {
            commentLogic.createComment(op)
        }
        assertEquals(expected = "User with ID or NAME $NON_EXISTING_USER_ID does not exist.", actual = e.message)
    }

    @Test
    fun `attempt to create comment for non-existing issue throws exception`() {
        val op = CreateCommentOperation(
            commentBody = "comment",
            creatorId = EXISTING_USER_ID,
            parentIssueId = NON_EXISTING_ISSUE_ID
        )
        val e = assertFailsWith<IssueNotFoundException> {
            commentLogic.createComment(op)
        }
        assertEquals(expected = "Issue ID = $NON_EXISTING_ISSUE_ID does not exist.", actual = e.message)
    }

    @Test
    fun `attempt to create comment for issue which is in the obligatory last state throws exception`() {
        val op = CreateCommentOperation(
            commentBody = "comment",
            creatorId = EXISTING_USER_ID,
            parentIssueId = EXISTING_ISSUE_IN_LAST_STATE.id
        )
        val e = assertFailsWith<CommentNotAllowedException> {
            commentLogic.createComment(op)
        }
        assertEquals(
            expected = "Comments are not allowed for issues with state == " +
                EXISTING_ISSUE_IN_LAST_STATE.state.stateName + ".",
            actual = e.message
        )
    }
}
