package com.web.server.businesslogic

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.whenever
import com.web.server.businesslogic.errors.AttemptToUpdateStaleStateException
import com.web.server.businesslogic.errors.ConflictingUpdatesDetectedException
import com.web.server.businesslogic.errors.IssueNotFoundException
import com.web.server.businesslogic.errors.NoSuchLabelException
import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneIssueOperation
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.operations.UpdateIssueStateOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.constants.DefaultFieldsValues.DEFAULT_VERSION
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.repositories.IssuesRepository
import com.web.server.repositories.entities.Issue
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import java.util.Optional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.AdditionalAnswers
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.orm.ObjectOptimisticLockingFailureException

@RunWith(MockitoJUnitRunner::class)
class IssueLogicImplTest {
    private lateinit var issueLogic: IssueLogic
    @Mock private lateinit var projectLogic: ProjectLogic
    @Mock private lateinit var userLogic: UserLogic
    @Mock private lateinit var issuesRepository: IssuesRepository
    @Mock private lateinit var projectSettingsLogic: ProjectSettingsLogic
    @Mock private lateinit var requiredFieldsValidator: RequiredFieldsValidator

    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val USER_PASSWORD = "pass"
        val EXISTING_USER_ID = generateNewId()
        val EXISTING_USER = User(EXISTING_USER_ID, EXISTING_USER_NAME, USER_PASSWORD)

        val EXISTING_PROJECT_ID = generateNewId()
        const val EXISTING_PROJECT_NAME = "existing project"
        const val EXISTING_PROJECT_DESCRIPTION = "existing project desc"
        val EXISTING_PROJECT = Project(
            id = EXISTING_PROJECT_ID,
            projectName = EXISTING_PROJECT_NAME,
            description = EXISTING_PROJECT_DESCRIPTION,
            creator = EXISTING_USER
        )
        val FIRST_ISSUE_STATE = IssueState(
            stateName = "one",
            isFirst = true,
            parentProject = EXISTING_PROJECT
        )
        val EXISTING_ISSUE_ID = generateNewId()
        val EXISTING_ISSUE = Issue(
            id = EXISTING_ISSUE_ID,
            issueName = "add test",
            description = "add test for feature X",
            createdBy = EXISTING_USER,
            parentProject = EXISTING_PROJECT,
            state = FIRST_ISSUE_STATE
        )
        val EXISTING_LABEL = IssueLabel(label = "feature", parentProject = EXISTING_PROJECT)
        val NON_EXISTING_ISSUE_ID = generateNewId()
    }

    @Before
    fun prepareTest() {
        whenever(userLogic.retrieveOneUser(RetrieveOneUserOperation(EXISTING_USER_ID)))
            .doReturn(EXISTING_USER)
        whenever(projectLogic.retrieveOneProject(RetrieveOneProjectOperation(EXISTING_PROJECT_ID)))
            .doReturn(EXISTING_PROJECT)
        whenever(projectSettingsLogic.findFirstIssueStateInProjectConfigs(EXISTING_PROJECT_ID))
            .doReturn(FIRST_ISSUE_STATE)
        whenever(projectSettingsLogic.findTransitionByProjectAndStatesNames(
            EXISTING_PROJECT_ID, FIRST_ISSUE_STATE.stateName, "new-state"))
            .doReturn(
                IssueStateTransition(
                    fromState = FIRST_ISSUE_STATE,
                    toState = IssueState(stateName = "new-state", isFirst = false, parentProject = EXISTING_PROJECT),
                    parentProject = EXISTING_PROJECT
                )
            )
        whenever(projectSettingsLogic.findTransitionByProjectAndStatesNames(
            EXISTING_PROJECT_ID, EXISTING_ISSUE.state.stateName, "new-state"))
            .doReturn(
                IssueStateTransition(
                    fromState = EXISTING_ISSUE.state,
                    toState = IssueState(stateName = "new-state", isFirst = false, parentProject = EXISTING_PROJECT),
                    parentProject = EXISTING_PROJECT
                )
            )
        whenever(issuesRepository.save(any<Issue>())).then(AdditionalAnswers.returnsLastArg<Project>())
        whenever(issuesRepository.findById(EXISTING_ISSUE_ID)).doReturn(Optional.of(EXISTING_ISSUE))

        issueLogic = IssueLogicImpl(
            userLogic, projectLogic, projectSettingsLogic, issuesRepository, requiredFieldsValidator
        )
    }

    @Test
    fun `issue can be created for a given project by a given user`() {
        val op = CreateIssueOperation(
            issueName = "test",
            description = "desc",
            creatorId = EXISTING_USER_ID,
            parentProjectId = EXISTING_PROJECT_ID,
            labels = listOf()
        )
        val issueCreated = issueLogic.createIssue(op)
        assertEquals(expected = EXISTING_USER, actual = issueCreated.createdBy)
        assertEquals(expected = EXISTING_PROJECT, actual = issueCreated.parentProject)
        assertEquals(expected = FIRST_ISSUE_STATE, actual = issueCreated.state)
        assertEquals(expected = "test", actual = issueCreated.issueName)
        assertEquals(expected = "desc", actual = issueCreated.description)
    }

    @Test
    fun `issue can be created with allowed labels`() {
        whenever(projectSettingsLogic.findMatchingLabels(
            EXISTING_PROJECT_ID, listOf(EXISTING_LABEL.label)))
            .doReturn(sortedSetOf(EXISTING_LABEL))

        val op = CreateIssueOperation(
            issueName = "test",
            description = "desc",
            creatorId = EXISTING_USER_ID,
            parentProjectId = EXISTING_PROJECT_ID,
            labels = listOf(EXISTING_LABEL.label)
        )
        val issueCreated = issueLogic.createIssue(op)
        assertEquals(expected = EXISTING_USER, actual = issueCreated.createdBy)
        assertEquals(expected = EXISTING_PROJECT, actual = issueCreated.parentProject)
        assertEquals(expected = FIRST_ISSUE_STATE, actual = issueCreated.state)
        assertEquals(expected = "test", actual = issueCreated.issueName)
        assertEquals(expected = "desc", actual = issueCreated.description)
        assertEquals(expected = setOf(EXISTING_LABEL), actual = issueCreated.issueLabels)
    }

    @Test
    fun `issue can not be created with labels which do not presen in project settings`() {
        val nonExistingLabelText = "non-existing-label"
        whenever(projectSettingsLogic.findMatchingLabels(
            EXISTING_PROJECT_ID, listOf(EXISTING_LABEL.label, nonExistingLabelText)))
            .doThrow(NoSuchLabelException(EXISTING_PROJECT_ID, listOf(nonExistingLabelText)))

        val op = CreateIssueOperation(
            issueName = "test",
            description = "desc",
            creatorId = EXISTING_USER_ID,
            parentProjectId = EXISTING_PROJECT_ID,
            labels = listOf(EXISTING_LABEL.label, nonExistingLabelText)
        )
        val e = assertFailsWith<NoSuchLabelException> {
            issueLogic.createIssue(op)
        }
        val expectedMsg = "Labels [$nonExistingLabelText] are not defiled for project ID = $EXISTING_PROJECT_ID."
        assertEquals(expected = expectedMsg, actual = e.message)
    }

    @Test
    fun `existing issue can be retrieved`() {
        val op = RetrieveOneIssueOperation(EXISTING_ISSUE_ID)
        val issueRetrieved = issueLogic.retrieveOneIssue(op)
        assertEquals(expected = EXISTING_USER, actual = issueRetrieved.createdBy)
        assertEquals(expected = EXISTING_PROJECT, actual = issueRetrieved.parentProject)
        assertEquals(expected = FIRST_ISSUE_STATE, actual = issueRetrieved.state)
        assertEquals(expected = EXISTING_ISSUE.issueName, actual = issueRetrieved.issueName)
        assertEquals(expected = EXISTING_ISSUE.description, actual = issueRetrieved.description)
    }

    @Test
    fun `requesting non-existent issue throws exception`() {
        val op = RetrieveOneIssueOperation(NON_EXISTING_ISSUE_ID)
        assertFailsWith<IssueNotFoundException> {
            issueLogic.retrieveOneIssue(op)
        }
    }

    @Test
    fun `issue status update operation produces updated issue`() {
        val op = UpdateIssueStateOperation(
            EXISTING_ISSUE.id, "new-state", EXISTING_USER_ID, DEFAULT_VERSION
        )
        val issueUpdated = issueLogic.updateState(op)
        val expectedIssue = EXISTING_ISSUE.copy(
            state = IssueState(stateName = "new-state", isFirst = false, parentProject = EXISTING_PROJECT)
        )
        assertEquals(expected = expectedIssue, actual = issueUpdated)
    }

    @Test
    fun `issue status update operation which assumes stale issue version trows exception`() {
        val op = UpdateIssueStateOperation(
            EXISTING_ISSUE.id, "new-state", EXISTING_USER_ID, DEFAULT_VERSION - 1
        )
        val e = assertFailsWith<AttemptToUpdateStaleStateException> {
            issueLogic.updateState(op)
        }
        assertEquals(expected = "Issue state to be updated is out of date.", actual = e.message)
    }

    @Test
    fun `when datastore detects a conflict, correct error is thrown`() {
        whenever(issuesRepository.save(any<Issue>())).doThrow(ObjectOptimisticLockingFailureException::class)
        val op = UpdateIssueStateOperation(
            EXISTING_ISSUE.id, "new-state", EXISTING_USER_ID, DEFAULT_VERSION
        )
        assertFailsWith<ConflictingUpdatesDetectedException> {
            issueLogic.updateState(op)
        }
    }

    @Test
    fun `issue can be created for a given project by a given user with allowed labels`() {

        val op = CreateIssueOperation(
            issueName = "test",
            description = "desc",
            creatorId = EXISTING_USER_ID,
            parentProjectId = EXISTING_PROJECT_ID,
            labels = listOf()
        )
        val issueCreated = issueLogic.createIssue(op)
        assertEquals(expected = EXISTING_USER, actual = issueCreated.createdBy)
        assertEquals(expected = EXISTING_PROJECT, actual = issueCreated.parentProject)
        assertEquals(expected = FIRST_ISSUE_STATE, actual = issueCreated.state)
        assertEquals(expected = "test", actual = issueCreated.issueName)
        assertEquals(expected = "desc", actual = issueCreated.description)
    }

    @Test
    fun `issue can not be created with arbitrary labels`() {
        whenever(issuesRepository.save(any<Issue>())).doThrow(ObjectOptimisticLockingFailureException::class)
        val op = UpdateIssueStateOperation(
            EXISTING_ISSUE.id, "new-state", EXISTING_USER_ID, DEFAULT_VERSION
        )
        assertFailsWith<ConflictingUpdatesDetectedException> {
            issueLogic.updateState(op)
        }
    }
}
