package com.web.server.businesslogic

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.whenever
import com.web.server.businesslogic.errors.ProjectAlreadyExistsException
import com.web.server.businesslogic.errors.ProjectNotFoundException
import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneProjectOperation
import com.web.server.businesslogic.operations.RetrieveOneUserOperation
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.businesslogic.validators.RequiredFieldsValidator
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.repositories.ProjectsRepository
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import java.util.Optional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.AdditionalAnswers
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.dao.DataIntegrityViolationException

@RunWith(MockitoJUnitRunner::class)
class ProjectLogicImplTest {
    private lateinit var projectLogic: ProjectLogic
    @Mock private lateinit var projectRepo: ProjectsRepository
    @Mock private lateinit var userLogic: UserLogic
    @Mock private lateinit var projectSettingsLogic: ProjectSettingsLogic
    @Mock private lateinit var requiredFieldsValidator: RequiredFieldsValidator

    private lateinit var projectLogicBrokenIntegrity: ProjectLogic
    @Mock private lateinit var projectsRepoBrokenIntegrity: ProjectsRepository

    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val USER_PASSWORD = "pass"
        val EXISTING_USER_ID = generateNewId()
        val EXISTING_USER = User(EXISTING_USER_ID, EXISTING_USER_NAME, USER_PASSWORD)

        val NEW_PROJECT_ID = generateNewId()
        const val NEW_PROJECT_NAME = "web"
        const val NEW_PROJECT_DESCRIPTION = "desc"
        val NEW_PROJECT = Project(
            id = NEW_PROJECT_ID,
            projectName = NEW_PROJECT_NAME,
            description = NEW_PROJECT_DESCRIPTION,
            creator = EXISTING_USER
        )
        val DEFAULT_PROJECT_SETTINGS_OPERATION = SetProjectSettingsOperation()
        val EXISTING_PROJECT_ID = generateNewId()
        const val EXISTING_PROJECT_NAME = "existing proj"
        const val EXISTING_PROJECT_DESCRIPTION = "existing proj desc"
        val EXISTING_PROJECT = Project(
            id = EXISTING_PROJECT_ID,
            projectName = EXISTING_PROJECT_NAME,
            description = EXISTING_PROJECT_DESCRIPTION,
            creator = EXISTING_USER
        )

        const val NON_EXISTING_PROJECT_ID = "non-existing-project-id"
    }

    @Before
    fun prepareTest() {
        whenever(projectRepo.save(any<Project>())).then(AdditionalAnswers.returnsLastArg<Project>())
        whenever(projectRepo.findById(NEW_PROJECT_ID)).doReturn(Optional.of(NEW_PROJECT))
        whenever(projectRepo.findProjectByProjectName(EXISTING_PROJECT_NAME)).doReturn(Optional.of(EXISTING_PROJECT))
        whenever(projectRepo.findById(NON_EXISTING_PROJECT_ID)).doReturn(Optional.empty())
        whenever(userLogic.retrieveOneUser(RetrieveOneUserOperation(EXISTING_USER_ID))).doReturn(EXISTING_USER)

        projectLogic = ProjectLogicImpl(userLogic, projectSettingsLogic, projectRepo, requiredFieldsValidator)

        whenever(projectsRepoBrokenIntegrity.save(any<Project>())).doThrow(DataIntegrityViolationException::class)
        projectLogicBrokenIntegrity = ProjectLogicImpl(
            userLogic, projectSettingsLogic, projectsRepoBrokenIntegrity, requiredFieldsValidator
        )
    }

    @Test
    fun `create project operation returns created project with correct data`() {
        val createProjectOp = CreateProjectOperation(
            projectName = NEW_PROJECT_NAME,
            projectDescription = NEW_PROJECT_DESCRIPTION,
            creatorId = EXISTING_USER_ID
        )
        val createdProject = projectLogic.createProject(createProjectOp, DEFAULT_PROJECT_SETTINGS_OPERATION)
        assertEquals(expected = NEW_PROJECT_NAME, actual = createdProject.projectName)
        assertEquals(expected = EXISTING_USER, actual = createdProject.creator)
    }

    @Test
    fun `project can be retrieved`() {
        val retrievedProject = projectLogic.retrieveOneProject(RetrieveOneProjectOperation(NEW_PROJECT_ID))
        assertEquals(expected = NEW_PROJECT, actual = retrievedProject)
    }

    @Test
    fun `attempt to get non-existing project throws exception`() {
        val exception = assertFailsWith<ProjectNotFoundException> {
            projectLogic.retrieveOneProject(RetrieveOneProjectOperation(NON_EXISTING_PROJECT_ID))
        }
        assertEquals(
            expected = "Project ID or NAME $NON_EXISTING_PROJECT_ID does not exist.",
            actual = exception.message
        )
    }

    @Test
    fun `creating a project with existing name throws exception`() {
        val createProjectOp = CreateProjectOperation(
            projectName = EXISTING_PROJECT_NAME,
            projectDescription = NEW_PROJECT_DESCRIPTION,
            creatorId = EXISTING_USER_ID
        )
        val exception = assertFailsWith<ProjectAlreadyExistsException> {
            projectLogic.createProject(createProjectOp, DEFAULT_PROJECT_SETTINGS_OPERATION)
        }
        assertEquals(
            expected = "Project with name $EXISTING_PROJECT_NAME already exists.",
            actual = exception.message
        )
    }

    @Test
    fun `data integrity violation exception means that user already exists`() {
        val createProjectOp = CreateProjectOperation(
            projectName = EXISTING_PROJECT_NAME,
            projectDescription = NEW_PROJECT_DESCRIPTION,
            creatorId = EXISTING_USER_ID
        )
        val exception = assertFailsWith<ProjectAlreadyExistsException> {
            projectLogicBrokenIntegrity.createProject(createProjectOp, DEFAULT_PROJECT_SETTINGS_OPERATION)
        }
        assertEquals(
            expected = "Project with name $EXISTING_PROJECT_NAME already exists.",
            actual = exception.message
        )
    }
}
