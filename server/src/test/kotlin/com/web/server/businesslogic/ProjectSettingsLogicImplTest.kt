package com.web.server.businesslogic

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.web.server.businesslogic.dataclasses.IssueLabelSetting
import com.web.server.businesslogic.dataclasses.IssueStateSetting
import com.web.server.businesslogic.dataclasses.IssueStateTransitionSetting
import com.web.server.businesslogic.errors.NoSuchIssueStateException
import com.web.server.businesslogic.errors.NoSuchLabelException
import com.web.server.businesslogic.errors.NoSuchStateTransitionException
import com.web.server.businesslogic.operations.SetProjectSettingsOperation
import com.web.server.businesslogic.validators.SetProjectSettingsOperationValidator
import com.web.server.constants.DefaultFieldsValues.generateNewId
import com.web.server.repositories.IssueLabelsRepository
import com.web.server.repositories.IssueStateTransitionsRepository
import com.web.server.repositories.IssueStatesRepository
import com.web.server.repositories.entities.IssueLabel
import com.web.server.repositories.entities.IssueState
import com.web.server.repositories.entities.IssueStateTransition
import com.web.server.repositories.entities.Project
import com.web.server.repositories.entities.User
import java.util.Optional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ProjectSettingsLogicImplTest {
    private lateinit var projectSettingsLogic: ProjectSettingsLogic
    @Mock private lateinit var issueStatesRepository: IssueStatesRepository
    @Mock private lateinit var issueStateTransitionsRepository: IssueStateTransitionsRepository
    @Mock private lateinit var issueLabelsRepository: IssueLabelsRepository
    @Mock private lateinit var validator: SetProjectSettingsOperationValidator

    private companion object {
        const val EXISTING_USER_NAME = "John Doe"
        const val USER_PASSWORD = "pass"
        val EXISTING_USER_ID = generateNewId()
        val EXISTING_USER = User(EXISTING_USER_ID, EXISTING_USER_NAME, USER_PASSWORD)

        val EXISTING_PROJECT_ID = generateNewId()
        const val EXISTING_PROJECT_NAME = "existing project"
        const val EXISTING_PROJECT_DESCRIPTION = "existing project desc"
        val EXISTING_PROJECT = Project(
            id = EXISTING_PROJECT_ID,
            projectName = EXISTING_PROJECT_NAME,
            description = EXISTING_PROJECT_DESCRIPTION,
            creator = EXISTING_USER
        )
        val EXISTING_LABEL_ONE = IssueLabel(label = "feature", parentProject = EXISTING_PROJECT)
        val EXISTING_LABEL_TWO = IssueLabel(label = "bug", parentProject = EXISTING_PROJECT)

        val obligatoryStateOne = IssueState(
            stateName = "closed", isFirst = true, parentProject = EXISTING_PROJECT
        )
        val obligatoryStateTwo = IssueState(
            stateName = "archived", isFirst = false, parentProject = EXISTING_PROJECT
        )
        val obligatoryTransition = IssueStateTransition(
            fromState = obligatoryStateOne, toState = obligatoryStateTwo, parentProject = EXISTING_PROJECT
        )

        val existingProjectLabels = setOf(EXISTING_LABEL_ONE, EXISTING_LABEL_TWO)
    }

    @Before
    fun prepareTest() {
        whenever(issueStatesRepository.saveAll(any<List<IssueState>>())).doAnswer {
            @Suppress("UNCHECKED_CAST") // fine for tests to ignore potential issues with this
            it.arguments[0] as List<IssueState>
        }

        whenever(issueLabelsRepository.saveAll(any<List<IssueLabel>>())).doAnswer {
            @Suppress("UNCHECKED_CAST") // fine for tests to ignore potential issues with this
            it.arguments[0] as List<IssueLabel>
        }

        whenever(issueStatesRepository.findByParentProjectIdAndByName(
            EXISTING_PROJECT_ID, obligatoryStateOne.stateName
        )).doReturn(Optional.of(obligatoryStateOne))

        whenever(issueStatesRepository.findByParentProjectIdAndByName(
            EXISTING_PROJECT_ID, obligatoryStateTwo.stateName
        )).doReturn(Optional.of(obligatoryStateTwo))

        whenever(issueStateTransitionsRepository.findByParentProjectIdAndStateNames(
            EXISTING_PROJECT_ID, obligatoryStateOne.stateName, obligatoryStateTwo.stateName
        )).doReturn(Optional.of(obligatoryTransition))

        whenever(issueLabelsRepository.findByParentProjectIdAndByLabels(
            EXISTING_PROJECT_ID, listOf("feature", "bug")
        )).doReturn(existingProjectLabels.toList())

        whenever(issueLabelsRepository.findByParentProjectIdAndByLabels(
            EXISTING_PROJECT_ID, listOf("bug")
        )).doReturn(listOf(IssueLabel(label = "bug", parentProject = EXISTING_PROJECT)))

        whenever(issueLabelsRepository.findByParentProjectIdAndByLabels(
            EXISTING_PROJECT_ID, listOf("non-existing")
        )).doReturn(emptyList<IssueLabel>())

        projectSettingsLogic = ProjectSettingsLogicImpl(
            validator,
            issueStatesRepository,
            issueStateTransitionsRepository,
            issueLabelsRepository
        )
    }

    @Test
    fun `valid project configuration passed by user can be successfully saved along with obligatory configuration`() {
        val userSettingsOp = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true),
                IssueStateSetting(name = "analysis", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "closed")
            ),
            issueLabelSettings = listOf(
                IssueLabelSetting("label a"),
                IssueLabelSetting("label b")
            )
        )
        // expected to be saved: user-defined
        val stateThree = IssueState(stateName = "open", isFirst = true, parentProject = EXISTING_PROJECT)
        val stateFour = IssueState(stateName = "analysis", isFirst = false, parentProject = EXISTING_PROJECT)

        projectSettingsLogic.setProjectSettings(EXISTING_PROJECT, userSettingsOp)

        // repos should get merged obligatory configs and user-provided configs.
        // the isFirst in obligatory states should get overwritten:
        val updatedObligatoryStateOne = obligatoryStateOne.copy(isFirst = false)
        verify(issueStatesRepository, times(1)).saveAll(
            listOf(updatedObligatoryStateOne, obligatoryStateTwo, stateThree, stateFour)
        )
        verify(issueStateTransitionsRepository, times(1)).saveAll(
            listOf(
                IssueStateTransition(
                    fromState = updatedObligatoryStateOne,
                    toState = obligatoryStateTwo,
                    parentProject = EXISTING_PROJECT
                ),
                IssueStateTransition(
                    fromState = stateThree,
                    toState = updatedObligatoryStateOne,
                    parentProject = EXISTING_PROJECT
                )
            )
        )
        verify(issueLabelsRepository, times(1)).saveAll(
            listOf(
                IssueLabel(label = "label a", parentProject = EXISTING_PROJECT),
                IssueLabel(label = "label b", parentProject = EXISTING_PROJECT)
            )
        )
    }

    @Test
    fun `when user specifies unnecessary obligatory configuration, it is ignored and only obligatory is saved`() {
        val userSettingsOp = SetProjectSettingsOperation()
        projectSettingsLogic.setProjectSettings(EXISTING_PROJECT, userSettingsOp)
        verify(issueStatesRepository, times(1)).saveAll(listOf(obligatoryStateOne, obligatoryStateTwo))
        verify(issueStateTransitionsRepository, times(1)).saveAll(listOf(obligatoryTransition))
    }

    @Test
    fun `when user specifies obligatory configurations with non-default isFirst parameter, all settings are saved`() {
        val userSettingsOp = SetProjectSettingsOperation(
            issueStateSettings = listOf(
                IssueStateSetting(name = "open", isFirst = true),
                IssueStateSetting(name = "closed", isFirst = false),
                IssueStateSetting(name = "archived", isFirst = false)
            ),
            issueStateTransitionSettings = listOf(
                IssueStateTransitionSetting(stateFrom = "open", stateTo = "closed"),
                IssueStateTransitionSetting(stateFrom = "closed", stateTo = "archived")
            ),
            issueLabelSettings = listOf(
                IssueLabelSetting("label a"),
                IssueLabelSetting("label b")
            )
        )
        // expected to be saved
        val expectedIssueStates = listOf(
            IssueState(stateName = "archived", isFirst = false, parentProject = EXISTING_PROJECT),
            IssueState(stateName = "open", isFirst = true, parentProject = EXISTING_PROJECT),
            IssueState(stateName = "closed", isFirst = false, parentProject = EXISTING_PROJECT)
        )

        val expectedIssueStatesTransitions = listOf(
            IssueStateTransition(
                fromState = expectedIssueStates[2], toState = expectedIssueStates[0], parentProject = EXISTING_PROJECT),
            IssueStateTransition(
                fromState = expectedIssueStates[1], toState = expectedIssueStates[2], parentProject = EXISTING_PROJECT)
        )

        projectSettingsLogic.setProjectSettings(EXISTING_PROJECT, userSettingsOp)

        verify(issueStatesRepository, times(1)).saveAll(expectedIssueStates)
        verify(issueStateTransitionsRepository, times(1)).saveAll(expectedIssueStatesTransitions)
        verify(issueLabelsRepository, times(1)).saveAll(
            listOf(
                IssueLabel(label = "label a", parentProject = EXISTING_PROJECT),
                IssueLabel(label = "label b", parentProject = EXISTING_PROJECT)
            )
        )
    }

    @Test
    fun `when user does not specify configuration, obligatory configuration is saved`() {
        projectSettingsLogic.setProjectSettings(EXISTING_PROJECT, null)
        verify(issueStatesRepository, times(1)).saveAll(listOf(obligatoryStateOne, obligatoryStateTwo))
        verify(issueStateTransitionsRepository, times(1)).saveAll(listOf(obligatoryTransition))
    }

    @Test
    fun `when state and transition to it both exist, transition can be retrieved`() {
        val tr = projectSettingsLogic.findTransitionByProjectAndStatesNames(
            EXISTING_PROJECT_ID, obligatoryStateOne.stateName, obligatoryStateTwo.stateName
        )
        assertEquals(expected = obligatoryTransition, actual = tr)
    }

    @Test
    fun `when target state does not exist, error is thrown`() {
        val e = assertFailsWith<NoSuchIssueStateException> {
            projectSettingsLogic.findTransitionByProjectAndStatesNames(
                EXISTING_PROJECT_ID, obligatoryStateOne.stateName, "some non-existing"
            )
        }
        val msg = "Issue state \"some non-existing\" does not exist for project ID = $EXISTING_PROJECT_ID."
        assertEquals(expected = msg, actual = e.message)
    }

    @Test
    fun `when transition does not exist, error is thrown`() {
        val stateOneName = obligatoryStateOne.stateName
        val stateTwoName = obligatoryStateTwo.stateName
        val e = assertFailsWith<NoSuchStateTransitionException> {
            projectSettingsLogic.findTransitionByProjectAndStatesNames(EXISTING_PROJECT_ID, stateTwoName, stateOneName)
        }
        val msg = "Issues of project ID = $EXISTING_PROJECT_ID can not change state " +
            "from \"$stateTwoName\" to \"$stateOneName\"."
        assertEquals(expected = msg, actual = e.message)
    }

    @Test
    fun `when labels present in project settings they are retrieved`() {
        val labels = projectSettingsLogic.findMatchingLabels(EXISTING_PROJECT_ID, listOf("feature", "bug"))
        assertEquals(expected = existingProjectLabels, actual = labels)
    }

    @Test
    fun `when only one label presents in project settings it is retrieved`() {
        val labels = projectSettingsLogic.findMatchingLabels(EXISTING_PROJECT_ID, listOf("bug"))
        assertEquals(expected = EXISTING_LABEL_TWO, actual = labels.first())
    }

    @Test
    fun `when empty list is passed empty list of labels is retrieved`() {
        val labels = projectSettingsLogic.findMatchingLabels(EXISTING_PROJECT_ID, emptyList())
        assertTrue { labels.isEmpty() }
    }

    @Test
    fun `when labels do not present in project settings exception is thrown`() {
        val e = assertFailsWith<NoSuchLabelException> {
            projectSettingsLogic.findMatchingLabels(EXISTING_PROJECT_ID, listOf("non-existing"))
        }
        val msg = "Labels [non-existing] are not defiled for project ID = $EXISTING_PROJECT_ID."
        assertEquals(expected = msg, actual = e.message)
    }
}
