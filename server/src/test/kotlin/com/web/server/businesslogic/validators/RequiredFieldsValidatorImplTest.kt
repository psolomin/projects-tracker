package com.web.server.businesslogic.validators

import com.web.server.businesslogic.errors.InvalidInputException
import com.web.server.businesslogic.operations.CreateCommentOperation
import com.web.server.businesslogic.operations.CreateIssueOperation
import com.web.server.businesslogic.operations.CreateProjectOperation
import com.web.server.businesslogic.operations.CreateUserOperation
import com.web.server.constants.DefaultFieldsValues
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MAX_LENGTH
import com.web.server.constants.UserInputConstraints.TEXT_INPUT_MIN_LENGTH
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RequiredFieldsValidatorImplTest {
    private companion object {
        val USER_ID = DefaultFieldsValues.generateNewId()
        val PROJECT_ID = DefaultFieldsValues.generateNewId()
        val ISSUE_ID = DefaultFieldsValues.generateNewId()
    }

    private val validator = RequiredFieldsValidatorImpl()

    @Test
    fun `text input of size within borders passes user input validation`() {
        val userOp = CreateUserOperation("user", "password")
        assertTrue { validator.validateUserCreationData(userOp) }
    }

    @Test
    fun `too short name raises exception`() {
        val userOp = CreateUserOperation("u", "password")
        val e1 = assertFailsWith<InvalidInputException> {
            validator.validateUserCreationData(userOp)
        }
        validateException(e1, "Name and password")
    }

    @Test
    fun `too short password raises exception`() {
        val userOp = CreateUserOperation("user", "p")
        val e1 = assertFailsWith<InvalidInputException> {
            validator.validateUserCreationData(userOp)
        }
        validateException(e1, "Name and password")
    }

    @Test
    fun `correct project info passes validation`() {
        val projectOp = CreateProjectOperation("project", "description", USER_ID)
        assertTrue { validator.validateProjectCreationData(projectOp) }
    }

    @Test
    fun `too short project name raises exception`() {
        val projectOp = CreateProjectOperation("p", "description", USER_ID)
        val e1 = assertFailsWith<InvalidInputException> {
            validator.validateProjectCreationData(projectOp)
        }
        validateException(e1, "Name and description")
    }

    @Test
    fun `correct issue info passes validation`() {
        val issueOp = CreateIssueOperation(
            "issue", "description", USER_ID, PROJECT_ID, emptyList()
        )
        assertTrue { validator.validateIssueCreationData(issueOp) }
    }

    @Test
    fun `short issue info fails validation`() {
        val issueOp = CreateIssueOperation(
            "i", "description", USER_ID, PROJECT_ID, emptyList()
        )
        val e1 = assertFailsWith<InvalidInputException> {
            validator.validateIssueCreationData(issueOp)
        }
        validateException(e1, "Name and description")
    }

    @Test
    fun `sufficient comment info passes validation`() {
        val commentOp = CreateCommentOperation("comment body", USER_ID, ISSUE_ID)
        assertTrue { validator.validateCommentCreationData(commentOp) }
    }

    @Test
    fun `short comment body fails validation`() {
        val commentOp = CreateCommentOperation("", USER_ID, ISSUE_ID)
        val e1 = assertFailsWith<InvalidInputException> {
            validator.validateCommentCreationData(commentOp)
        }
        validateException(e1, "Comment")
    }

    private fun validateException(e1: InvalidInputException, varyingText: String) {
        val inputLimitExceptionText = "must have at least $TEXT_INPUT_MIN_LENGTH " +
            "and at most $TEXT_INPUT_MAX_LENGTH characters."
        val expectedMsg = "$varyingText $inputLimitExceptionText"
        assertEquals(expectedMsg, e1.message)
    }
}
