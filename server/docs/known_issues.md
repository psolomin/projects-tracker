# Known Issues

- If a field is missing or `NULL` for `POST` requests' body, 400 is replied, but with "naked" stacktrace segment.
