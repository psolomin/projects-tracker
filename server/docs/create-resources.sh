#!/usr/bin/env bash

PROTO="https"
HOST="web-server.lvh.me"
BASE_URL="$PROTO://$HOST"
CURL_OPTS="-sk"

NAME="Alex"
PASS="Pass"

SECRET=$(echo -n "$NAME:$PASS" | openssl base64)

echo "Creating User..."

curl $CURL_OPTS -X POST "$BASE_URL/users" \
  -H "Content-Type: application/json" \
  -d '{"user_name": "Alex", "user_password": "Pass"}' > /dev/null 2>&1

echo "Creating Project..."

R=$(curl $CURL_OPTS -X POST "$BASE_URL/projects" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic $SECRET" \
  -d '{
       "project_name": "Web Project",
       "description": "My Notorious Web Project",
       "project_settings": {
         "issue_states": [
          {"name": "open", "is_first": true},
          {"name": "closed", "is_first": false},
          {"name": "archived", "is_first": false}
         ],
         "issue_state_transitions": [
          {"state_from": "open", "state_to": "closed"},
          {"state_from": "closed", "state_to": "archived"}
         ],
         "issue_labels": [{"label": "bug"}, {"label": "feature"}]
       }
      }'
  )

echo "Creating Project Issue..."
CREATE_ISSUE_URL=$(echo $R | jq -r '._links."add-new-issue".href')

R=$(curl $CURL_OPTS -X POST "$CREATE_ISSUE_URL" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic $SECRET" \
  -d '{"issue_name": "Login bug","description": "Can not submit login form","labels": ["bug"]}'
  )

echo "Creating Issue Comment..."
CREATE_COMMENT_URL=$(echo $R | jq -r '._links."add-new-comment".href')

COMMENT_URL=$(curl $CURL_OPTS -X POST "$CREATE_COMMENT_URL" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic $SECRET" \
  -d '{"comment_body": "Comment"}' | jq -r '._links.self.href')

curl $CURL_OPTS -X GET "$COMMENT_URL" -H "Authorization: Basic $SECRET" | jq
