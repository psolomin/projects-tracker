# Resources & Endpoints

## Content

- [Authentication](#authentication)
- [Root](#root)
- [Users](#users-resource)
- [Projects](#projects-resource)
- [Issues](#issues-resource)
- [Comments](#comments-resource)
- [API Limits](#api-limits)
- [Tooling used for examples](#tooling-used-for-examples)

## Authentication

Almost all endpoints require authentication for each request ([Basic Auth](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#Basic_authentication_scheme)).

To access them, you must create a user - follow the instructions [here](#users-resource). After that, you will be able to pass auth header with this value:

```
$ echo -n '<user_name>:<user_password>' | openssl base64
```

Example:

```
$ SECRET=$(echo -n 'Alex:Pass' | openssl base64)
$ curl -v -X GET "http://localhost:8080/users/all" -H "Authorization: Basic $SECRET"
```

## Root

### GET /

Lists high-level view of existing endpoints. No auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/" | jq

< HTTP/1.1 200 
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Sat, 08 Feb 2020 19:51:20 GMT
< 
{ [392 bytes data]
100   385    0   385    0     0  17898      0 --:--:-- --:--:-- --:--:-- 18333
* Connection #0 to host localhost left intact
{
  "_links": {
    "self": {
      "href": "http://localhost:8080"
    },
    "users-all": {
      "href": "http://localhost:8080/users/all",
      "title": "All Users"
    },
    "projects-all": {
      "href": "http://localhost:8080/projects/filters",
      "title": "All Projects"
    },
    "issues-all": {
      "href": "http://localhost:8080/issues/filters",
      "title": "All Issues"
    },
    "comments-all": {
      "href": "http://localhost:8080/comments/filters",
      "title": "All Comments"
    }
  }
}
```

Other responses:

- 405 Method Not Allowed
- 406 Not Acceptable

## Users Resource

### GET /users

Getting a form for new user creation. No auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/users" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Mon, 03 Aug 2020 23:04:40 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [340 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "_templates": {
    "default": {
      "title": "Create new user",
      "method": "POST",
      "content_type": "application/json",
      "properties": [
        {
          "name": "user_name",
          "required": true,
          "prompt": "User name",
          "value": "usr"
        },
        {
          "name": "user_password",
          "required": true,
          "prompt": "Password",
          "value": "pass"
        }
      ]
    }
  },
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/users"
    }
  }
}
```

Other responses:

- 405 Method Not Allowed
- 406 Not Acceptable

### POST /users

Creating new user. No auth required.

```
$ curl -skv -X POST "https://web-server.lvh.me/users" \
  -H "Content-Type: application/json" \
  -d '{"user_name": "Alex", "user_password": "Pass"}' | jq

< HTTP/1.1 201 
< Server: nginx/1.17.8
< Date: Mon, 03 Aug 2020 23:06:10 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [625 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "7bdb5803-83cb-49a1-8a68-60df5810e900",
  "user_name": "Alex",
  "created_at": "2020-08-03T23:06:09.855Z",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/users/7bdb5803-83cb-49a1-8a68-60df5810e900"
    },
    "projects": {
      "href": "https://web-server.lvh.me/projects/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
      "title": "Projects created"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
      "title": "Issues created"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
      "title": "Comments created"
    }
  }
}
```

Other responses:

- 400 Bad Request
  - Body is missing
  - Body is malformed
  - User with same name already exists
  - Name or password are too long / too short
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /users/all

Listing all existing users (paged). Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/users/all" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Mon, 03 Aug 2020 23:07:20 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [931 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "_embedded": {
    "users": [
      {
        "id": "7bdb5803-83cb-49a1-8a68-60df5810e900",
        "user_name": "Alex",
        "created_at": "2020-08-03T23:06:09.855Z",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/users/7bdb5803-83cb-49a1-8a68-60df5810e900"
          },
          "projects": {
            "href": "https://web-server.lvh.me/projects/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
            "title": "Projects created"
          },
          "issues": {
            "href": "https://web-server.lvh.me/issues/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
            "title": "Issues created"
          },
          "comments": {
            "href": "https://web-server.lvh.me/comments/filters?creatorId=7bdb5803-83cb-49a1-8a68-60df5810e900",
            "title": "Comments created"
          }
        }
      }
    ]
  },
  "_links": {
    "first": {
      "href": "https://web-server.lvh.me/users/all?page=0&size=5"
    },
    "self": {
      "href": "https://web-server.lvh.me/users/all?page=0&size=5"
    },
    "last": {
      "href": "https://web-server.lvh.me/users/all?page=0&size=5"
    }
  },
  "page": {
    "size": 5,
    "total_elements": 1,
    "total_pages": 1,
    "number": 0
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /users/{userId}

Displays single user. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Tue, 04 Aug 2020 21:46:09 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [625 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "de43a5c1-1d5b-4465-9393-248bc33d5562",
  "user_name": "Alex",
  "created_at": "2020-08-04T21:31:59.790Z",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562"
    },
    "projects": {
      "href": "https://web-server.lvh.me/projects/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Projects created"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Issues created"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Comments created"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 404 Not Found
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /users/home

Displays current authenticated user. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/users/home" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Tue, 04 Aug 2020 21:48:32 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [625 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "de43a5c1-1d5b-4465-9393-248bc33d5562",
  "user_name": "Alex",
  "created_at": "2020-08-04T21:31:59.790Z",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562"
    },
    "projects": {
      "href": "https://web-server.lvh.me/projects/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Projects created"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Issues created"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?creatorId=de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Comments created"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

## Projects resource

### GET /projects

Getting a form for new project creation. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/projects" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Mon, 03 Aug 2020 23:08:12 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [446 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "_templates": {
    "default": {
      "title": "Create new project",
      "method": "POST",
      "content_type": "application/json",
      "properties": [
        {
          "name": "project_name",
          "required": true,
          "value": "name"
        },
        {
          "name": "description",
          "required": true,
          "value": "desc"
        },
        {
          "name": "project_settings",
          "required": false,
          "templated": true,
          "value": "https://web-server.lvh.me/templates/templates/project-settings-tpl"
        }
      ]
    }
  },
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/projects"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

### POST /projects

Create new project (Example 1). Auth required.

```
$ curl -skv -X POST "https://web-server.lvh.me/projects" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic QWxleDpQYXNz" \
  -d '{"project_name": "Phoenix Project", "description": "My Notorious Phoenix Project"}' | jq

< HTTP/1.1 201 
< Server: nginx/1.17.8
< Date: Mon, 03 Aug 2020 23:09:14 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [694 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "c770da6d-98c5-461a-8a01-bf7b7d63b817",
  "project_name": "Phoenix Project",
  "description": "My Notorious Phoenix Project",
  "created_at": "2020-08-03T23:09:14.343Z",
  "creator": "7bdb5803-83cb-49a1-8a68-60df5810e900",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/projects/c770da6d-98c5-461a-8a01-bf7b7d63b817"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/7bdb5803-83cb-49a1-8a68-60df5810e900",
      "title": "Created by"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c770da6d-98c5-461a-8a01-bf7b7d63b817",
      "title": "Issues"
    },
    "add-new-issue": {
      "href": "https://web-server.lvh.me/projects/c770da6d-98c5-461a-8a01-bf7b7d63b817/issues",
      "title": "Add new issue"
    }
  }
}
```

Create new project (Example 2) - with project settings. Auth required.

```
$ curl -skv -X POST "https://web-server.lvh.me/projects" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic QWxleDpQYXNz" \
  -d '{
       "project_name": "Web Project",
       "description": "My Notorious Web Project",
       "project_settings": {
         "issue_states": [
          {"name": "open", "is_first": true},
          {"name": "closed", "is_first": false},
          {"name": "archived", "is_first": false}
         ],
         "issue_state_transitions": [
          {"state_from": "open", "state_to": "closed"},
          {"state_from": "closed", "state_to": "archived"}
         ],
         "issue_labels": [{"label": "bug"}, {"label": "feature"}]
       }
      }' | jq

< HTTP/1.1 201 
< Server: nginx/1.17.8
< Date: Tue, 04 Aug 2020 21:36:33 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [686 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1",
  "project_name": "Web Project",
  "description": "My Notorious Web Project",
  "created_at": "2020-08-04T21:36:33.785Z",
  "creator": "de43a5c1-1d5b-4465-9393-248bc33d5562",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/projects/c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Created by"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1",
      "title": "Issues"
    },
    "add-new-issue": {
      "href": "https://web-server.lvh.me/projects/c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1/issues",
      "title": "Add new issue"
    }
  }
}
```

Other responses:

- 400 Bad Request
  - Body is missing
  - Body is malformed
  - Project with same name already exists
  - Name or description are too long / too short
  - Project settings are invalid:
    - Project's Issues' configuration is invalid
    - Project's Issues' labels are invalid
- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

When Project settings are invalid, the error message in the response body will indicate what went wrong. Example:

```
{
  "message": "Only [A-Za-z0-9]+ are allowed for labels."
}
```

### GET /projects/filters

Get all projects (paged). Allows searching for projects created by other users via : `GET /projects/filters?creatorId={userId}&page=0&size=5`. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/projects/filters" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Tue, 04 Aug 2020 21:55:39 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1788 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "_embedded": {
    "projects": [
      {
        "id": "0077d1fd-edca-416e-9f6f-4b00ba1ce48f",
        "project_name": "Phoenix Project",
        "description": "My Notorious Phoenix Project",
        "created_at": "2020-08-04T21:32:56.621Z",
        "creator": "de43a5c1-1d5b-4465-9393-248bc33d5562",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/projects/0077d1fd-edca-416e-9f6f-4b00ba1ce48f"
          },
          "created-by": {
            "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562",
            "title": "Created by"
          },
          "issues": {
            "href": "https://web-server.lvh.me/issues/filters?projectId=0077d1fd-edca-416e-9f6f-4b00ba1ce48f",
            "title": "Issues"
          },
          "add-new-issue": {
            "href": "https://web-server.lvh.me/projects/0077d1fd-edca-416e-9f6f-4b00ba1ce48f/issues",
            "title": "Add new issue"
          }
        }
      },
      {
        "id": "c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1",
        "project_name": "Web Project",
        "description": "My Notorious Web Project",
        "created_at": "2020-08-04T21:36:33.785Z",
        "creator": "de43a5c1-1d5b-4465-9393-248bc33d5562",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/projects/c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1"
          },
          "created-by": {
            "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562",
            "title": "Created by"
          },
          "issues": {
            "href": "https://web-server.lvh.me/issues/filters?projectId=c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1",
            "title": "Issues"
          },
          "add-new-issue": {
            "href": "https://web-server.lvh.me/projects/c21f7ff1-30f2-4d2a-91ea-0490c3c1dad1/issues",
            "title": "Add new issue"
          }
        }
      }
    ]
  },
  "_links": {
    "first": {
      "href": "https://web-server.lvh.me/projects/filters?page=0&size=5"
    },
    "self": {
      "href": "https://web-server.lvh.me/projects/filters?page=0&size=5"
    },
    "last": {
      "href": "https://web-server.lvh.me/projects/filters?page=0&size=5"
    },
    "add-new-project": {
      "href": "https://web-server.lvh.me/projects",
      "title": "Create project"
    }
  },
  "page": {
    "size": 5,
    "total_elements": 2,
    "total_pages": 1,
    "number": 0
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /projects/{projectId}

Get single project. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/projects/0077d1fd-edca-416e-9f6f-4b00ba1ce48f" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Tue, 04 Aug 2020 21:57:52 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [694 bytes data]
* Connection #0 to host web-server.lvh.me left intact
{
  "id": "0077d1fd-edca-416e-9f6f-4b00ba1ce48f",
  "project_name": "Phoenix Project",
  "description": "My Notorious Phoenix Project",
  "created_at": "2020-08-04T21:32:56.621Z",
  "creator": "de43a5c1-1d5b-4465-9393-248bc33d5562",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/projects/0077d1fd-edca-416e-9f6f-4b00ba1ce48f"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/de43a5c1-1d5b-4465-9393-248bc33d5562",
      "title": "Created by"
    },
    "issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=0077d1fd-edca-416e-9f6f-4b00ba1ce48f",
      "title": "Issues"
    },
    "add-new-issue": {
      "href": "https://web-server.lvh.me/projects/0077d1fd-edca-416e-9f6f-4b00ba1ce48f/issues",
      "title": "Add new issue"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 404 Not Found
- 405 Method Not Allowed
- 406 Not Acceptable

## Issues Resource

### GET /projects/{projectId}/issues

Get template for issue creation for project with ID = `projectId`. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce/issues" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sat, 08 Aug 2020 12:36:37 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [426 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "_templates": {
    "default": {
      "title": "Create new issue",
      "method": "POST",
      "content_type": "application/json",
      "properties": [
        {
          "name": "issue_name",
          "required": true,
          "value": "name"
        },
        {
          "name": "description",
          "required": true,
          "value": "desc"
        },
        {
          "name": "labels",
          "required": false,
          "templated": false,
          "value": [
            "bug",
            "feature"
          ]
        }
      ]
    }
  },
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce/issues"
    }
  }
}
```

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

### POST /projects/{projectId}/issues

Creates new issue within the scope of project with ID = `projectId`. Auth required.

```
$ curl -skv -X POST "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce/issues" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic QWxleDpQYXNz" \
  -d '{
       "issue_name": "Login bug",
       "description": "Can not submit login form",
       "labels": ["bug"]
      }' | jq

< HTTP/1.1 201 
< Server: nginx/1.17.8
< Date: Sat, 08 Aug 2020 12:43:10 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< ETag: "1"
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1325 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "id": "863b3e70-7853-4fff-80ea-983ef4875671",
  "issue_name": "Login bug",
  "description": "Can not submit login form",
  "labels": [
    "bug"
  ],
  "created_at": "2020-08-08T12:43:10.548Z",
  "created_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
  "updated_at": "2020-08-08T12:43:10.548Z",
  "state": "open",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671"
    },
    "update-state": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Update state"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
      "title": "Created by"
    },
    "parent-project": {
      "href": "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "Project"
    },
    "parent-project-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "All project issues"
    },
    "same-labels-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce&labelsContain=bug",
      "title": "Issues with same labels"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?issueId=863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Comments"
    },
    "add-new-comment": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671/comments",
      "title": "Add new comment"
    }
  }
}
```

Other responses:

- 400 Bad Request
  - Body is missing
  - Body is malformed
  - Name or description are too long / too short
  - Label(s) are not defined for project with ID = `projectId`
- 401 Unauthorized
- 404 Not Found
  - if project with `projectId` does not exist
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /issues/{issueId}

Display issue. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671" \
  -H "Content-Type: application/json" \
  -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sat, 08 Aug 2020 12:48:05 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< ETag: "1"
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1325 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "id": "863b3e70-7853-4fff-80ea-983ef4875671",
  "issue_name": "Login bug",
  "description": "Can not submit login form",
  "labels": [
    "bug"
  ],
  "created_at": "2020-08-08T12:43:10.548Z",
  "created_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
  "updated_at": "2020-08-08T12:43:10.548Z",
  "state": "open",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671"
    },
    "update-state": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Update state"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
      "title": "Created by"
    },
    "parent-project": {
      "href": "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "Project"
    },
    "parent-project-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "All project issues"
    },
    "same-labels-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce&labelsContain=bug",
      "title": "Issues with same labels"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?issueId=863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Comments"
    },
    "add-new-comment": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671/comments",
      "title": "Add new comment"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 404 Not Found
- 405 Method Not Allowed
- 406 Not Acceptable

### PATCH /issues/{issueId}

Update Issue state. Auth required.

```
$ curl -skv -X PATCH "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671" \
  -H "Content-Type: application/json" \
  -H 'If-Match: "1"' \
  -H "Authorization: Basic QWxleDpQYXNz" \
  -d '{"new_state": "closed"}' | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sat, 08 Aug 2020 13:03:25 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< ETag: "2"
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1493 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "id": "863b3e70-7853-4fff-80ea-983ef4875671",
  "issue_name": "Login bug",
  "description": "Can not submit login form",
  "labels": [
    "bug"
  ],
  "created_at": "2020-08-08T12:43:10.548Z",
  "created_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
  "updated_at": "2020-08-08T13:03:25.773Z",
  "updated_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
  "state": "closed",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671"
    },
    "update-state": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Update state"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
      "title": "Created by"
    },
    "parent-project": {
      "href": "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "Project"
    },
    "parent-project-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce",
      "title": "All project issues"
    },
    "same-labels-issues": {
      "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce&labelsContain=bug",
      "title": "Issues with same labels"
    },
    "comments": {
      "href": "https://web-server.lvh.me/comments/filters?issueId=863b3e70-7853-4fff-80ea-983ef4875671",
      "title": "Comments"
    },
    "add-new-comment": {
      "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671/comments",
      "title": "Add new comment"
    },
    "updated-by": {
      "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
      "title": "Updated by"
    }
  }
}
```

Other responses:

- 400 Bad Request
  - Body is missing
  - Body is malformed
  - New Issue state is undefined for the project 
  - Issue version (`If-Match` header) missing
- 401 Unauthorized
- 404 Not Found
- 405 Method Not Allowed
- 406 Not Acceptable
- 412 Precondition Failed
  - Current request failed due to conflicting update (someone updated the issue before), new `eTag` required for an update attempt

### GET /issues/filters

Get all issues (paged). Auth required.

Allows searching by:

- parent project: `GET /issues/filters?projectId={projectId}&page=0&size=5`
- creator: `GET /issues/filters?creatorId={userId}&page=0&size=5`
- issue labels: `GET /issues/filters?labelsContain={labels}&page=0&size=5` with comma-separated `labels` like `labelA,labelB`

```
$ curl -skv -X GET "https://web-server.lvh.me/issues/filters" \
    -H "Content-Type: application/json" \
    -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sat, 08 Aug 2020 13:05:00 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1815 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "_embedded": {
    "issues": [
      {
        "id": "863b3e70-7853-4fff-80ea-983ef4875671",
        "issue_name": "Login bug",
        "description": "Can not submit login form",
        "labels": [
          "bug"
        ],
        "created_at": "2020-08-08T12:43:10.548Z",
        "created_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
        "updated_at": "2020-08-08T13:03:25.773Z",
        "updated_by": "f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
        "state": "closed",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671"
          },
          "update-state": {
            "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671",
            "title": "Update state"
          },
          "created-by": {
            "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
            "title": "Created by"
          },
          "parent-project": {
            "href": "https://web-server.lvh.me/projects/c128f17e-e503-4cbf-b07e-8020ec8286ce",
            "title": "Project"
          },
          "parent-project-issues": {
            "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce",
            "title": "All project issues"
          },
          "same-labels-issues": {
            "href": "https://web-server.lvh.me/issues/filters?projectId=c128f17e-e503-4cbf-b07e-8020ec8286ce&labelsContain=bug",
            "title": "Issues with same labels"
          },
          "comments": {
            "href": "https://web-server.lvh.me/comments/filters?issueId=863b3e70-7853-4fff-80ea-983ef4875671",
            "title": "Comments"
          },
          "add-new-comment": {
            "href": "https://web-server.lvh.me/issues/863b3e70-7853-4fff-80ea-983ef4875671/comments",
            "title": "Add new comment"
          },
          "updated-by": {
            "href": "https://web-server.lvh.me/users/f9a5ce67-f5e8-4ca1-8892-560b83c2f4df",
            "title": "Updated by"
          }
        }
      }
    ]
  },
  "_links": {
    "first": {
      "href": "https://web-server.lvh.me/issues/filters?page=0&size=5"
    },
    "self": {
      "href": "https://web-server.lvh.me/issues/filters?page=0&size=5"
    },
    "last": {
      "href": "https://web-server.lvh.me/issues/filters?page=0&size=5"
    }
  },
  "page": {
    "size": 5,
    "total_elements": 1,
    "total_pages": 1,
    "number": 0
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

## Comments Resource

### GET /issues/{issueId}/comments

Get a form for creating a comment. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95/comments" \
    -H "Content-Type: application/json" \
    -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sun, 09 Aug 2020 12:43:25 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [310 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "_templates": {
    "default": {
      "title": "Create new comment",
      "method": "POST",
      "content_type": "application/json",
      "properties": [
        {
          "name": "comment_body",
          "required": true,
          "value": "this is a comment"
        }
      ]
    }
  },
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95/comments"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

### POST /issues/{issueId}/comments

Create a comment for issue with ID = `issueId`. Auth required.

```
$ curl -skv -X POST "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95/comments" \
    -H "Content-Type: application/json" \
    -H "Authorization: Basic QWxleDpQYXNz" \
    -d '{"comment_body": "My Comment"}'| jq

< HTTP/1.1 201 
< Server: nginx/1.17.8
< Date: Sun, 09 Aug 2020 12:46:09 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [831 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "id": "1f84d94b-5203-454a-93ef-5b069ce4b199",
  "comment_body": "My Comment",
  "created_at": "2020-08-09T12:46:09.175Z",
  "created_by": "8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
  "parent_issue_id": "432e9d72-2103-46cc-a915-3bc983c52d95",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/comments/1f84d94b-5203-454a-93ef-5b069ce4b199"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
      "title": "Created by"
    },
    "parent-issue": {
      "href": "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95",
      "title": "Issue"
    },
    "parent-project": {
      "href": "https://web-server.lvh.me/projects/568b6bbf-1d0d-427e-8371-9378e39318be",
      "title": "Project"
    },
    "parent-issue-comments": {
      "href": "https://web-server.lvh.me/comments/filters?issueId=432e9d72-2103-46cc-a915-3bc983c52d95",
      "title": "All issue comments"
    }
  }
}
```

Other responses:

- 400 Bad Request
  - Body is missing
  - Body is malformed
- 401 Unauthorized
- 404 Not Found
  - When Issue does not exist
- 405 Method Not Allowed
- 406 Not Acceptable
- 422 Unprocessable Entity
  - When current Issue state does not allow adding more comments

### GET /comments/{commentId}

Get single Comment representation. Auth required.

```
$ curl -skv -X GET "https://web-server.lvh.me/comments/1f84d94b-5203-454a-93ef-5b069ce4b199" \
    -H "Content-Type: application/json" \
    -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sun, 09 Aug 2020 12:54:16 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [831 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "id": "1f84d94b-5203-454a-93ef-5b069ce4b199",
  "comment_body": "My Comment",
  "created_at": "2020-08-09T12:46:09.175Z",
  "created_by": "8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
  "parent_issue_id": "432e9d72-2103-46cc-a915-3bc983c52d95",
  "_links": {
    "self": {
      "href": "https://web-server.lvh.me/comments/1f84d94b-5203-454a-93ef-5b069ce4b199"
    },
    "created-by": {
      "href": "https://web-server.lvh.me/users/8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
      "title": "Created by"
    },
    "parent-issue": {
      "href": "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95",
      "title": "Issue"
    },
    "parent-project": {
      "href": "https://web-server.lvh.me/projects/568b6bbf-1d0d-427e-8371-9378e39318be",
      "title": "Project"
    },
    "parent-issue-comments": {
      "href": "https://web-server.lvh.me/comments/filters?issueId=432e9d72-2103-46cc-a915-3bc983c52d95",
      "title": "All issue comments"
    }
  }
}
```

Other responses:

- 401 Unauthorized
- 404 Not Found
  - When Comment does not exist
- 405 Method Not Allowed
- 406 Not Acceptable

### GET /comments/filters

Listing all existing Comments (paged). Auth required.

Allows searching by:

- parent Issue: `GET /comments/filters?issueId={issueId}&page=0&size=5`
- creator: `GET /comments/filters?creatorId={userId}&page=0&size=5`

```
$ curl -skv -X GET "https://web-server.lvh.me/comments/filters" \                             
      -H "Content-Type: application/json" \
      -H "Authorization: Basic QWxleDpQYXNz" | jq

< HTTP/1.1 200 
< Server: nginx/1.17.8
< Date: Sun, 09 Aug 2020 13:01:17 GMT
< Content-Type: application/prs.hal-forms+json;charset=UTF-8
< Transfer-Encoding: chunked
< Connection: keep-alive
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< Strict-Transport-Security: max-age=31536000 ; includeSubDomains
< X-Frame-Options: DENY
< Strict-Transport-Security: max-age=31536000
< 
{ [1978 bytes data]
* Connection #0 to host web-server.lvh.me left intact
* Closing connection 0
{
  "_embedded": {
    "comments": [
      {
        "id": "d4503213-fa51-475c-aff9-beafb9acc231",
        "comment_body": "Comment",
        "created_at": "2020-08-09T12:36:50.815Z",
        "created_by": "8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
        "parent_issue_id": "432e9d72-2103-46cc-a915-3bc983c52d95",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/comments/d4503213-fa51-475c-aff9-beafb9acc231"
          },
          "created-by": {
            "href": "https://web-server.lvh.me/users/8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
            "title": "Created by"
          },
          "parent-issue": {
            "href": "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95",
            "title": "Issue"
          },
          "parent-project": {
            "href": "https://web-server.lvh.me/projects/568b6bbf-1d0d-427e-8371-9378e39318be",
            "title": "Project"
          },
          "parent-issue-comments": {
            "href": "https://web-server.lvh.me/comments/filters?issueId=432e9d72-2103-46cc-a915-3bc983c52d95",
            "title": "All issue comments"
          }
        }
      },
      {
        "id": "1f84d94b-5203-454a-93ef-5b069ce4b199",
        "comment_body": "My Comment",
        "created_at": "2020-08-09T12:46:09.175Z",
        "created_by": "8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
        "parent_issue_id": "432e9d72-2103-46cc-a915-3bc983c52d95",
        "_links": {
          "self": {
            "href": "https://web-server.lvh.me/comments/1f84d94b-5203-454a-93ef-5b069ce4b199"
          },
          "created-by": {
            "href": "https://web-server.lvh.me/users/8b6a1929-cb04-4e47-8ce5-dce3e08fc7d6",
            "title": "Created by"
          },
          "parent-issue": {
            "href": "https://web-server.lvh.me/issues/432e9d72-2103-46cc-a915-3bc983c52d95",
            "title": "Issue"
          },
          "parent-project": {
            "href": "https://web-server.lvh.me/projects/568b6bbf-1d0d-427e-8371-9378e39318be",
            "title": "Project"
          },
          "parent-issue-comments": {
            "href": "https://web-server.lvh.me/comments/filters?issueId=432e9d72-2103-46cc-a915-3bc983c52d95",
            "title": "All issue comments"
          }
        }
      }
    ]
  },
  "_links": {
    "first": {
      "href": "https://web-server.lvh.me/comments/filters?page=0&size=5"
    },
    "self": {
      "href": "https://web-server.lvh.me/comments/filters?page=0&size=5"
    },
    "last": {
      "href": "https://web-server.lvh.me/comments/filters?page=0&size=5"
    }
  },
  "page": {
    "size": 5,
    "total_elements": 2,
    "total_pages": 1,
    "number": 0
  }
}
```

Other responses:

- 401 Unauthorized
- 405 Method Not Allowed
- 406 Not Acceptable

## API Limits

- All text input fields have min = 3 and max = 125 characters
- Project creation allowed with up to 15 Issues' states configuration
- Project creation allowed with up to 15 Issues' labels configuration
- Only ASCII alphanumeric characters allowed for labels
- Up to 40 elements per page allowed for resources' listings

## Tooling used for examples

```
$ curl --version
curl 7.54.0 (x86_64-apple-darwin18.0) libcurl/7.54.0 LibreSSL/2.6.5 zlib/1.2.11 nghttp2/1.24.1
Protocols: dict file ftp ftps gopher http https imap imaps ldap ldaps pop3 pop3s rtsp smb smbs smtp smtps telnet tftp 
Features: AsynchDNS IPv6 Largefile GSS-API Kerberos SPNEGO NTLM NTLM_WB SSL libz HTTP2 UnixSockets HTTPS-proxy

$ jq --version
jq-1.6
```
