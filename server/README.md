# PROJECTS TRACKER - HTTP App

Implements the project tracking system logic.

## Endpoints

Endpoints support [HAL](http://stateless.co/hal_specification.html) for resources' discovery and [HAL-FORMS](https://rwcbook.github.io/hal-forms/) media type for actions' discovery.

Details on API endpoints and resources can be found [here](docs/endpoints.md).

## Architecture

![Server Architecture](docs/server-architecture.png)

[Source](http://www.plantuml.com/plantuml/png/JP5FJ-Cm4CNlyob6EQvKARrLrUwgtJ-h1Ne8JMwbgXnd6WoERJmE813VdN72ePRyz6p_9depsN1ICOs31TRA4YvNPVbjMPObyADnGKKreBQA-PztgJPlXAkBbuxEQhIfeTUqjQPt0xc4MeGYWFCTuMzhv0WcNvsL9lpVD1KgnlkKmepK59yfuZiW7bGmn_nQRAkhE_Uz_Lc7Q5oF-xzrWreYH6yjHIuoR7rlz8p_6jauOjxbgwdmnYThtCH52dx6zkBO91yDyKHTLzi2FYxY5VNjhiXifon0fv9g5HC6Vh9u0AceFAS-ac2ma0L_lf0rlb1xalodheKVig2IvDe4PL48Ixo6aIKwXEWVI2S6mpmIW_P3dXl34M0U5oxcyMGHBny5sUAcZc5IEUM-88lu75Usk1mLDkGw-QcV)

## Configuration

| name                         | required |
|------------------------------|----------|
| SPRING_DATASOURCE_URL        |    yes   |
| SPRING_DATASOURCE_USERNAME   |    yes   |
| SPRING_DATASOURCE_PASSWORD   |    yes   |
| SPRING_JPA_DATABASE_PLATFORM |    yes   |


## How To

### Build executable

* `make build` - generates an artifact at `./build/libs/web-server-0.0.1.jar`

### Starting and stopping the app

* Load balancer & two server nodes: `make run-distributed`

* One server node: `make run`

* One server node on host JVM: `make run-local`

* Tear down all Docker services: `make down`

### Development

* Run code style check - `./gradlew ktlintCheck`, apply formatting - `./gradlew ktlintFormat`

* Check for unused dependencies - `./gradlew lintGradle` (still to be configured)

* Run business logic tests - `make unit-test-local`

* Run unit & integration tests in Docker - `make test`

* Run unit & integration tests on host JVM - `make test-local`

* Start dev database in Docker - `make db`

* Execute some query against a database (requires password - which is the one in `db/.env.db` file):
    - `psql -h localhost -p 5432 -d web_api_db -U db_master_user -c "select * from public.users limit 5"`
    - `psql -h localhost -p 5432 -d web_api_db -U db_master_user -v ON_ERROR_STOP=ON -a -f db/db_setup.sql`

**NOTE:** It is possible to run / debug with your IDE, if the `./.env.example` env vars are given to run / debug configurations of your IDE.

## Known Issues

Listed [here](docs/known_issues.md)
