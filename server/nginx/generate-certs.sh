#!/usr/bin/env bash

set -e

# create certs dir if does not exist:
mkdir -p /etc/nginx/ssl/web-common.lvh.me

# generate new cert:
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout /etc/nginx/ssl/web-common.lvh.me/web-common.key \
    -new \
    -out /etc/nginx/ssl/web-common.lvh.me/web-common.crt  \
    -subj /CN=lvh.me \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /etc/ssl/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS.1:*.lvh.me\n')) \
    -sha256 \
    -days 365
