#!/usr/bin/env bash

export NODE_1_UPSTREAM_HOST=$NODE_1_UPSTREAM_HOST
export NODE_1_UPSTREAM_PORT=$NODE_1_UPSTREAM_PORT
export NODE_2_UPSTREAM_HOST=$NODE_2_UPSTREAM_HOST
export NODE_2_UPSTREAM_PORT=$NODE_2_UPSTREAM_PORT

envsubst '$NODE_1_UPSTREAM_HOST,$NODE_1_UPSTREAM_PORT,$NODE_2_UPSTREAM_HOST,$NODE_2_UPSTREAM_PORT' \
  < /etc/nginx/conf.d/conf.template \
  > /etc/nginx/conf.d/lvh.me.conf

rm /etc/nginx/conf.d/conf.template
rm /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'
